# Portfolio Manger

Проект предназначен для работы с порфелями акций, и подсчетом статистики за каждый день.

## Использование

1. Войдите в докере `docker login registry.gitlab.com`
2. Указать образ `registry.gitlab.com/security-site/dbcache/dbcache:latest`

## Для локального запуска сервиса

1. Запустите docker compose локальный стек
2. Соберите image `clean compile jib:dockerBuild -Dmaven.test.skip=true`
3. В файле `.env` укажите ваш ip
4. Запустите сервис из docker-compose.yaml
