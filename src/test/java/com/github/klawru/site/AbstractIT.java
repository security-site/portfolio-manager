package com.github.klawru.site;

import com.github.klawru.site.config.BlockHoundConfigIntegration;
import com.github.klawru.site.repository.BankMapperRepository;
import com.github.klawru.site.utility.TestClock;
import com.github.klawru.site.utility.TimescaleDBContainer;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mockito;
import org.mockito.internal.util.MockUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.PostgreSQLContainer;
import reactor.blockhound.BlockHound;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;
import static org.testcontainers.containers.PostgreSQLContainer.POSTGRESQL_PORT;

@Slf4j
@ActiveProfiles({"test"})
@SpringBootTest
@TestInstance(PER_CLASS)
public abstract class AbstractIT {
    @Autowired
    ApplicationContext applicationContext;
    @Autowired
    Set<? extends ReactiveCrudRepository<?, ?>> repositories;
    List<Object> mockedBeans;
    @Autowired
    TestClock clock;

    private static final PostgreSQLContainer<?> POSTGRES = new TimescaleDBContainer<>("timescale/timescaledb:2.5.1-pg14");

    static {
        POSTGRES.start();
        BlockHound.install(new BlockHoundConfigIntegration());
    }

    @BeforeAll
    void beforeAll() {
        //Как вариант есть библиотека MockInBean
        findAllMock();
    }

    @AfterEach
    void tearDown() {
        clearAllRepositories();
        resetAllMock();
        clock.reset();
    }

    private void clearAllRepositories() {
        repositories.stream()
                .filter(repository -> !(repository instanceof BankMapperRepository))
                .forEach(repository -> repository.deleteAll().block());
    }

    private void resetAllMock() {
        mockedBeans.forEach(Mockito::reset);
    }

    private void findAllMock() {
        mockedBeans = Arrays.stream(applicationContext.getBeanDefinitionNames())
                .map(name -> applicationContext.getBean(name))
                .filter(MockUtil::isMock)
                .collect(Collectors.toList());
    }

    @DynamicPropertySource
    static void postgresProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", () -> POSTGRES.getJdbcUrl() + "?schema=portfolio");
        registry.add("spring.datasource.username", POSTGRES::getUsername);
        registry.add("spring.datasource.password", POSTGRES::getPassword);
        registry.add("spring.flyway.url", POSTGRES::getJdbcUrl);
        registry.add("spring.flyway.user", POSTGRES::getUsername);
        registry.add("spring.flyway.password", POSTGRES::getPassword);
        registry.add("spring.r2dbc.url", () -> "r2dbc:pool:postgres://%s:%d/%s?schema=portfolio"
                .formatted(POSTGRES.getContainerIpAddress(), POSTGRES.getMappedPort(POSTGRESQL_PORT), POSTGRES.getDatabaseName()));
        registry.add("spring.r2dbc.username", POSTGRES::getUsername);
        registry.add("spring.r2dbc.password", POSTGRES::getPassword);
    }

}
