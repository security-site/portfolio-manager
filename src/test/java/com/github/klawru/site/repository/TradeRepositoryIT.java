package com.github.klawru.site.repository;

import com.github.klawru.site.AbstractIT;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

import static com.github.klawru.site.testutil.TradeTestUtils.newTrade;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@Slf4j
class TradeRepositoryIT extends AbstractIT {
    @Autowired
    TradeRepository repository;

    @Test
    void upsert() {
        final var user = UUID.randomUUID();
        final var expected = newTrade("UPSERT", 1, user);
        //When
        repository.upsert(expected).block();
        repository.upsert(expected).block();
        //Then
        final var tradeEntities = repository.findAllByUserIdOrderByDateDescIdDesc(user)
                .collectList()
                .block();
        log.debug("tradeEntities:{}", tradeEntities);
        assertNotNull(tradeEntities);
        assertEquals(1, tradeEntities.size());
    }
}