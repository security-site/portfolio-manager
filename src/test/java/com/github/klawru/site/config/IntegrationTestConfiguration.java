package com.github.klawru.site.config;

import com.github.klawru.site.service.client.DbCacheClient;
import com.github.klawru.site.service.client.DbCacheMockClientTest;
import com.github.klawru.site.utility.TestClock;
import org.mockito.Mockito;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.oauth2.client.OAuth2AuthorizeRequest;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.ReactiveOAuth2AuthorizedClientManager;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.InMemoryReactiveClientRegistrationRepository;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.JwtClaimNames;
import org.springframework.security.oauth2.jwt.ReactiveJwtDecoder;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneOffset;

import static java.time.temporal.ChronoUnit.DAYS;
import static org.springframework.security.oauth2.core.AuthorizationGrantType.CLIENT_CREDENTIALS;
import static org.springframework.security.oauth2.core.OAuth2AccessToken.TokenType.BEARER;

@Configuration
public class IntegrationTestConfiguration {

    @Bean
    WebTestClient webTestClient(ApplicationContext context) {
        return WebTestClient.bindToApplicationContext(context).build();
    }

    @Bean
    @Primary
    DbCacheClient dbCacheMockClient(Clock clock) {
        return Mockito.spy(new DbCacheMockClientTest(clock));
    }

    @Bean
    @Primary
    ReactiveJwtDecoder jwtDecoder() {
        return token -> Mono.just(Jwt.withTokenValue(token)
                .header("alg", "none")
                .claim(JwtClaimNames.SUB, "user")
                .claim("scope", "read")
                .build());
    }


    /**
     * <pre>
     * 2020
     * +------+-----+-----+-----+-----+-----+----+
     * | May  |     |     |     |     |     |    |
     * +------+-----+-----+-----+-----+-----+----+
     * | MO   | TU  | WE  | TH  | FR  | SA  | SU |
     * | 1    | 2   | 3   |     |     |     |    |
     * | 4    | 5   | 6   | 7   | 8   | 9   | 10 |
     * | 11   | 12  | 13  | 14  | 15  | 16  | 17 |
     * | 18   | 19  | 20  | 21  | 22  | 23  | 24 |
     * | 25   | 26  | 27  | 28  | 29  | 30  | 31 |
     * |      |     |     |     |     |     |    |
     * | June |     |     |     |     |     |    |
     * | MO   | TU  | WE  | TH  | FR  | SA  | SU |
     * |->1___| 2   | 3   | 4   | 5   | 6   | 7  |
     * | 8    | 9   | 10  | 11  | 12  | 13  | 14 |
     * | 15   | 16  | 17  | 18  | 19  | 20  | 21 |
     * | 22   | 23  | 24  | 25  | 26  | 27  | 28 |
     * | 29   | 30  |     |     |     |     |    |
     * +------+-----+-----+-----+-----+-----+----+
     * </pre>
     */
    @Bean
    @Primary
    TestClock clockMock() {
        return new TestClock(Instant.parse("2020-06-01T12:00:00.00Z"), ZoneOffset.UTC);
    }

    @Bean
    @Primary
    ReactiveOAuth2AuthorizedClientManager clientManager(Clock clock, ClientRegistration registration) {
        return new ReactiveOAuth2AuthorizedClientManager() {
            final OAuth2AuthorizedClient client = new OAuth2AuthorizedClient(registration, "Mock",
                    new OAuth2AccessToken(BEARER, "mock_jwt",
                            clock.instant().minus(1, DAYS),
                            clock.instant().plus(1, DAYS)));

            @Override
            public Mono<OAuth2AuthorizedClient> authorize(OAuth2AuthorizeRequest authorizeRequest) {
                return Mono.just(client);
            }
        };
    }

    @Bean
    ClientRegistration clientRegistration() {
        return ClientRegistration.withRegistrationId("keycloak")
                .authorizationGrantType(CLIENT_CREDENTIALS)
                .clientId("backendId")
                .tokenUri("http://tokenUriMock")
                .build();
    }

    @Bean
    InMemoryReactiveClientRegistrationRepository mockRegistrationRepository(ClientRegistration registration) {
        return new InMemoryReactiveClientRegistrationRepository(registration);
    }


}
