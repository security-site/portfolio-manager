package com.github.klawru.site.utility;

import com.github.klawru.site.AbstractIT;
import com.github.klawru.site.repository.entity.Operation;
import com.github.klawru.site.repository.entity.TradeEntity;
import com.github.klawru.site.repository.entity.TradesFileInfo;
import com.github.klawru.site.service.TradesService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomUtils;
import org.eclipse.collections.impl.factory.Lists;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.UUID;
import java.util.stream.IntStream;

@Slf4j
@Disabled("TradeDataGeneratorIT - test disabled: генератор тестовых данных")
public class TradeDataGeneratorIT extends AbstractIT {

    @Autowired
    TradesService tradesService;

    private final int tickerTradeSize = 10000;
    private final int users = 1;

    @Test
    public void generateData() {
        final var uuids = generateUsersId(users);
        final var tickers = new String[]{"RU000A100D30", "NLMK", "FXCN", "AFLT", "SIBN", "FXIT", "CHMF", "MAGN", "SBCB", "PMSBP", "FXUS", "VTBR", "FXIM", "MRKV", "T-RM", "OGKB", "FXRU", "IRAO", "FXGD", "VTBE", "RSTI", "RU000A100D89", "FLOT", "RU000A101HU5", "BSPB", "ALRS", "RU000A0JXMB0", "VTBY", "GAZP", "FXDE", "MOEX", "FESH", "RTKM", "MAIL", "RCMX"};
        final var operations = Operation.values();
        final var starDate = LocalDateTime.of(2015, 1, 1, 9, 0, 0);
        Arrays.stream(uuids).parallel().forEach(uuid -> {
            log.info("Start batch {}", uuid);
            var fileId = UUID.randomUUID();
            var tickerTrade = Lists.mutable.<TradeEntity>withInitialCapacity(tickerTradeSize);
            for (String ticker : tickers) {
                var startDateTicker = starDate.plusDays(RandomUtils.nextLong(0, 1001));
                var price = RandomUtils.nextLong(100, 10000);
                var sumQuantity = 0;

                for (int i = 0; i < tickerTradeSize; i++) {
                    var operation = operations[RandomUtils.nextInt(0, 2)];
                    startDateTicker = generateDateTime(startDateTicker);
                    int quantity = calcQuantity(sumQuantity, operation);
                    sumQuantity = calcSumQuantity(sumQuantity, quantity, operation);
                    price = generatePrice(price);
                    tickerTrade.add(generateTrade(uuid, startDateTicker, ticker, operation, price, quantity, fileId));
                }
            }
            tradesService.saveTrades(tickerTrade.toImmutable(), new TradesFileInfo(fileId, "test_generateData")).block();
        });
    }

    private Instant generateFileId(Instant fileId) {
        if (RandomUtils.nextInt(0, 20) == 1)
            return Instant.now();
        return fileId;
    }

    private int calcQuantity(int sumQuantity, Operation operation) {
        if (sumQuantity == 0) {
            operation = Operation.BUY;
        }
        int quantity;
        if (operation == Operation.BUY) {
            quantity = RandomUtils.nextInt(1, 111);
        } else {
            quantity = RandomUtils.nextInt(1, 120);
            if (quantity > sumQuantity) {
                quantity = sumQuantity;
            }

        }
        return quantity;
    }

    private int calcSumQuantity(int sumQuantity, int quantity, Operation operation) {
        if (operation == Operation.BUY)
            sumQuantity += quantity;
        else
            sumQuantity -= quantity;
        return sumQuantity;
    }

    private long generatePrice(long price) {
        if (price < 100)
            return Math.round(100_00 * RandomUtils.nextDouble(1, 1.2));
        else
            return Math.round(price * RandomUtils.nextDouble(0.8, 1.2));
    }

    @NotNull
    private LocalDateTime generateDateTime(LocalDateTime startDateTicker) {
        //В 30% случаев следующая сделка пройдет в тотже день
        if (RandomUtils.nextInt(0, 10) > 3)
            startDateTicker = startDateTicker.plusMinutes(RandomUtils.nextInt(0, 360));
        else
            startDateTicker = startDateTicker.plusDays(RandomUtils.nextInt(0, 10)).plusHours(RandomUtils.nextInt(0, 12));
        return startDateTicker;
    }

    private UUID[] generateUsersId(int size) {
        return IntStream.range(0, size).mapToObj(i -> UUID.randomUUID()).toArray(UUID[]::new);
    }

    private TradeEntity generateTrade(@NotNull UUID userId, LocalDateTime date, @NotNull String ticker, @NotNull Operation operation, long price, int quantity, UUID fileId) {
        return new TradeEntity(null,
                userId,
                date,
                date,
                ticker,
                "",
                "",
                operation,
                BigDecimal.valueOf(price),
                643,
                BigDecimal.valueOf(price).multiply(BigDecimal.valueOf(quantity)),
                5,
                quantity,
                BigDecimal.ZERO,
                643,
                BigDecimal.ZERO,
                643,
                null,
                0,
                fileId,
                "");
    }
}
