package com.github.klawru.site.utility;

import com.github.klawru.site.repository.entity.Operation;
import com.github.klawru.site.repository.entity.SecurityKey;
import com.github.klawru.site.repository.entity.TradeEntity;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.collections.api.factory.Lists;
import org.junit.jupiter.api.Test;

import static com.github.klawru.site.testutil.TradeTestUtils.newTrade;
import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
class TradeUtilityTest {

    @Test
    void negativeIfSale() {
        assertThat(TradeUtility.negativeIfSale(Operation.SALE, 10))
                .isEqualTo(-10);
        assertThat(TradeUtility.negativeIfSale(Operation.BUY, 10))
                .isEqualTo(10);

        assertThat(TradeUtility.negativeIfSale(Operation.SALE, 10L))
                .isEqualTo(-10L);
        assertThat(TradeUtility.negativeIfSale(Operation.BUY, 10L))
                .isEqualTo(10L);
    }


    @Test
    void negativeExact() {
        assertThat(TradeUtility.negativeExact(42)).isNegative();
        assertThat(TradeUtility.negativeExact(-10)).isNegative();
    }

    @Test
    void createMoneyTicker() {
        final var rub = TradeUtility.createMoneyTicker("RUB");
        assertThat(TradeUtility.isMoney(rub)).isTrue();
    }

    @Test
    void isMoney() {
        assertThat(TradeUtility.isMoney("$RUB")).isTrue();
        assertThat(TradeUtility.isMoney("SBER")).isFalse();

    }

    @Test
    void groupBySecurityKey() {
        int i = 0;
        final var result = Lists.mutable.of(
                        newTrade("1", "1", i++),
                        newTrade("2", "2", i++), newTrade("2", "2", i++),
                        newTrade("3", "3", i++), newTrade("3", "3", i++), newTrade("3", "3", i++))
                .stream()
                .collect(TradeUtility.groupBySecurityKey());
        log.debug("result:{}", result);
        assertThat(result.size()).isEqualTo(6);
        assertThat(result.get(SecurityKey.of("1", "1"))).hasSize(1).extracting(TradeEntity::getTicker).containsOnly("1");
        assertThat(result.get((SecurityKey.of("2", "2")))).hasSize(2).extracting(TradeEntity::getTicker).containsOnly("2");
        assertThat(result.get((SecurityKey.of("3", "3")))).hasSize(3).extracting(TradeEntity::getTicker).containsOnly("3");
    }
}