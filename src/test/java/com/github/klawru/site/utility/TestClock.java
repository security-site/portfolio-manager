package com.github.klawru.site.utility;

import lombok.Getter;

import javax.annotation.Nonnull;
import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.TemporalAmount;

public class TestClock extends Clock {
    private final Instant resetHolder;
    private Instant instant;
    @Getter
    private final ZoneId zone;

    public TestClock(Instant fixedInstant, ZoneId zone) {
        this.resetHolder = fixedInstant;
        this.instant = fixedInstant;
        this.zone = zone;
    }

    @Override
    public Clock withZone(ZoneId zone) {
        if (zone.equals(this.zone)) {  // intentional NPE
            return this;
        }
        return new TestClock(instant, zone);
    }

    @Override
    public long millis() {
        return instant.toEpochMilli();
    }

    @Override
    public Instant instant() {
        return instant;
    }

    public void plus(@Nonnull TemporalAmount amount) {
        instant = instant.plus(amount);
    }

    public void reset() {
        instant = resetHolder;
    }
}