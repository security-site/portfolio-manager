package com.github.klawru.site.utility;

import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.containers.wait.strategy.LogMessageWaitStrategy;
import org.testcontainers.utility.DockerImageName;

import java.time.Duration;
import java.util.Map;

import static java.time.temporal.ChronoUnit.SECONDS;

public class TimescaleDBContainer<SELF extends PostgreSQLContainer<SELF>> extends PostgreSQLContainer<SELF> {
    public TimescaleDBContainer(String dockerImageName) {
        super(DockerImageName.parse(dockerImageName).asCompatibleSubstituteFor("postgres"));
        setWaitStrategy(new LogMessageWaitStrategy()
                .withRegEx(".*PostgreSQL init process complete; ready for start up.*\\s")
                .withTimes(1)
                .withStartupTimeout(Duration.of(60, SECONDS)));
        withEnv(Map.of(
                "TIMESCALEDB_TELEMETRY", "off",
                "NO_TS_TUNE", "true"));
    }
}
