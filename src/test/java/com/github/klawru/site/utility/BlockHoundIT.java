package com.github.klawru.site.utility;

import com.github.klawru.site.AbstractIT;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import static org.junit.Assert.assertThrows;

public class BlockHoundIT extends AbstractIT {

    @Test
    void blockHoundTest() {
        assertThrows(Exception.class, () -> {
            Mono.fromCallable(() -> {
                Thread.sleep(1);
                return "";
            }).subscribeOn(Schedulers.parallel()).block();
        });
    }

}
