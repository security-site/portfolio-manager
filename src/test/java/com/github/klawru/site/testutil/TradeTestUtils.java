package com.github.klawru.site.testutil;

import com.github.klawru.site.repository.entity.Operation;
import com.github.klawru.site.repository.entity.TradeEntity;
import lombok.experimental.UtilityClass;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static com.github.klawru.site.testutil.TestUtils.fromJson;
import static com.github.klawru.site.testutil.TestUtils.getFile;

@UtilityClass
public class TradeTestUtils {

    public static List<TradeEntity> parseJsonTrade(String s) throws java.io.IOException {
        return Arrays.asList(fromJson(getFile(s), TradeEntity[].class));
    }

    public static TradeEntity newTrade(String ticker, String exchange, int isin) {
        return new TradeEntity(UUID.randomUUID(),
                null,
                LocalDateTime.now().truncatedTo(ChronoUnit.MICROS),
                LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS),
                ticker,
                exchange,
                "" + isin,
                Operation.BUY,
                BigDecimal.valueOf(100000L),
                643,
                BigDecimal.valueOf(500000L),
                5,
                5,
                BigDecimal.valueOf(0L),
                643,
                BigDecimal.valueOf(0L),
                643,
                123,
                0,
                UUID.randomUUID(),
                "note");
    }

    public static TradeEntity newTrade(String ticker, int isin, UUID userId) {
        final var entity = newTrade(ticker, ticker, isin);
        entity.setUserId(userId);
        return entity;
    }
}
