package com.github.klawru.site.testutil;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.experimental.UtilityClass;
import org.assertj.core.api.recursive.comparison.RecursiveComparisonConfiguration;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.JwtClaimNames;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.util.ResourceUtils;
import reactor.util.context.Context;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.UUID;

import static org.assertj.core.util.BigDecimalComparator.BIG_DECIMAL_COMPARATOR;

@UtilityClass
public class TestUtils {

    static final ObjectMapper mapper = new ObjectMapper()
            .findAndRegisterModules()
            .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);

    public static final RecursiveComparisonConfiguration recursiveConfig = RecursiveComparisonConfiguration.builder()
            .withComparatorForType(BIG_DECIMAL_COMPARATOR, BigDecimal.class)
            .build();

    public static File getFile(String path) throws FileNotFoundException {
        return ResourceUtils.getFile("classpath:" + path);
    }

    public static String asJson(Object object) throws IOException {
        return mapper.writeValueAsString(object);
    }

    public static <T> T fromJson(File file, Class<T> object) throws IOException {
        return mapper.readValue(file, object);
    }

    public static Context getAuthenticationContext(UUID userId) {
        return ReactiveSecurityContextHolder.withAuthentication(
                new JwtAuthenticationToken(
                        Jwt.withTokenValue("JWT")
                                .header("alg", "none")
                                .claim(JwtClaimNames.SUB, userId.toString())
                                .claim("scope", "read")
                                .build(),
                        null,
                        "UserName"));
    }
}
