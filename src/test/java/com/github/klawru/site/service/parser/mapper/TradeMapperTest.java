package com.github.klawru.site.service.parser.mapper;

import com.github.klawru.site.repository.entity.Operation;
import com.github.klawru.site.repository.entity.TradeEntity;
import com.github.klawru.site.service.parser.excel.ParsedTrade;
import com.github.klawru.site.utility.UuidGenerator;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.collections.api.factory.Maps;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

@Slf4j
class TradeMapperTest {
    private final ParsedTradeMapper tradeMapper = ParsedTradeMapper.INSTANCE;
    private final CurrencyMapper currencyMapper = CurrencyMapper.INSTANCE;
    UuidGenerator uuidGenerator = new UuidGenerator();

    @Test
    void mapList() {
        final var expected = createParsedTrade();
        final var tradeDTOList = List.of(expected, expected);
        final var formatters = List.of(DateTimeFormatter.ISO_LOCAL_DATE_TIME).toArray(DateTimeFormatter[]::new);
        //When
        final var result = tradeMapper.map(tradeDTOList, uuidGenerator, UUID.randomUUID());
        //Then
        log.debug("resultList:{}", result);
        assertThat(result)
                .hasSize(4)
                .filteredOn(TradeEntity::getTicker, "TICKER")
                .hasSize(2)
                .extracting(TradeEntity::getSeqId)
                .doesNotHaveDuplicates();
    }

    private ParsedTrade createParsedTrade() {
        return new ParsedTrade(
                LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS),
                LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS),
                "TICKER",
                "EXCHANGE",
                "ISIN",
                "SALE",
                new BigDecimal("10.0"),
                new BigDecimal("100.00"),
                "RUB",
                10,
                new BigDecimal("0.1"),
                "RUB",
                new BigDecimal("0.1"),
                "RUB",
                "NOTE",
                Maps.mutable.empty(),
                1
        );
    }

    @Test
    void mapOne() {
        final var now = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS);
        final var dto = new ParsedTrade(
                LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS),
                LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS),
                "TICKER",
                "EXCHANGE",
                "ISIN",
                "SALE",
                new BigDecimal("100.00"),
                new BigDecimal("100.00"),
                "RUB",
                2,
                new BigDecimal("10.0"),
                "RUB",
                new BigDecimal("0.1"),
                "RUB",
                "NOTE",
                Maps.mutable.empty(),
                1);
        final var formatters = List.of(DateTimeFormatter.ISO_LOCAL_DATE_TIME).toArray(DateTimeFormatter[]::new);
        final var fileId = UUID.randomUUID();
        final TradeEntity expected;
        expected = new TradeEntity(null, null,
                now,
                now,
                "TICKER",
                "EXCHANGE",
                "ISIN",
                Operation.SALE,
                new BigDecimal("100.00"),
                currencyMapper.currencyToInteger("RUB"),
                new BigDecimal("199.90"),
                -2,
                null,
                new BigDecimal("10.0"),
                currencyMapper.currencyToInteger("RUB"),
                new BigDecimal("0.1"),
                currencyMapper.currencyToInteger("RUB"),
                null,
                null,
                fileId,
                "note"
        ).withId(null);
        //When
        final var result = tradeMapper.map(dto, uuidGenerator, fileId).withId(null);
        result.calcHash();
        //Then
        log.debug("resultList:{}", result);
        assertEquals(expected, result);
    }
}