package com.github.klawru.site.service;

import com.github.klawru.site.AbstractIT;
import com.github.klawru.site.repository.DayBalanceRepository;
import com.github.klawru.site.repository.entity.DayBalance;
import com.github.klawru.site.repository.entity.TradesFileInfo;
import com.github.klawru.site.testutil.TestUtils;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.collections.api.factory.Lists;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import reactor.util.context.Context;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;
import java.util.UUID;

import static com.github.klawru.site.testutil.TradeTestUtils.parseJsonTrade;
import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
class DayBalanceCalcServiceIT extends AbstractIT {
    @Autowired
    DayBalanceCalcService calcService;
    @Autowired
    DayBalanceRepository balanceRepository;
    @Autowired
    TradesService tradesService;

    @Test
    @SneakyThrows
    void calcDayBalanceForUser() {
        final var userId = UUID.fromString("ffff522b-540b-4d91-9173-e70f9386ffff");
        final var withAuthentication = TestUtils.getAuthenticationContext(userId);
        uploadTrades(userId, withAuthentication);
        //When
        calcService.calcDayBalanceForUser(userId)
                .contextWrite(withAuthentication)
                .block();
        //Then
        final var result = balanceRepository.findAllByUserId(userId).collectList().block();
        log.info("list={}", result);
        assertThat(result).isNotNull().hasSize(3)
                .allSatisfy(dayBalance -> {
                    assertThat(dayBalance.getRubSum()).isEqualByComparingTo(BigDecimal.valueOf(50));
                    assertThat(dayBalance.getEurSum()).isEqualByComparingTo(BigDecimal.valueOf(50));
                    assertThat(dayBalance.getUsdSum()).isEqualByComparingTo(BigDecimal.valueOf(50));
                    assertThat(dayBalance.getUserId()).isEqualTo(userId);
                })
                .extracting(DayBalance::getDate)
                .containsOnly(
                        LocalDate.of(2020, Month.MAY, 29),
                        LocalDate.of(2020, Month.MAY, 30),
                        LocalDate.of(2020, Month.MAY, 31)
                );
    }

    private void uploadTrades(UUID userId, Context withAuthentication) throws java.io.IOException {
        final var tradeEntityList = Lists.mutable.withAll(parseJsonTrade("trades/test/1_money_trade.json"));
        tradeEntityList.forEach(entity -> {
            entity.setId(UUID.randomUUID());
            entity.setUserId(userId);
        });
        tradesService.saveTrades(tradeEntityList.toImmutable(), new TradesFileInfo(UUID.randomUUID(), "test_uploadTrades"))
                .contextWrite(withAuthentication)
                .block();
    }


}