package com.github.klawru.site.service.parser.excel;

import com.github.klawru.site.AbstractIT;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.util.UUID;

import static com.github.klawru.site.testutil.TestUtils.*;
import static com.github.klawru.site.testutil.TradeTestUtils.parseJsonTrade;
import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
class ExcelParserIT extends AbstractIT {
    @Autowired
    ExcelParser service;
    @Autowired
    ExcelExpressionParserService mapperService;

    static final UUID fileId = UUID.fromString("1ec6655e-cf7a-6aa9-ac8c-3334b54fafa0");


    @Test
    @SneakyThrows
    void parseSberMoney() {
        File sberMoney = getFile("trades/sberOnline/money.xlsx");
        final var sberOnline = mapperService.bankData("SBER_ONLINE").block();
        //When
        final var actual = service
                .parse(sberMoney, fileId, sberOnline)
                .block();


        //Then
        log.debug(asJson(actual));
        final var expected = parseJsonTrade("trades/sberOnline/money.json");
        assertThat(actual)
                .usingRecursiveComparison(recursiveConfig)
                .ignoringFields("id", "parentId")
                .isEqualTo(expected);
    }

    @Test
    @SneakyThrows
    void parseSberFXIM() {
        File sberMoney = getFile("trades/sberOnline/fxim.xlsx");
        final var sber_online = mapperService.bankData("SBER_ONLINE").block();
        //When
        final var actual = service
                .parse(sberMoney, fileId, sber_online)
                .block();
        //Then

        //remove auto generated id
        actual.forEach(entity -> {
            entity.setId(null);
            entity.setParentId(null);
        });
        log.debug(asJson(actual));
        final var expected = parseJsonTrade("trades/sberOnline/fxim.json");
        assertThat(actual)
                .usingRecursiveComparison(recursiveConfig)
                .ignoringFields("id", "parentId")
                .isEqualTo(expected);
    }


    @Test
    @SneakyThrows
    void parseSberTrades() {
        final var excelFile = getFile("trades/sberOnline/trades.xlsx");
        //When
        final var actual = service.parse(excelFile, fileId, mapperService.bankData("SBER_ONLINE").block())
                .block();

        actual.forEach(entity -> {
            entity.setId(null);
            entity.setParentId(null);
        });
        //Then
        //remove auto generated id
        actual.forEach(entity -> {
            entity.setId(null);
            entity.setParentId(null);
        });
        log.debug(asJson(actual));
        final var expected = parseJsonTrade("trades/sberOnline/trades.json");
        assertThat(actual)
                .usingRecursiveComparison(recursiveConfig)
                .ignoringFields("id", "parentId")
                .isEqualTo(expected);
    }

    @Test
    @SneakyThrows
    void parseTinkoff() {
        File tinkoff = getFile("trades/tinkoff/trades.xlsx");
        final var tinkoffOnline = mapperService.bankData("TINKOFF_ONLINE").block();
        //When
        final var actual = service.parse(tinkoff, fileId, tinkoffOnline)
                .block();

        //Then
        //remove auto generated id
        actual.forEach(entity -> {
            entity.setId(null);
            entity.setParentId(null);
        });
        log.debug(asJson(actual));
        final var expected = parseJsonTrade("trades/tinkoff/trades.json");
        assertThat(actual)
                .usingRecursiveComparison(recursiveConfig)
                .ignoringFields("id", "parentId")
                .isEqualTo(expected);
    }
}