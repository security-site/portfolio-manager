package com.github.klawru.site.service.client;

import com.github.klawru.lib.contract.security.v1.types.request.CurrencyRateRequestDTO;
import com.github.klawru.lib.contract.security.v1.types.request.SecurityRequestDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.*;
import com.github.klawru.lib.contract.security.v1.types.responce.currency.CurrencyRateDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.currency.RateHistoryDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.payment.AbstractPaymentDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.payment.DividendDTO;
import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.javamoney.moneta.Money;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.Nonnull;
import java.math.BigDecimal;
import java.time.Clock;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;


public class DbCacheMockClientTest extends DbCacheClient {

    private final Clock clock;

    public DbCacheMockClientTest(Clock clock) {
        super(null);
        this.clock = clock;
    }

    @Override
    public Flux<SecurityDTO> getSecurity(SecurityRequestDTO request) {
        return Mono.fromCallable(() -> new SecurityDTO(request.getTicker(),
                request.getExchange(),
                "",
                "sn" + request.getTicker(),
                "fn" + request.getTicker(),
                "sln" + request.getTicker(),
                "fln" + request.getTicker(),
                MoneyDTO.of(1, 0, "RUB"),
                1,
                LocalDate.of(2010, 1, 1),
                100000L,
                Money.of(BigDecimal.valueOf(10000000L), "RUB"),
                new SecurityTypeDTO(SecurityType.STOCK_SHARES, SecuritySubType.COMMON_SHARE, "STOCK_SHARES"),
                new SectorDTO(SectorType.CHEMISTRY, "CHEMISTRY"),
                false,
                "",
                generateSecurityHistory(request.getFrom(), LocalDate.now(clock)),
                generatePayments(request.getFrom(), LocalDate.now(clock)),
                Instant.now(clock))).flux();
    }

    private List<AbstractPaymentDTO> generatePayments(LocalDate from, LocalDate to) {
        if (from == null || from.toEpochDay() > to.toEpochDay())
            return Collections.emptyList();
        final var paymentDTOS = Lists.mutable.<AbstractPaymentDTO>empty();
        for (long i = from.toEpochDay(); i < to.toEpochDay(); i++) {
            final var date = LocalDate.ofEpochDay(i);
            if (date.getDayOfMonth() == 1)
                paymentDTOS.add(new DividendDTO(date, MoneyDTO.of(10, 0, "RUB")));
        }
        return paymentDTOS;
    }

    private List<HistoryDTO> generateSecurityHistory(LocalDate from, LocalDate to) {
        if (from == null || from.toEpochDay() > to.toEpochDay())
            return Collections.emptyList();
        final var historyDTOS = Lists.mutable.<HistoryDTO>empty();
        for (long i = from.toEpochDay(); i < to.toEpochDay(); i++) {
            final var date = LocalDate.ofEpochDay(i);
            historyDTOS.add(new HistoryDTO(date,
                    BigDecimal.valueOf(100),
                    BigDecimal.valueOf(100),
                    BigDecimal.valueOf(100),
                    BigDecimal.valueOf(100),
                    BigDecimal.valueOf(100),
                    "RUB",
                    MoneyDTO.of(1, 0, "RUB"),
                    BigDecimal.valueOf(100000L),
                    1000L)
            );
        }
        return historyDTOS;
    }

    @Override
    public Mono<SecurityPriceDTO> getSecurityPrice(SecurityRequestDTO request) {
        return Mono.fromCallable(() -> generateSecurityPrice(request));
    }

    private SecurityPriceDTO generateSecurityPrice(SecurityRequestDTO request) {
        return new SecurityPriceDTO(request.getTicker(),
                request.getExchange(),
                LocalDateTime.now(clock),
                BigDecimal.valueOf(100),
                BigDecimal.valueOf(100),
                BigDecimal.valueOf(100),
                BigDecimal.valueOf(100),
                "RUB",
                BigDecimal.TEN.multiply(BigDecimal.valueOf(100L)),
                100L
        );
    }

    @Override
    public Mono<CurrencyRateDTO> getCurrencyRate(CurrencyRateRequestDTO request) {
        return Mono.fromCallable(() -> {
            var rateHistoryDTOS = generateRateHistoryList(request.getFrom(), LocalDate.now(clock));
            return new CurrencyRateDTO(request.getCurrency(), BigDecimal.ONE, rateHistoryDTOS);
        });
    }

    private MutableList<RateHistoryDTO> generateRateHistoryList(LocalDate from, @Nonnull LocalDate to) {
        if (from == null || from.toEpochDay() > to.toEpochDay())
            return null;
        final var historyDTOS = Lists.mutable.<RateHistoryDTO>empty();
        for (long i = from.toEpochDay(); i < to.toEpochDay(); i++) {
            final var date = LocalDate.ofEpochDay(i);
            historyDTOS.add(new RateHistoryDTO(date, BigDecimal.ONE));
        }
        return historyDTOS;
    }
}