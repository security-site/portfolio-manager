package com.github.klawru.site.controllers;

import com.github.kagkarlsson.scheduler.task.TaskInstance;
import com.github.klawru.lib.contract.porfolio.v1.types.request.DayBalanceRequest;
import com.github.klawru.lib.contract.porfolio.v1.types.request.TradeAddRequest;
import com.github.klawru.lib.contract.porfolio.v1.types.request.TradeUpdateRequest;
import com.github.klawru.lib.contract.porfolio.v1.types.request.TradesRequest;
import com.github.klawru.lib.contract.porfolio.v1.types.responce.StockPortfolioDTO;
import com.github.klawru.lib.contract.porfolio.v1.types.responce.TradeDTO;
import com.github.klawru.site.AbstractIT;
import com.github.klawru.site.service.DayBalanceCalcService;
import com.github.klawru.site.utility.TestClock;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.collections.api.list.MutableList;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import reactor.core.publisher.Flux;
import reactor.util.context.Context;

import java.io.File;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

import static com.github.klawru.lib.contract.porfolio.v1.types.responce.Operation.BUY;
import static com.github.klawru.lib.contract.porfolio.v1.types.responce.Operation.SALE;
import static com.github.klawru.site.testutil.TestUtils.*;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.BigDecimalComparator.BIG_DECIMAL_COMPARATOR;
import static org.eclipse.collections.impl.collector.Collectors2.toList;

@Slf4j
class PortfolioControllerIT extends AbstractIT {

    @Autowired
    PortfolioController controller;
    @Autowired
    TestClock clock;
    @Autowired
    DayBalanceCalcService dayBalanceCalcService;
    UUID userId = UUID.fromString("F0000000-0000-0000-0000-00000000000F");
    Context authentication = getAuthenticationContext(userId);

    @BeforeEach
    void setUp() {
        uploadTrades("SBER_ONLINE", "trades/sberOnline/money.xlsx", authentication);
        uploadTrades("SBER_ONLINE", "trades/sberOnline/trades.xlsx", authentication);
    }

    @SneakyThrows
    @Test
    void getPortfolioTest() {
        //When
        List<StockPortfolioDTO> actual = controller.getPortfolio()
                .collectList()
                .contextWrite(authentication)
                .toFuture().get(10, SECONDS);
        //Then
        log.info("{}", actual);
        assertThat(actual)
                .filteredOn(StockPortfolioDTO::getTicker, "$RUB")
                .singleElement()
                .usingRecursiveComparison(recursiveConfig)
                .isEqualTo(new StockPortfolioDTO("$RUB",
                        "CURRENCY",
                        5,
                        3,
                        BigDecimal.ZERO,
                        BigDecimal.valueOf(40439_50, 2),
                        BigDecimal.valueOf(-6985_00, 2),
                        "RUB",
                        BigDecimal.ZERO,
                        BigDecimal.ZERO,
                        "RUB",
                        BigDecimal.ZERO,
                        "XXX",
                        LocalDateTime.parse("2020-05-25T00:00"),
                        LocalDateTime.parse("2020-05-29T09:00")));
    }

    /**
     * @see PortfolioController#getTrades(TradesRequest)
     */
    @Test
    void getTradesTest() {
        //When
        List<TradeDTO> actual = controller.getTrades(TradesRequest.of())
                .contextWrite(authentication)
                .collectList().block();
        //Then
        log.info("{}", actual);
        assertThat(actual)
                .hasSize(12)
                .isSortedAccordingTo(Comparator.comparing(TradeDTO::getDate).reversed())
                .extracting(TradeDTO::getTicker)
                .containsOnly("VTBY", "VTBR", "$RUB", "RU000A101HU5");
        assertThat(actual)
                .filteredOn(TradeDTO::getTicker, "$RUB")
                .hasSize(8)
                .first()
                .returns(Money.of(BigDecimal.valueOf(33454_50, 2), "RUB"), TradeDTO::getSum);
        assertThat(actual)
                .filteredOn(TradeDTO::getTicker, "VTBR")
                .hasSize(1)
                .singleElement()
                .returns(100_000, TradeDTO::getTotalQuantity);
        assertThat(actual)
                .filteredOn(TradeDTO::getTicker, "VTBY")
                .hasSize(2)
                .first()
                .returns(1, TradeDTO::getTotalQuantity);
        assertThat(actual)
                .filteredOn(TradeDTO::getTicker, "RU000A101HU5")
                .hasSize(1)
                .singleElement()
                .returns(1, TradeDTO::getTotalQuantity);

    }

    /**
     * @see PortfolioController#uploadFile
     */
    @Test
    void uploadFileTest() {
        Long moneyTrades = uploadTrades("SBER_ONLINE", "trades/sberOnline/money.xlsx", authentication);
        assertThat(moneyTrades).isEqualTo(0);
        Long trades = uploadTrades("SBER_ONLINE", "trades/sberOnline/trades.xlsx", authentication);
        assertThat(trades).isEqualTo(0);
    }

    /**
     * @see PortfolioController#addTrade
     */
    @Test
    @SneakyThrows
    void addTradeTest() {
        Long uploadedSize = controller.addTrade(new TradeAddRequest("RU000A101HU5",
                        "MISX",
                        LocalDateTime.now(clock),
                        BUY,
                        10,
                        BigDecimal.valueOf(100),
                        null,
                        BigDecimal.TEN,
                        "RUB"))
                .contextWrite(authentication)
                .block();
        //Then
        assertThat(uploadedSize).isEqualTo(1);
        MutableList<TradeDTO> trades = getTrades();
        assertThat(trades)
                .filteredOn(TradeDTO::getTicker, "RU000A101HU5")
                .filteredOn(TradeDTO::getQuantity, 10)
                .singleElement()
                .returns(BUY, TradeDTO::getOperation)
                .returns(11, TradeDTO::getTotalQuantity)
                .returns(Money.of(BigDecimal.valueOf(100), "RUB"), TradeDTO::getPrice)
                .returns(Money.of(BigDecimal.valueOf(1010), "RUB"), TradeDTO::getSum)
                .returns(Money.of(BigDecimal.valueOf(10), "RUB"), TradeDTO::getFee);
        List<StockPortfolioDTO> portfolio = getPortfolio();
        assertThat(portfolio)
                .filteredOn(StockPortfolioDTO::getTicker, "RU000A101HU5")
                .singleElement()
                .extracting(StockPortfolioDTO::getBuyQuantity, StockPortfolioDTO::getBuySumPrice,
                        StockPortfolioDTO::getBuyFee, StockPortfolioDTO::getLastTransaction)
                .usingComparatorForType(BIG_DECIMAL_COMPARATOR, BigDecimal.class)
                .containsExactly(11, BigDecimal.valueOf(1920), BigDecimal.valueOf(20), LocalDateTime.now(clock));
    }

    /**
     * @see PortfolioController#getDayBalance
     */
    @Test
    void getDayBalanceTest() {
        dayBalanceCalcService.calcPortfolio(new TaskInstance<>("rtt", "rtt", userId), null);
        //When
        var actual = controller.getDayBalance(new DayBalanceRequest(null))
                .contextWrite(authentication)
                .collect(toList()).block();

        log.info("getDayBalance='{}'", actual);
        assertThat(actual)
                .hasSize(6);
    }

    /**
     * @see PortfolioController#updateTrades
     */
    @SneakyThrows
    @Test
    void updateTrades() {
        MutableList<TradeDTO> tradeDTOS = getTrades();
        TradeDTO detect = tradeDTOS.detect(each -> each.getTicker().equals("VTBR"));
        //When
        controller.updateTrades(TradeUpdateRequest.builder()
                        .id(detect.getId())
                        .quantity(2000)
                        .build())
                .contextWrite(authentication)
                .block();
        //Then
        MutableList<TradeDTO> changedTrades = getTrades();
        log.info("changedTrades='{}'", changedTrades);
        TradeDTO changedTrade = changedTrades.detect(each -> each.getId().equals(detect.getId()));
        detect.setQuantity(2000);
        detect.setSum(Money.of(BigDecimal.valueOf(125_61, 2), "RUB"));
        detect.setTotalQuantity(2000);
        assertThat(changedTrade)
                .usingRecursiveComparison(recursiveConfig)
                .isEqualTo(detect);
    }

    /**
     * @see PortfolioController#deleteTrades
     */
    @Test
    @SneakyThrows
    void deleteTrades() {
        MutableList<TradeDTO> tradeDTOS = getTrades();
        TradeDTO detect = tradeDTOS.detect(each -> each.getTicker().equals("VTBY") && each.getOperation() == SALE);
        //When
        controller.updateTrades(TradeUpdateRequest.builder()
                        .id(detect.getId())
                        .quantity(5)
                        .build())
                .contextWrite(authentication)
                .block();
        //Then

        //Check trade changes
        MutableList<TradeDTO> changedTrades = getTrades();
        log.info("changedTrades='{}'", changedTrades);
        TradeDTO changedTrade = changedTrades.detect(each -> each.getId().equals(detect.getId()));
        detect.setQuantity(-5);
        detect.setSum(Money.of(BigDecimal.valueOf(495_00, 2), "RUB"));
        detect.setTotalQuantity(0);
        assertThat(changedTrade)
                .usingRecursiveComparison(recursiveConfig)
                .isEqualTo(detect);
        //Check portfolio
        List<StockPortfolioDTO> portfolioDTOList = getPortfolio();
        assertThat(portfolioDTOList)
                .filteredOn(stockPortfolioDTO -> stockPortfolioDTO.getTicker().equals("VTBY"))
                .first()
                .usingRecursiveComparison(recursiveConfig)
                .isEqualTo(new StockPortfolioDTO("VTBY",
                        "MISX",
                        5,
                        5,
                        BigDecimal.ZERO,
                        BigDecimal.valueOf(505),
                        BigDecimal.valueOf(495),
                        "RUB",
                        BigDecimal.valueOf(5),
                        BigDecimal.valueOf(5),
                        "RUB",
                        BigDecimal.ZERO,
                        "XXX",
                        LocalDateTime.parse("2020-05-26T09:00"),
                        LocalDateTime.parse("2020-05-28T09:00")));

    }

    MutableList<TradeDTO> getTrades() throws InterruptedException, java.util.concurrent.ExecutionException, java.util.concurrent.TimeoutException {
        return controller.getTrades(TradesRequest.of())
                .contextWrite(authentication)
                .collect(toList())
                .toFuture().get(10, SECONDS);
    }

    @SneakyThrows
    Long uploadTrades(String bankType, String path, Context authentication) {
        DefaultDataBufferFactory dataBufferFactory = DefaultDataBufferFactory.sharedInstance;
        File sberMoney = getFile(path);
        Flux<DataBuffer> dataBufferFlux = DataBufferUtils.readInputStream(() -> new FileInputStream(sberMoney), dataBufferFactory, 1024 * 10);
        Long uploadedSize = controller.uploadFile(dataBufferFlux, bankType, sberMoney.getName(), sberMoney.length())
                .contextWrite(authentication)
                .block();
        log.info("uploadTradesSize='{}' for file='{}'", uploadedSize, sberMoney.getName());
        return uploadedSize;
    }

    List<StockPortfolioDTO> getPortfolio() throws InterruptedException, java.util.concurrent.ExecutionException, java.util.concurrent.TimeoutException {
        return controller.getPortfolio()
                .collectList()
                .contextWrite(authentication)
                .toFuture().get(10, SECONDS);
    }
}