DROP TABLE IF EXISTS trade;

CREATE TABLE IF NOT EXISTS trade
(
    id                 uuid PRIMARY KEY,
    parent_id          uuid REFERENCES trade (id) ON DELETE CASCADE,

    date               timestamp(6)   NOT NULL,
    settlement_date    timestamp(0),

    ticker             varchar(25)    NOT NULL,
    exchange           varchar(25)    NOT NULL,
    isin               varchar(20),
    operation          varchar(4)     NOT NULL,

    price              numeric(21, 7) NOT NULL,
    price_currency     int            NOT NULL,
    sum                numeric(21, 7),

    quantity           int            NOT NULL,
    total_quantity     int,

    nkd                numeric(21, 7),
    nkd_currency       int,

    fee                numeric(21, 7),
    fee_currency       int,

    hash               int            NOT NULL,
    seq_id             int DEFAULT 0  NOT NULL,
    file_id            UUID           NOT NULL,
    note               varchar(255),

    user_id            UUID,
    created_date       timestamptz,
    last_modified_by   UUID,
    last_modified_date timestamptz,
    info               jsonb
) WITH (FILLFACTOR = 90);

CREATE TABLE IF NOT EXISTS trade_file
(
    id                 uuid PRIMARY KEY,
    filename           varchar(512) NOT NULL,
    user_id            UUID,
    created_date       timestamptz,
    last_modified_by   UUID,
    last_modified_date timestamptz
);

CREATE INDEX IF NOT EXISTS trade_file_id ON trade (user_id, file_id);

CREATE UNIQUE INDEX IF NOT EXISTS trade_uniq_idx
    ON trade (user_id ASC NULLS LAST, CAST(date AS date) ASC NULLS LAST, ticker ASC NULLS LAST, hash ASC NULLS LAST);

CREATE INDEX IF NOT EXISTS trade_user_id_ticker_operation_date_time_index
    ON trade (user_id, ticker, operation, date)
    INCLUDE (quantity,price);

DROP TABLE IF EXISTS trade_balance;

CREATE TABLE IF NOT EXISTS trade_balance
(
    id                 uuid PRIMARY KEY,

    ticker             varchar        NOT NULL,
    exchange           varchar,

    buy_quantity       bigint         NOT NULL DEFAULT 0,
    sale_quantity      bigint         NOT NULL DEFAULT 0,

    average_price      numeric(21, 5) NOT NULL DEFAULT 0,
    buy_sum_price      numeric(21, 5) NOT NULL DEFAULT 0,
    sale_sum_price     numeric(21, 5) NOT NULL DEFAULT 0,
    price_currency     int            NOT NULL,

    buy_fee            numeric(21, 5) NOT NULL DEFAULT 0,
    sale_fee           numeric(21, 5) NOT NULL DEFAULT 0,
    fee_currency       int            NOT NULL DEFAULT 999,

    dividend           bigint         NOT NULL DEFAULT 0,
    dividend_currency  int            NOT NULL DEFAULT 999,

    first_transaction  timestamp(0)   NOT NULL,
    last_transaction   timestamp(0)   NOT NULL,


    user_id            uuid           NOT NULL,
    created_date       timestamp(0)   NOT NULL,
    last_modified_by   uuid           NOT NULL,
    last_modified_date timestamp(0)   NOT NULL
) WITH (FILLFACTOR = 85);


CREATE UNIQUE INDEX IF NOT EXISTS trade_balance_id_idx ON trade_balance (user_id ASC NULLS LAST, ticker ASC NULLS LAST);


DROP TABLE IF EXISTS bank_mapper;
CREATE TABLE IF NOT EXISTS bank_mapper
(
    id                 bigserial PRIMARY KEY,
    bank_id            varchar,
    headers            varchar[],
    start_data         varchar,
    end_data           varchar,
    date               varchar,
    settlement_date    varchar,
    ticker             varchar,
    exchange           varchar,
    isin               varchar,
    operation          varchar,
    price              varchar,
    sum                varchar,
    price_currency     varchar,
    quantity           varchar,
    nkd                varchar,
    nkd_currency       varchar,
    fee                varchar,
    fee_currency       varchar,
    note               varchar,
    additional_data    varchar[],
    context_fillers    varchar[],
    transform_map      varchar,
    date_patterns      varchar[],

    version            int DEFAULT 0,
    user_id            uuid,
    created_date       timestamp(0),
    last_modified_by   uuid,
    last_modified_date timestamp(0)
);


DROP TABLE IF EXISTS currency_code;
CREATE TABLE IF NOT EXISTS currency_code
(
    code             int PRIMARY KEY,
    str              varchar(4),
    fractionalDigits int
);

CREATE UNIQUE INDEX IF NOT EXISTS currency_code_uind ON currency_code (str, code);


INSERT INTO bank_mapper (id,
                         bank_id,
                         headers,
                         start_data,
                         end_data,
                         date,
                         settlement_date,
                         ticker,
                         exchange,
                         isin,
                         operation,
                         price,
                         sum,
                         price_currency,
                         quantity,
                         nkd,
                         nkd_currency,
                         fee,
                         fee_currency,
                         note,
                         additional_data,
                         context_fillers,
                         transform_map,
                         date_patterns,
                         version, user_id, created_date, last_modified_by, last_modified_date)
VALUES (1, 'SBER_ONLINE',
        '{"НомерДоговора", "НомерСделки", "ДатаЗаключения", "ДатаРасчётов", "КодФинансовогоИнструмента", "ТипРынка", "Операция", "Количество", "Цена", "Нкд", "ОбъёмСделки", "Валюта", "Курс", "КомиссияТорговойСистемы", "КомиссияБанка", "ДатаПодачиПоручения", "ДатаИсполненияПоручения", "Сумма", "СуммаЗачисления/списания", "ВалютаОперации", "ЗачислениеНа", "СодержаниеОперации"}',
        'true',
        'false',
        'row[''ДатаЗаключения''] ?: row[''ДатаПодачиПоручения'']',
        'row[''ДатаРасчётов''] ?: row[''ДатаИсполненияПоручения'']',
        'row[''КодФинансовогоИнструмента''] ?: transformMap[row[''ВалютаОперации'']]',
        '''MISX''',
        '''''',
        'transformMap[row[''Операция'']]',
        'row[''Цена''] ?: row[''Сумма'']',
        'row[''СуммаЗачисления/списания''] ?: row[''Сумма'']',
        'row[''Валюта''] ?: row[''ВалютаОперации'']',
        'row[''Количество'']?:''0''', 'row[''Нкд'']',
        'row[''Валюта'']',
        '#asBD(row[''КомиссияБанка''] ?: ''0'').add(#asBD(row[''КомиссияТорговойСистемы''] ?: ''0''))',
        '''RUB''',
        'row[''СодержаниеОперации'']',
        '{"#pair(''import'',''SberOnline'')"}',
        '{}',
        '{''Покупка'':''BUY'',''Продажа'':''SALE'', ''RUB'':''$RUB'',''USD'':''$USD'',''EUR'':''$EUR'', ''Ввод ДС'':''BUY'', ''Вывод ДС'':''SALE'', ''Зачисление купона'':''BUY'', ''Зачисление дивидендов'':''BUY''}',
        '{"yyyy-MM-dd''T''HH:mm:ss"}',
        0,
        '00000000-0000-0000-0000-000000000000', NOW(), '00000000-0000-0000-0000-000000000000', NOW())
ON CONFLICT DO NOTHING;
INSERT INTO bank_mapper (id, bank_id,
                         headers,
                         start_data,
                         end_data,
                         date,
                         settlement_date,
                         ticker,
                         exchange,
                         isin,
                         operation,
                         price,
                         sum,
                         price_currency,
                         quantity,
                         nkd,
                         nkd_currency,
                         fee,
                         fee_currency,
                         note,
                         additional_data,
                         context_fillers,
                         transform_map,
                         date_patterns,
                         version, user_id, created_date, last_modified_by, last_modified_date)
VALUES (2, 'TINKOFF_ONLINE',
        '{"НомерСделки","НомерПоручения","ДатаЗаключения","ДатаРасчетов","Время","ВидСделки","КодАктива","ЦенаЗаЕдиницу","ВалютаЦены","Количество","Нкд","КомиссияБрокера","ВалютаКомиссии","КомиссияБиржи","ВалютаКомиссииБиржи","КомиссияКлир.Центра","ВалютаКомиссииКлир.Центра","Дата","ВремяСовершения","ДатаИсполнения","Операция","СуммаЗачисления","СуммаСписания", "СуммаСделки", "Примечание"}',
        'row.containsValue(''1.1 Информация о совершенных и исполненных сделках на конец отчетного периода'') or row.containsValue(''2. Операции с денежными средствами'')',
        'row.containsValue(''1.2 Информация о неисполненных сделках на конец отчетного периода'') or row.containsValue(''3.1 Движение по ценным бумагам инвестора'')',
        '(row[''ДатаЗаключения''] ?: row[''Дата''] ?: row[''ДатаИсполнения''])+''T''+(row[''Время''] ?: row[''ВремяСовершения''] ?: ''00:00:00'')',
        '(row[''ДатаРасчетов''] ?: row[''ДатаИсполнения'']) +''T00:00:00''',
        'row[''КодАктива''] ?: (transformMap[context[''КодАктива'']])',
        'transformMap[row[''ТорговаяПлощадка'']?: '''']',
        '''''',
        'transformMap[row[''ВидСделки''] ?: row[''Операция''] ?: '''']',
        'row[''ЦенаЗаЕдиницу''] ?: #asBD(row[''СуммаЗачисления'']).sub(#asBD(row[''СуммаСписания'']))',
        'row[''СуммаСделки''] ?: #asBD(row[''СуммаЗачисления'']).sub(#asBD(row[''СуммаСписания'']))',
        'row[''ВалютаЦены''] ?: context[''КодАктива'']',
        'row[''Количество''] ?: ''0''',
        'row[''Нкд'']',
        'row[''ВалютаЦены''] ?: context[''КодАктива'']',
        '#asBD(row[''КомиссияБрокера''] ?: ''0'').add(#asBD(row[''КомиссияБиржи''] ?: ''0'')).add(#asBD(row[''КомиссияКлир.Центра''] ?: ''0''))',
        'row[''ВалютаКомиссии''] ?: row[''Валюта комиссии биржи''] ?: ''RUB''',
        '#cnn(row[''Операция''],row[''Примечание''])', '{}',
        '{"row.containsValue(''RUB'') ? context.put(''КодАктива'',''RUB'') : ''''", "row.containsValue(''USD'') ? context.put(''КодАктива'',''USD'') : ''''", "row.containsValue(''EUR'') ? context.put(''КодАктива'',''EUR'') : ''''", "row.containsValue(''3.1 Движение по ценным бумагам инвестора'') ? context.remove(''КодАктива'') : null"}',
        '{''Покупка'':''BUY'',''Продажа'':''SALE'',''Пополнение счета'':''BUY'', ''Налог'':''SALE'', ''Выплата дивидендов'':''BUY'', ''Вывод средств'':''SALE'', ' ||
        '''МосБиржа'':''MISX'', ''ММВБ'':''MISX'',''СПБ'':''SPBE'', ' ||
        '''RUB'':''$RUB'',''USD'':''$USD'',''EUR'':''$EUR''}',
        '{"dd.MM.yyyy''T''HH:mm:ss", "yyyy-MM-dd''T''HH:mm:ss"}',
        0,
        '00000000-0000-0000-0000-000000000000', NOW(), '00000000-0000-0000-0000-000000000000', NOW())
ON CONFLICT DO NOTHING;


CREATE TABLE IF NOT EXISTS trade_day_balance
(
    id                 bigserial PRIMARY KEY,
    date               date           NOT NULL,
    rub_sum            numeric(21, 5) NOT NULL,
    eur_sum            numeric(21, 5) NOT NULL,
    usd_sum            numeric(21, 5) NOT NULL,

    user_id            uuid,
    created_date       timestamp(0),
    last_modified_by   uuid,
    last_modified_date timestamp(0)
);


CREATE UNIQUE INDEX IF NOT EXISTS trade_day_balance_user_id_idx ON trade_day_balance (user_id ASC NULLS LAST, date ASC NULLS LAST);


CREATE TABLE IF NOT EXISTS scheduled_tasks
(
    task_name            text                     NOT NULL,
    task_instance        text                     NOT NULL,
    task_data            bytea,
    execution_time       timestamp WITH TIME ZONE NOT NULL,
    picked               BOOLEAN                  NOT NULL,
    picked_by            text,
    last_success         timestamp WITH TIME ZONE,
    last_failure         timestamp WITH TIME ZONE,
    consecutive_failures INT,
    last_heartbeat       timestamp WITH TIME ZONE,
    version              BIGINT                   NOT NULL,
    PRIMARY KEY (task_name, task_instance)
)