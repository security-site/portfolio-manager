package com.github.klawru.site.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.util.unit.DataSize;
import org.springframework.web.cors.CorsConfiguration;

@ConfigurationProperties(
        prefix = "application",
        ignoreUnknownFields = false
)
@RefreshScope
@Data
public class ApplicationProperties {
    private Http http = new Http();
    private ParseConfiguration fileParse = new ParseConfiguration();
    private LokiConfig loki;
    private FeatureFlag featureFlag;

    @Data
    public static class Http {

        private Cache cache = new Cache();
        @NestedConfigurationProperty
        private CorsConfiguration cors = new CorsConfiguration();

        @Data
        public static class Cache {
            int timeToLiveInDays = 1461; // 4 years (including leap day)
        }
    }

    @Data
    public static class ParseConfiguration {
        private DataSize inMemorySize = DataSize.ofKilobytes(512);
        private DataSize maxSize = DataSize.ofMegabytes(10);
    }

    @Data
    public static class LokiConfig {
        String host;
        String port;
    }

    @Data
    public static class FeatureFlag {
        boolean jdbcBatch;
    }
}
