package com.github.klawru.site.config;

import com.github.kagkarlsson.scheduler.task.ExecutionContext;
import com.github.kagkarlsson.scheduler.task.Task;
import com.github.kagkarlsson.scheduler.task.helper.Tasks;
import com.github.kagkarlsson.scheduler.task.schedule.Schedules;
import com.github.klawru.site.repository.StockPortfolioRepository;
import com.github.klawru.site.service.DayBalanceCalcService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.UUID;

@Configuration
@RequiredArgsConstructor
public class TaskConfig {
    private final StockPortfolioRepository portfolioRepository;
    private final Clock clock;

    @Bean
    Task<Void> portfolioCalcTask(Task<UUID> calkPortfolioFor) {
        return Tasks.recurring(
                        "portfolioCalc",
                        Schedules.daily(ZoneId.of("Europe/Moscow"), LocalTime.of(1, 0)))
                .execute((taskInstance, executionContext) -> createTaskForAllPortfolio(executionContext, calkPortfolioFor));
    }

    @Bean
    Task<Void> calcNowEveryOne(DayBalanceCalcService calcService, Task<UUID> calkPortfolioFor) {
        return Tasks.oneTime("calcNowEveryOne", Void.class).execute(
                (taskInstance, executionContext) -> createTaskForAllPortfolio(executionContext, calkPortfolioFor));
    }

    @Bean
    Task<UUID> calkPortfolioForUser(DayBalanceCalcService calcService) {
        return Tasks.oneTime("calkPortfolioForUser", UUID.class)
                .execute(calcService::calcPortfolio);
    }

    public void createTaskForAllPortfolio(ExecutionContext context, Task<UUID> calkPortfolioFor) {
        final var now = Instant.now(clock);
        portfolioRepository.getAllUserId()
                .doOnNext(uuid -> context.getSchedulerClient()
                        .schedule(calkPortfolioFor.instance("calkPortfolioForUser" + uuid.getUserId(), uuid.getUserId()), now)
                )
                .blockLast();
    }

}
