package com.github.klawru.site.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ConditionalOnProperty("app.featureFlag.jdbcBatch")
@Import(DataSourceAutoConfiguration.class)
public class JdbcConfig {

}
