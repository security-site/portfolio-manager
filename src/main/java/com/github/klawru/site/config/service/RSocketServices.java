package com.github.klawru.site.config.service;

import io.rsocket.loadbalance.LoadbalanceTarget;
import io.rsocket.loadbalance.RoundRobinLoadbalanceStrategy;
import io.rsocket.transport.netty.client.TcpClientTransport;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.rsocket.RSocketRequester;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.List;

import static com.github.klawru.lib.contract.config.ClientDiscovery.DBCACHE;

@Configuration
@RequiredArgsConstructor
public class RSocketServices {
    private final DiscoveryClient discoveryClient;

    @Bean
    @Qualifier(DBCACHE)
    public RSocketRequester createExchangeClientsRequester(RSocketRequester.Builder builder, DiscoveryClient discoveryClient) {
        return builder
                .transports(targets(DBCACHE, discoveryClient), new RoundRobinLoadbalanceStrategy());
    }

    public Flux<List<LoadbalanceTarget>> targets(String serviceName, DiscoveryClient discoveryClient) {
        return Mono.fromSupplier(() -> discoveryClient.getInstances(serviceName))
                .map(this::toLoadBalanceTarget)
                .repeatWhen(longFlux -> longFlux.delayElements(Duration.ofSeconds(10)));
    }

    private List<LoadbalanceTarget> toLoadBalanceTarget(List<ServiceInstance> rSocketServers) {
        return rSocketServers.stream()
                .map(server -> LoadbalanceTarget.from(server.getHost() + server.getPort(), TcpClientTransport.create(server.getHost(), server.getPort() + 1)))
                .toList();
    }

}
