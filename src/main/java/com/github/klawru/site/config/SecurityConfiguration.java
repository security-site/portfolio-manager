package com.github.klawru.site.config;

import com.github.klawru.site.security.KeycloakJwtAuthenticationConverter;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.oauth2.client.*;
import org.springframework.security.oauth2.client.registration.ReactiveClientRegistrationRepository;
import org.springframework.security.oauth2.jwt.ReactiveJwtDecoder;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.oauth2.server.resource.authentication.JwtReactiveAuthenticationManager;
import org.springframework.security.oauth2.server.resource.authentication.ReactiveJwtAuthenticationConverterAdapter;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.security.web.server.context.NoOpServerSecurityContextRepository;

import java.time.Clock;

@EnableWebFluxSecurity
@RequiredArgsConstructor
public class SecurityConfiguration {
    @Bean
    public Clock clock() {
        return Clock.systemDefaultZone();
    }

    @Bean
    protected JwtReactiveAuthenticationManager authenticationManager(
            ReactiveJwtDecoder jwtDecoder,
            ReactiveJwtAuthenticationConverterAdapter authenticationConverterAdapter) {
        final var manager = new JwtReactiveAuthenticationManager(jwtDecoder);
        manager.setJwtAuthenticationConverter(authenticationConverterAdapter);
        return manager;
    }

    @Bean
    protected ReactiveJwtAuthenticationConverterAdapter keycloakJwtAuthenticationConverter() {
        final var jwtAuthenticationConverter = new JwtAuthenticationConverter();
        jwtAuthenticationConverter.setPrincipalClaimName("name");
        jwtAuthenticationConverter.setJwtGrantedAuthoritiesConverter(new KeycloakJwtAuthenticationConverter());
        return new ReactiveJwtAuthenticationConverterAdapter(jwtAuthenticationConverter);
    }

    @Bean
    public SecurityWebFilterChain springSecurityFilterChain(ServerHttpSecurity http, ReactiveAuthenticationManager authenticationManager,
                                                            ReactiveJwtAuthenticationConverterAdapter jwtAuthenticationConverter) {
        http
                .csrf().disable()
                .cors().and()
                .oauth2ResourceServer(oauth2ResourceServer -> oauth2ResourceServer
                        .jwt()
                        .jwtAuthenticationConverter(jwtAuthenticationConverter)
                        .authenticationManager(authenticationManager)
                )
                .securityContextRepository(NoOpServerSecurityContextRepository.getInstance())
                .authorizeExchange(exchanges -> exchanges
                        .pathMatchers(HttpMethod.OPTIONS, "**").permitAll()
                        .pathMatchers("/actuator/health").permitAll()
                        .pathMatchers("/actuator/prometheus").permitAll()
                        .pathMatchers("/rsocket").permitAll()
                        .pathMatchers("/swagger-ui.html").permitAll()
                        .pathMatchers("/webjars/swagger-ui/**").permitAll()
                        .pathMatchers("/v3/**").permitAll()
                        .anyExchange().authenticated()
                )
                .logout().disable()
                .formLogin().disable();
        return http.build();
    }

    @Bean
    ReactiveOAuth2AuthorizedClientManager authorizedClientManager(
            ReactiveClientRegistrationRepository registrationRepository,
            ReactiveOAuth2AuthorizedClientService authorizedClientService) {
        var manager = new AuthorizedClientServiceReactiveOAuth2AuthorizedClientManager(registrationRepository, authorizedClientService);
        var authorizedClientProvider = new ClientCredentialsReactiveOAuth2AuthorizedClientProvider();
        manager.setAuthorizedClientProvider(authorizedClientProvider);
        return manager;
    }

    @Bean
    OAuth2AuthorizeRequest auth2AuthorizeRequest() {
        return OAuth2AuthorizeRequest.withClientRegistrationId("keycloak").
                principal("backend").
                build();
    }

}
