package com.github.klawru.site.config;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.OAuthFlow;
import io.swagger.v3.oas.models.security.OAuthFlows;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.javamoney.moneta.Money;
import org.springdoc.core.GroupedOpenApi;
import org.springdoc.core.SpringDocUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.codec.ServerCodecConfigurer;
import org.springframework.http.codec.multipart.DefaultPartHttpMessageReader;
import org.springframework.http.codec.multipart.MultipartHttpMessageReader;
import org.springframework.web.reactive.config.WebFluxConfigurer;

import javax.money.MonetaryAmount;

/**
 * Configuration of web application with Servlet 3.0 APIs.
 */
@Slf4j
@Configuration
@RequiredArgsConstructor
public class WebConfigurer implements WebFluxConfigurer {

    private final ApplicationProperties applicationProperties;

    static {
        SpringDocUtils.getConfig()
                .replaceWithClass(MonetaryAmount.class, org.springdoc.core.converters.models.MonetaryAmount.class)
                .replaceWithClass(Money.class, org.springdoc.core.converters.models.MonetaryAmount.class);
    }

    @Bean
    public OpenAPI customOpenAPI(@Value("${spring.security.oauth2.client.provider.keycloak.issuer-uri}") String jwtUrl) {
        return new OpenAPI()
                .components(new Components()
                        .addSecuritySchemes("OAuth2", new SecurityScheme()
                                .type(SecurityScheme.Type.OAUTH2)
                                .scheme("bearer")
                                .bearerFormat("jwt")
                                .in(SecurityScheme.In.HEADER)
                                .name("Authorization")
                                .flows(new OAuthFlows().implicit(new OAuthFlow()
                                        .authorizationUrl(jwtUrl + "/protocol/openid-connect/auth")
                                ))))
                .addSecurityItem(new SecurityRequirement().addList("OAuth2"))
                .info(new Info().title("Portfolio manager API").version("0.1"));
    }

    @Bean
    public GroupedOpenApi storeOpenApi() {
        String[] paths = {"/**"};
        return GroupedOpenApi.builder().group("porfolio").pathsToMatch(paths)
                .build();
    }

    @Override
    public void configureHttpMessageCodecs(ServerCodecConfigurer configurer) {
        //Фикс ошибки при загрузке файлов
        //На 8Кб при загрузке файла может выбросить ошибку
        DefaultPartHttpMessageReader partReader = new DefaultPartHttpMessageReader();
        partReader.setMaxHeadersSize(1024 * 16); // 16 KiB, default is 8 KiB
        partReader.setEnableLoggingRequestDetails(true);

        MultipartHttpMessageReader multipartReader = new MultipartHttpMessageReader(partReader);
        multipartReader.setEnableLoggingRequestDetails(true);

        configurer.defaultCodecs().multipartReader(multipartReader);
    }

}
