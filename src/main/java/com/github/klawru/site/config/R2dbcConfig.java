package com.github.klawru.site.config;

import com.github.klawru.site.security.SecurityUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.ReactiveAuditorAware;
import org.springframework.data.mapping.callback.ReactiveEntityCallbacks;
import org.springframework.data.r2dbc.config.EnableR2dbcAuditing;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.UUID;

@Configuration
@EnableR2dbcAuditing
@EnableTransactionManagement
public class R2dbcConfig {

    @Bean
    public ReactiveAuditorAware<UUID> auditorAware() {
        return SecurityUtils::getCurrentUserId;
    }

    @Bean
    public ReactiveEntityCallbacks reactiveEntityCallbacks(ApplicationContext applicationContext) {
        return ReactiveEntityCallbacks.create(applicationContext);
    }


}
