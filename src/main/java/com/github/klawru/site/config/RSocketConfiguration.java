package com.github.klawru.site.config;

import org.springframework.boot.rsocket.messaging.RSocketStrategiesCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.rsocket.EnableRSocketSecurity;
import org.springframework.security.config.annotation.rsocket.RSocketSecurity;
import org.springframework.security.rsocket.core.PayloadSocketAcceptorInterceptor;
import org.springframework.security.rsocket.metadata.BearerTokenAuthenticationEncoder;
import org.springframework.util.MimeType;

import static com.github.klawru.lib.contract.porfolio.v1.PortfolioInfo.*;
import static io.rsocket.metadata.WellKnownMimeType.MESSAGE_RSOCKET_AUTHENTICATION;

@Configuration
@EnableRSocketSecurity
public class RSocketConfiguration {

    public static final MimeType AUTHENTICATION_MIME_TYPE = MimeType.valueOf(MESSAGE_RSOCKET_AUTHENTICATION.getString());

    @Bean
    RSocketStrategiesCustomizer BearerTokenAuthenticationEncoder() {
        return strategies -> {
            strategies.encoder(new BearerTokenAuthenticationEncoder());
            strategies.metadataExtractorRegistry(metadataExtractorRegistry -> {
                metadataExtractorRegistry.metadataToExtract(MimeType.valueOf(MIME_FILE_SIZE), String.class, MIME_FILE_SIZE);
                metadataExtractorRegistry.metadataToExtract(MimeType.valueOf(MIME_FILE_NAME), String.class, MIME_FILE_NAME);
                metadataExtractorRegistry.metadataToExtract(MimeType.valueOf(MIME_BANK_IMPORT), String.class, MIME_BANK_IMPORT);
            });
        };
    }

    @Bean
    PayloadSocketAcceptorInterceptor rsocketSecurity(RSocketSecurity rsocket, ReactiveAuthenticationManager manager) {
        rsocket
                .authenticationManager(manager)
                .authorizePayload(authorize ->
                        authorize
                                .anyRequest().authenticated()
                                .anyExchange().permitAll()
                )
                .jwt(Customizer.withDefaults());
        return rsocket.build();
    }

}
