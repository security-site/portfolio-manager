package com.github.klawru.site.security;

import lombok.Value;

import java.util.UUID;

@Value
public class SimpleUser {
    String username;
    UUID id;
}
