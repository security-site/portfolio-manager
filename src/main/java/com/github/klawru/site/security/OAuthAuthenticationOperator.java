package com.github.klawru.site.security;

import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.oauth2.client.OAuth2AuthorizeRequest;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.ReactiveOAuth2AuthorizedClientManager;
import org.springframework.security.oauth2.core.AbstractOAuth2Token;
import org.springframework.security.oauth2.server.resource.BearerTokenAuthenticationToken;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.context.Context;

import java.util.NoSuchElementException;

/**
 * Сервис позволяет получить токен для межсервисных запросов.
 */
@Service
@RequiredArgsConstructor
public class OAuthAuthenticationOperator {

    private final ReactiveOAuth2AuthorizedClientManager authorizedClientManager;
    private final ReactiveAuthenticationManager authenticationManager;
    private final OAuth2AuthorizeRequest clientRequest;


    /**
     * Дополняет контекст реактивной последовательности авторизацией.
     *
     * @param mono Mono который должен быть выполнен с авториазацией
     * @return возвращает Mono c авторизацией
     * @throws NoSuchElementException         если не была получена аунтификация
     * @throws AuthenticationServiceException если возникла проблема с получением аунтификации
     */
    public <T> Mono<T> withAuthentication(Mono<T> mono) {
        return getAuthenticationContext()
                .flatMap(mono::contextWrite);
    }

    public <T> Flux<T> withAuthentication(Flux<T> flux) {
        return getAuthenticationContext()
                .flatMapMany(flux::contextWrite);
    }

    private Mono<Context> getAuthenticationContext() {
        return authorizedClientManager.authorize(clientRequest)
                .map(OAuth2AuthorizedClient::getAccessToken)
                .map(AbstractOAuth2Token::getTokenValue)
                .switchIfEmpty(Mono.error(() -> new AuthenticationServiceException("Нет токена")))
                .flatMap(jwt -> authenticationManager.authenticate(new BearerTokenAuthenticationToken(jwt)))
                .map(ReactiveSecurityContextHolder::withAuthentication)
                .single();
    }

}
