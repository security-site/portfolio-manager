package com.github.klawru.site.security;

import lombok.extern.slf4j.Slf4j;
import org.eclipse.collections.api.list.ImmutableList;
import org.eclipse.collections.impl.factory.Lists;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.util.StringUtils;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

@Slf4j
public class KeycloakJwtAuthenticationConverter implements Converter<Jwt, Collection<GrantedAuthority>> {
    private static final ImmutableList<String> WELL_KNOWN_AUTHORITIES_CLAIM_NAMES = Lists.immutable.of("role", "scope", "scp");

    @Override
    public Collection<GrantedAuthority> convert(@Nonnull Jwt jwt) {
        Collection<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        for (String authority : getAuthorities(jwt)) {
            grantedAuthorities.add(new SimpleGrantedAuthority(authority));
        }
        return grantedAuthorities;
    }

    private String getAuthoritiesClaimName(Jwt jwt) {
        return WELL_KNOWN_AUTHORITIES_CLAIM_NAMES
                .detect(jwt::hasClaim);
    }

    private Collection<String> getAuthorities(Jwt jwt) {
        String claimName = getAuthoritiesClaimName(jwt);
        if (claimName == null) {
            log.trace("Returning no authorities since could not find any claims that might contain scopes");
            return Collections.emptyList();
        }
        if (log.isTraceEnabled()) {
            log.trace("Looking for scopes in claim {}", claimName);
        }
        Object authorities = jwt.getClaim(claimName);
        if (authorities instanceof String) {
            if (StringUtils.hasText((String) authorities)) {
                return Arrays.asList(((String) authorities).split(" "));
            }
            return Collections.emptyList();
        }
        if (authorities instanceof Collection && containString((Collection<?>) authorities)) {
            return (Collection<String>) authorities;
        }
        return Collections.emptyList();
    }

    private boolean containString(Collection<?> authorities) {
        return authorities.stream().allMatch(str -> str instanceof String);
    }
}
