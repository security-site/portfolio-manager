package com.github.klawru.site.error;

public class InternalServerError extends RuntimeException {
    public InternalServerError(String message) {
        super(message);
    }

    public InternalServerError(String message, Throwable cause) {
        super(message, cause);
    }
}
