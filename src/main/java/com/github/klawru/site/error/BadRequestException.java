package com.github.klawru.site.error;

public class BadRequestException extends RuntimeException {
    // TODO: 22.01.2022 i18n
    public BadRequestException(String message) {
        super(message);
    }

    public BadRequestException(String message, Throwable cause) {
        super(message, cause);
    }
}
