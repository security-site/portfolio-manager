package com.github.klawru.site.controllers;

import com.github.klawru.lib.contract.porfolio.v1.types.request.*;
import com.github.klawru.lib.contract.porfolio.v1.types.responce.DayBalanceDTO;
import com.github.klawru.lib.contract.porfolio.v1.types.responce.StockPortfolioDTO;
import com.github.klawru.lib.contract.porfolio.v1.types.responce.TradeDTO;
import com.github.klawru.site.service.DayBalanceService;
import com.github.klawru.site.service.PortfolioService;
import com.github.klawru.site.service.TradesService;
import com.github.klawru.site.service.TradesUploadService;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

import static com.github.klawru.lib.contract.config.ClientDiscovery.PORTFOLIO;
import static com.github.klawru.lib.contract.porfolio.v1.PortfolioInfo.*;


@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = PORTFOLIO + "/" + PORTFOLIO_INFO, produces = MediaType.APPLICATION_JSON_VALUE)
public class PortfolioWebController {

    private final TradesUploadService tradesUploadService;
    private final PortfolioService portfolioService;
    private final TradesService tradesService;
    private final DayBalanceService dayBalanceService;


    @GetMapping(value = GET_PORTFOLIO)
    public Flux<StockPortfolioDTO> getPortfolio() {
        return portfolioService.getPortfolio();
    }

    @PostMapping(GET_DAY_BALANCE)
    public Flux<DayBalanceDTO> getDayBalance(DayBalanceRequest request) {
        return dayBalanceService.getDayBalance(request);
    }

    @PostMapping(value = GET_TRADES)
    public Flux<TradeDTO> getTrades(@Valid @RequestBody TradesRequest request) {
        return tradesService.getTrades(request);
    }

    @PostMapping(ADD_TRADES)
    public Mono<Long> addTrade(@Valid TradeAddRequest request) {
        return tradesService.addTrade(request);
    }

    @PostMapping(UPDATE_TRADES)
    public Mono<Void> updateTrades(@Valid TradeUpdateRequest request) {
        return tradesService.updateTrade(request);
    }

    @PostMapping(DELETE_TRADES)
    public Mono<Void> deleteTrades(@Valid TradeDeleteRequest request) {
        return tradesService.deleteTrades(request);
    }

    @PostMapping(value = UPLOAD_FILE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public Mono<Long> uploadFile(@RequestPart(name = "file") Mono<FilePart> filePartMono,
                                 @RequestPart(name = "bankImport") String bankImportType,
                                 @Parameter(hidden = true)
                                 @RequestHeader(value = "Content-Length", required = false) Long contentLength) {
        return filePartMono.flatMap(filePart ->
                tradesUploadService.uploadFile(filePart.content(), bankImportType, filePart.filename(), contentLength));
    }
}
