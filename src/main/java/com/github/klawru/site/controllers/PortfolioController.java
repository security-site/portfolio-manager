package com.github.klawru.site.controllers;

import com.github.klawru.lib.contract.porfolio.v1.PortfolioInfo;
import com.github.klawru.lib.contract.porfolio.v1.types.request.*;
import com.github.klawru.lib.contract.porfolio.v1.types.responce.DayBalanceDTO;
import com.github.klawru.lib.contract.porfolio.v1.types.responce.StockPortfolioDTO;
import com.github.klawru.lib.contract.porfolio.v1.types.responce.TradeDTO;
import com.github.klawru.site.service.DayBalanceService;
import com.github.klawru.site.service.PortfolioService;
import com.github.klawru.site.service.TradesService;
import com.github.klawru.site.service.TradesUploadService;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Controller;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.Nonnull;
import javax.validation.Valid;

import static com.github.klawru.lib.contract.config.ClientDiscovery.PORTFOLIO;
import static com.github.klawru.lib.contract.porfolio.v1.PortfolioInfo.PORTFOLIO_INFO;


@Controller
@RequiredArgsConstructor
@MessageMapping(PORTFOLIO + "." + PORTFOLIO_INFO + ".")
public class PortfolioController implements PortfolioInfo {

    private final TradesUploadService tradesUploadService;
    private final PortfolioService portfolioService;
    private final TradesService tradesService;
    private final DayBalanceService dayBalanceService;


    @Override
    @MessageMapping(GET_PORTFOLIO)
    public Flux<StockPortfolioDTO> getPortfolio() {
        return portfolioService.getPortfolio();
    }

    @Override
    @MessageMapping(GET_DAY_BALANCE)
    public Flux<DayBalanceDTO> getDayBalance(@Valid DayBalanceRequest request) {
        return dayBalanceService.getDayBalance(request);
    }

    @Override
    @MessageMapping(GET_TRADES)
    public Flux<TradeDTO> getTrades(@Valid TradesRequest request) {
        return tradesService.getTrades(request);
    }

    @Override
    @MessageMapping(ADD_TRADES)
    public Mono<Long> addTrade(@Valid TradeAddRequest request) {
        return tradesService.addTrade(request);
    }

    @Override
    @MessageMapping(UPDATE_TRADES)
    public Mono<Void> updateTrades(@Valid TradeUpdateRequest request) {
        return tradesService.updateTrade(request);
    }

    @Override
    @MessageMapping(DELETE_TRADES)
    public Mono<Void> deleteTrades(@Valid TradeDeleteRequest request) {
        return tradesService.deleteTrades(request);
    }

    @MessageMapping(UPLOAD_FILE)
    public Mono<Long> uploadFile(@Nonnull @Payload Flux<DataBuffer> filePartMono,
                                 @Nonnull @Header(MIME_BANK_IMPORT) String bankImportType,
                                 @Nonnull @Header(MIME_FILE_NAME) String fileName,
                                 @Header(value = MIME_FILE_SIZE, required = false) Long contentLength) {
        return tradesUploadService.uploadFile(filePartMono, bankImportType, fileName, contentLength);
    }

}
