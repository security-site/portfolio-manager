package com.github.klawru.site.repository.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import javax.annotation.Nonnull;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.util.UUID;

@Table("trade_day_balance")
@Getter
@Setter
@ToString
@AllArgsConstructor
public class DayBalance implements Persistable<Long> {
    @Id
    @With
    Long id;
    @Nonnull
    LocalDate date;
    /**
     * Средняя цена открытых позиций
     */
    BigDecimal rubSum;
    BigDecimal eurSum;
    BigDecimal usdSum;

    @Column("user_id")
    @JsonIgnore
    private UUID userId;

    @Column("created_date")
    @JsonIgnore
    private Instant createdDate;

    @Column("last_modified_by")
    @JsonIgnore
    private UUID lastModifiedBy;

    @Column("last_modified_date")
    @JsonIgnore
    private Instant lastModifiedDate;

    @Override
    public boolean isNew() {
        return id == null;
    }


}
