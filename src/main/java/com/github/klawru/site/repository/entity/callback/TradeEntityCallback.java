package com.github.klawru.site.repository.entity.callback;

import com.github.klawru.site.repository.entity.TradeEntity;
import com.github.klawru.site.utility.UuidGenerator;
import lombok.RequiredArgsConstructor;
import org.reactivestreams.Publisher;
import org.springframework.data.r2dbc.mapping.event.BeforeConvertCallback;
import org.springframework.data.relational.core.sql.SqlIdentifier;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
@RequiredArgsConstructor
public class TradeEntityCallback implements BeforeConvertCallback<TradeEntity> {

    private final UuidGenerator uuidGenerator;

    @Override
    public Publisher<TradeEntity> onBeforeConvert(TradeEntity entity, SqlIdentifier table) {
        return Mono.fromCallable(() -> {
            entity.onBeforeSave(uuidGenerator);
            return entity;
        });
    }
}
