package com.github.klawru.site.repository;


import com.github.klawru.site.repository.entity.StockPortfolio;
import com.github.klawru.site.repository.entity.projection.QuantityAveragePriceDTO;
import com.github.klawru.site.repository.entity.projection.StockPortfolioUser;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import reactor.core.publisher.Flux;

import java.util.Collection;
import java.util.UUID;


public interface StockPortfolioRepository extends R2dbcRepository<StockPortfolio, UUID> {

    Flux<StockPortfolio> findByUserId(UUID userId);

    @Query("SELECT p.* FROM trade_balance p WHERE p.user_id=:userId " +
            "AND (p.ticker, p.exchange) IN (SELECT UNNEST(ARRAY[:ticker ]), UNNEST(ARRAY[:exchange ]))")
    Flux<StockPortfolio> findByUserIdAndTickerIn(UUID userId, Collection<String> ticker, Collection<String> exchange);

    String calcQuantityAndAveragePrice = """
            WITH securityKey AS ( --список искомых бумаг
                  SELECT UNNEST(ARRAY[:ticker ]) ticker, UNNEST(ARRAY[:exchange ]) exchange
            ),
                current_quntity AS NOT MATERIALIZED ( --Последняя сделка
                  SELECT sk.ticker, sk.exchange, lastTransaction.total_quantity, lastTransaction.sum
                  FROM securityKey sk
                           CROSS JOIN LATERAL ( SELECT t.total_quantity, t.sum
                                                FROM trade t
                                                WHERE t.user_id = :userId
                                                  AND t.ticker = sk.ticker
                                                  AND t.exchange = sk.exchange
                                                ORDER BY t.date DESC, t.id DESC
                                                LIMIT 1 ) AS lastTransaction
            ),
                 reverse_sum_qnt AS (
                     SELECT ticker,
                            exchange,
                            date,
                            quantity,
                            price,
                            SUM(quantity) OVER (PARTITION BY ticker ORDER BY date DESC) AS reverseSumQnt
                     FROM trade
                     WHERE user_id = :userId
                       AND operation = 'BUY'
                 ),
                 last_tran_datetime AS (
                     SELECT cq.ticker,
                            cq.exchange,
                            cq.total_quantity,
                            cq.sum,
                            LastPartialStock.date,
                            LastPartialStock.quantity,
                            LastPartialStock.reverseSumQnt,
                            cq.total_quantity - LastPartialStock.reverseSumQnt +
                            LastPartialStock.quantity AS UseThisStock
                     FROM current_quntity AS cq
                              CROSS JOIN LATERAL (
                         SELECT rsq.date,
                                rsq.quantity,
                                rsq.reverseSumQnt
                         FROM reverse_sum_qnt AS rsq
                         WHERE rsq.ticker = cq.ticker
                           AND rsq.exchange = cq.exchange
                           AND rsq.reverseSumQnt >= cq.total_quantity
                         ORDER BY rsq.date DESC
                         LIMIT 1
                         ) AS LastPartialStock
                     WHERE LEFT(cq.ticker,1)<>'$'
                 )
            SELECT ltd.ticker,
                   ltd.exchange,
                   ltd.total_quantity,
                   SUM(CASE
                           WHEN t.date = ltd.date  THEN ltd.UseThisStock
                           ELSE t.quantity
                           END * t.Price/NULLIF(ltd.total_quantity,0)
                   ) AS average_price
            FROM last_tran_datetime AS ltd
                     INNER JOIN trade AS t
                                ON t.ticker = ltd.ticker
                                    AND t.exchange = ltd.exchange
                                    AND t.user_id = :userId
                                    AND t.date >= ltd.date
                                    AND t.operation = 'BUY'
            GROUP BY ltd.ticker, ltd.exchange, ltd.total_quantity
            UNION ALL
            SELECT  cq.ticker,
                    cq.exchange,
                    cq.total_quantity,
                    cq.sum AS average_price
            FROM current_quntity cq
            WHERE LEFT(cq.ticker,1)='$'
            """;

    @Query(calcQuantityAndAveragePrice)
    Flux<QuantityAveragePriceDTO> calcAveragePrice(UUID userId, Collection<String> ticker, Collection<String> exchange);

    @Query("SELECT user_id FROM trade_balance GROUP BY user_id")
    Flux<StockPortfolioUser> getAllUserId();
}
