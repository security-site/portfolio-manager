package com.github.klawru.site.repository.entity;

/**
 * Ключ по которому можно точно определить бумагу
 */
public interface SecurityKey {
    String getTicker();

    String getExchange();

    static SecurityKey of(String ticker, String exchange) {
        return new SecurityKeyImpl(ticker, exchange);
    }
}
