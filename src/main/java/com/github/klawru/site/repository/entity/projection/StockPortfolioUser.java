package com.github.klawru.site.repository.entity.projection;

import lombok.Value;

import java.util.UUID;

@Value
public class StockPortfolioUser {
    UUID userId;
}
