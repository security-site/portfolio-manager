package com.github.klawru.site.repository.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.github.klawru.site.utility.TradeUtility;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

@Table("trade_balance")
@Data
@AllArgsConstructor
@NoArgsConstructor(staticName = "create")
@EqualsAndHashCode(of = "id", callSuper = false)
public class StockPortfolio extends AbstractAuditingEntity<UUID> implements SecurityKeyEntity {
    @Id
    @With
    UUID id;

    String ticker;
    String exchange;

    Integer buyQuantity;
    Integer saleQuantity;
    /**
     * Средняя цена открытых позиций
     */
    BigDecimal averagePrice;
    BigDecimal buySumPrice;
    BigDecimal saleSumPrice;
    Integer priceCurrency;

    BigDecimal buyFee;
    BigDecimal saleFee;
    Integer feeCurrency;

    BigDecimal dividend;
    Integer dividendCurrency;

    LocalDateTime firstTransaction;
    LocalDateTime lastTransaction;

    public static StockPortfolio createNew(UUID id, String ticker, String exchange, Integer priceCurrency, Integer feeCurrency, LocalDateTime firstTransaction) {
        return new StockPortfolio(id, ticker, exchange, 0, 0, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, priceCurrency,
                BigDecimal.ZERO, BigDecimal.ZERO, feeCurrency, BigDecimal.ZERO, null, firstTransaction, firstTransaction);
    }

    public void addBuyQuantity(int buyQuantity) {
        this.buyQuantity += buyQuantity;
    }

    public void addSellQuantity(int saleQuantity) {
        this.saleQuantity += saleQuantity;
    }

    public void addBuySumPrice(BigDecimal buySum) {
        this.buySumPrice = this.buySumPrice.add(buySum);
    }

    public void addSaleSumPrice(BigDecimal saleSumPrice) {
        this.saleSumPrice = this.saleSumPrice.add(saleSumPrice);
    }

    public void addSaleFee(BigDecimal fee) {
        this.saleFee = this.saleFee.add(fee);
    }

    public void addBuyFee(BigDecimal buyFee) {
        this.buyFee = this.buyFee.add(buyFee);
    }

    public int getQuantity() {
        return buyQuantity - saleQuantity;
    }

    public boolean isMoney() {
        return TradeUtility.isMoney(ticker);
    }

    @JsonIgnore
    public SecurityKey getSecurityKey() {
        return new SecurityKeyImpl(getTicker(), getExchange());
    }

    @Override
    public boolean isNew() {
        return getCreatedDate() == null;
    }
}
