package com.github.klawru.site.repository;

import com.github.klawru.site.repository.entity.BankMapperEntity;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import reactor.core.publisher.Mono;


public interface BankMapperRepository extends R2dbcRepository<BankMapperEntity, Long> {

    Mono<BankMapperEntity> findByBankId(String id);
}
