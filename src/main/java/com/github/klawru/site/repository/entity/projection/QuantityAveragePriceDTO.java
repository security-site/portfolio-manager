package com.github.klawru.site.repository.entity.projection;

import com.github.klawru.site.repository.entity.SecurityKeyEntity;
import lombok.Value;

import javax.annotation.Nullable;
import java.math.BigDecimal;

@Value
public class QuantityAveragePriceDTO implements SecurityKeyEntity {
    String ticker;
    String exchange;
    /**
     * Для денежных средств подсчет не ведеться
     */
    @Nullable
    Integer totalQuantity;
    @Nullable
    BigDecimal averagePrice;

    public QuantityAveragePriceDTO(String ticker, String exchange, Integer totalQuantity, @Nullable BigDecimal averagePrice) {
        this.ticker = ticker;
        this.exchange = exchange;
        this.totalQuantity = totalQuantity;
        this.averagePrice = averagePrice;
    }
}
