package com.github.klawru.site.repository.entity;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.util.UUID;

@Table("trade_file")
@Getter
@Setter
@ToString
@AllArgsConstructor
public class TradesFileInfo extends AbstractAuditingEntity<UUID> {
    @Id
    @With
    UUID id;
    String filename;
}
