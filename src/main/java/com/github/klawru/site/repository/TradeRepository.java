package com.github.klawru.site.repository;


import com.github.klawru.site.repository.entity.Operation;
import com.github.klawru.site.repository.entity.TradeEntity;
import com.github.klawru.site.repository.entity.projection.LastTransaction;
import org.eclipse.collections.api.map.ImmutableMap;
import org.eclipse.collections.impl.collector.Collectors2;
import org.springframework.data.r2dbc.repository.Modifying;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

public interface TradeRepository extends R2dbcRepository<TradeEntity, UUID> {


    @Query("""
            SELECT lt.ticker, lt.exchange, lt.sum, lt.quantity, lt.total_quantity
            FROM UNNEST(ARRAY [:ticker ]) AS cte
            CROSS JOIN LATERAL (SELECT *
                                FROM trade
                                WHERE user_id = :userId
                                  AND ticker = cte.cte
                                ORDER BY date, id
                                LIMIT 1) AS lt
            """)
    Flux<LastTransaction> findAllByUserIdAndTicker(UUID userId, Collection<String> tickers);

    default Mono<ImmutableMap<String, LastTransaction>> findLastTransaction(UUID userId, Collection<String> tickers) {
        return findAllByUserIdAndTicker(userId, tickers)
                .collect(Collectors2.toImmutableMap(LastTransaction::getTicker, each -> each));
    }

    String UPSERT_SQL = """
            INSERT INTO trade (id, parent_id, date, settlement_date, ticker, exchange, isin, operation,
                               price, price_currency, sum,  quantity, total_quantity,
                               nkd,  nkd_currency,
                               fee,  fee_currency,
                               hash, seq_id, file_id, note,
                               user_id, created_date, last_modified_by, last_modified_date)
            VALUES (:id, :parentId, :date, :settlementDate, :ticker, :exchange, :isin, :operation,
                    :price,  :priceCurrency, :sum,
                    :quantity, :totalQuantity,
                    :nkd, :nkdCurrency,
                    :fee, :feeCurrency,
                    :hash, :seqId, :fileId, :note,
                    :userId, :createdDate, :lastModifiedBy, :lastModifiedDate)
            ON CONFLICT DO NOTHING""";

    @Modifying
    @Query(UPSERT_SQL)
    Mono<Boolean> upsert(UUID id, UUID parentId, LocalDateTime date, LocalDateTime settlementDate,
                         String ticker, String exchange, String isin, Operation operation,
                         BigDecimal price, Integer priceCurrency, BigDecimal sum,
                         Integer quantity, Integer totalQuantity,
                         BigDecimal nkd, Integer nkdCurrency,
                         BigDecimal fee, Integer feeCurrency,
                         Integer hash, Integer seqId, UUID fileId, String note,
                         UUID userId, Instant createdDate, UUID lastModifiedBy, Instant lastModifiedDate
    );

    @Modifying
    default Mono<Boolean> upsert(TradeEntity entity) {
        return upsert(entity.getId(), entity.getParentId(), entity.getDate(), entity.getSettlementDate(),
                entity.getTicker(), entity.getExchange(), entity.getIsin(), entity.getOperation(),
                entity.getPrice(), entity.getPriceCurrency(),
                entity.getSum(), entity.getQuantity(), entity.getTotalQuantity(),
                entity.getNkd(), entity.getNkdCurrency(),
                entity.getFee(), entity.getFeeCurrency(),
                entity.getHash(), entity.getSeqId(), entity.getFileId(), entity.getNote(),
                entity.getUserId(), entity.getCreatedDate(), entity.getLastModifiedBy(), entity.getLastModifiedDate());
    }

    @Modifying
    @Query("""
            WITH beforeTrade AS ( --Находим первую тразакцию до вставленной
                SELECT cte.*,
                       COALESCE((SELECT t2.total_quantity
                                 FROM trade AS t2
                                 WHERE t2.user_id = :userId
                                   AND t2.date < cte.date
                                   AND t2.total_quantity IS NOT NULL
                                   AND t2.ticker = cte.ticker
                                 ORDER BY t2.date DESC, t2.id DESC
                                 LIMIT 1), 0) AS startQuantity
                FROM UNNEST(ARRAY [:tickers ],ARRAY [:exchanges ], ARRAY [:fromDate ]) AS cte (ticker, exchange, date)
            ),
                 trandeAfterNew AS ( --Подсчитываем количество бумаг для всех вставленных и последущих сделок
                     SELECT new_transaction.id, new_transaction.total_quantity
                     FROM beforeTrade bf
                              CROSS JOIN LATERAL (
                         SELECT t.id,
                                SUM(quantity) OVER (ORDER BY t.date, id) + bf.startQuantity AS total_quantity
                         FROM trade AS t
                         WHERE user_id = :userId
                           AND t.ticker = bf.ticker
                           AND t.exchange = bf.exchange
                           AND t.date >= bf.date
                         ORDER BY t.date, t.id) AS new_transaction
                 )
            UPDATE trade AS t
            SET total_quantity=lt.total_quantity
            FROM trandeAfterNew lt
            WHERE t.id = lt.id
              AND t.total_quantity IS DISTINCT FROM lt.total_quantity
            """)
    Mono<Void> updateTotalQuantity(UUID userId, Collection<String> tickers, Collection<String> exchanges, Collection<LocalDateTime> fromDate);


    @Modifying
    @Query("""
            WITH beforeTransaction AS
                     (
                         SELECT cte.*,
                                COALESCE((
                                            SELECT t2.sum
                                            FROM trade AS t2
                                            WHERE t2.user_id = :userId
                                              AND t2.date < cte.date
                                              AND t2.sum IS NOT NULL
                                              AND t2.ticker = cte.ticker
                                              AND t2.exchange = 'CURRENCY'
                                            ORDER BY t2.date DESC, t2.id DESC
                                            LIMIT 1), 0) AS startSum
                         FROM UNNEST(ARRAY [:tickers ], ARRAY [:fromDate ]) AS cte (ticker, date)),
                 transactionsAfterNew AS (SELECT new_transaction.id, new_transaction.sum
                                      FROM beforeTransaction cte
                                               CROSS JOIN LATERAL ( SELECT t.id, ((SUM(t.price)OVER (ORDER BY t.date, id)) + cte.startSum) AS SUM
                                                                    FROM trade AS t
                                                                    WHERE t.user_id = :userId
                                                                      AND t.ticker = cte.ticker
                                                                      AND t.exchange = 'CURRENCY'
                                                                      AND t.date >= cte.date
                                                                    ORDER BY t.date, t.id) AS new_transaction)
            UPDATE trade AS t
            SET sum=lt.sum
            FROM transactionsAfterNew lt
            WHERE t.id = lt.id
              AND t.sum IS DISTINCT FROM lt.sum
            """)
    Mono<Void> updateMoneySum(UUID userId,
                              Collection<String> tickers,
                              Collection<LocalDateTime> fromDate);


    Mono<Boolean> deleteAllByFileId(UUID fileId);


    default Flux<TradeEntity> findAllFromDate(UUID userId, LocalDate onDate) {
        return findAllByUserIdAndDateIsAfter(userId, onDate.atStartOfDay());
    }

    @Query("SELECT * FROM trade WHERE user_id=:userId AND (id = :tradeId OR parent_id = :tradeId)")
    Flux<TradeEntity> findByIdWithSubTrade(UUID userId, UUID tradeId);

    Flux<TradeEntity> findAllByUserIdAndDateIsAfter(UUID userId, LocalDateTime from);

    Flux<TradeEntity> findAllByUserIdAndDateBetweenOrderByDateDesc(UUID userId, LocalDateTime from, LocalDateTime to);

    Mono<TradeEntity> findFirstByUserIdOrderByDateAscIdAsc(UUID userId);

    Flux<TradeEntity> findAllByUserIdOrderByDateDescIdDesc(UUID userId);

    Flux<TradeEntity> findAllByFileIdInOrderByDateDescIdDesc(List<UUID> fileIds);

    Flux<TradeEntity> findAllByUserIdAndDateAfterOrderByDateDescIdDesc(UUID userId, LocalDateTime localDateTime);

}
