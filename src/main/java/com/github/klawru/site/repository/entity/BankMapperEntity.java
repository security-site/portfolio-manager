package com.github.klawru.site.repository.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Table("bank_mapper")
@Data
@AllArgsConstructor
@EqualsAndHashCode
public class BankMapperEntity implements Serializable, Persistable<Long> {
    @Id
    Long id;
    String bankId;
    List<String> headers;
    String startData;
    String endData;
    String date;
    String settlementDate;
    String ticker;
    String exchange;
    String isin;
    String operation;
    String price;
    String sum;
    String priceCurrency;
    String quantity;
    String nkd;
    String nkdCurrency;
    String fee;
    String feeCurrency;
    String note;
    @NotNull
    List<String> additionalData;
    @NotNull
    List<String> contextFillers;
    String transformMap;
    List<String> datePatterns;

    @Version
    Integer version;

    @Override
    public boolean isNew() {
        return id == null;
    }
}
