package com.github.klawru.site.repository.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Column;

import java.io.Serial;
import java.io.Serializable;
import java.time.Instant;
import java.util.UUID;

/**
 * Base abstract class for entities which will hold definitions for created, last modified, created by,
 * last modified by attributes.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public abstract class AbstractAuditingEntity<T> implements Serializable, Persistable<T> {

    @Serial
    private static final long serialVersionUID = 1L;

    @CreatedBy
    @Column("user_id")
    @JsonIgnore
    private UUID userId;

    @CreatedDate
    @Column("created_date")
    @JsonIgnore
    private Instant createdDate;

    @LastModifiedBy
    @Column("last_modified_by")
    @JsonIgnore
    private UUID lastModifiedBy;

    @LastModifiedDate
    @Column("last_modified_date")
    @JsonIgnore
    private Instant lastModifiedDate;


    @Override
    public boolean isNew() {
        return createdDate == null;
    }
}
