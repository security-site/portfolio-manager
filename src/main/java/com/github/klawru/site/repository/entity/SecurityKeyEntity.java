package com.github.klawru.site.repository.entity;

/**
 * Ключ по которому можно точно определить бумагу
 */
public interface SecurityKeyEntity {

    String getTicker();

    String getExchange();

    default SecurityKey getSecurityKey() {
        return SecurityKey.of(getTicker(), getExchange());
    }
}
