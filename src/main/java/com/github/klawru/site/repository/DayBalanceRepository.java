package com.github.klawru.site.repository;

import com.github.klawru.site.repository.entity.DayBalance;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;
import java.util.UUID;

/**
 * Spring Data R2DBC repository for the {@link DayBalance} entity.
 */
public interface DayBalanceRepository extends R2dbcRepository<DayBalance, Long> {
    Mono<DayBalance> findFirstByUserIdOrderByDateDesc(UUID uuid);

    Flux<DayBalance> findAllByUserId(UUID userId);

    Flux<DayBalance> findAllByUserIdAndDateAfter(UUID uuid, LocalDate date);
}
