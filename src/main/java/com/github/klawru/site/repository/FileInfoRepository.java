package com.github.klawru.site.repository;

import com.github.klawru.site.repository.entity.DayBalance;
import com.github.klawru.site.repository.entity.TradesFileInfo;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import reactor.core.publisher.Mono;

import java.util.UUID;

/**
 * Spring Data R2DBC repository for the {@link DayBalance} entity.
 */
public interface FileInfoRepository extends R2dbcRepository<TradesFileInfo, UUID> {

    @Query("UPDATE trade_file SET last ")
    Mono<Boolean> updateLastChange(UUID fileId);
}
