package com.github.klawru.site.repository.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.github.klawru.site.service.parser.mapper.CurrencyMapper;
import com.github.klawru.site.utility.TradeUtility;
import com.github.klawru.site.utility.UuidGenerator;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.With;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.javamoney.moneta.Money;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

import static com.github.klawru.site.utility.TradeUtility.calcSum;
import static com.github.klawru.site.utility.TradeUtility.negativeIfSale;


@Table("trade")
@Data
@AllArgsConstructor
@EqualsAndHashCode(of = "id", callSuper = false)
public class TradeEntity extends AbstractAuditingEntity<UUID> implements Serializable, SecurityKeyEntity {
    @Id
    @With
    UUID id;
    /**
     * Используеться денежными средствами для указания связанной сделки
     */
    UUID parentId;

    /**
     * Только дата иммет смысл. Во времени храниться последовательность для сортировки
     */
    LocalDateTime date;
    LocalDateTime settlementDate;

    @NotNull
    String ticker;
    String exchange;
    String isin;
    @NotNull
    Operation operation;

    /**
     * Цена, для сделок с дененжными средствами покупки хранят отрицательные числа
     */
    BigDecimal price;


    Integer priceCurrency;


    /**
     * Общая сумма сделки, но сделки с валютой хранят в sum остаток после текущей сделки.
     */
    BigDecimal sum;

    Integer quantity;

    Integer totalQuantity;

    BigDecimal nkd;
    Integer nkdCurrency;
    /**
     * Коммисия
     */
    BigDecimal fee;
    Integer feeCurrency;

    Integer hash;
    Integer seqId;
    UUID fileId;

    String note;//Заметка о сделке

    public void onBeforeSave(UuidGenerator generator) {
        if (isNew()) {
            if (id == null)
                id = generator.generate();
            convertNew();
        }
    }

    public void convertNew() {
        if (isMoney()) {
            //Для операции продажа используем отрицательные числа при работе с денежными средствами
            price = negativeIfSale(operation, price);
            exchange = "CURRENCY";
            quantity = negativeIfSale(operation, 1);
        } else {
            //если сделка с бумагой
            //Для сделок с бумагами подсчитываем сумму, если она не заполнена
            if (sum == null || sum.compareTo(BigDecimal.ZERO) == 0) {
                int scale = CurrencyMapper.currencyToUnit(priceCurrency).getDefaultFractionDigits();
                this.sum = calcSum(price, quantity, fee, operation).setScale(scale, RoundingMode.HALF_EVEN);
            }
            quantity = negativeIfSale(operation, quantity);
        }
        calcHash();
    }

    public void calcHash() {
        hash = new HashCodeBuilder(17, 37)
                .append(operation.ordinal())
                .append(date.toLocalDate())
                .append(price)
                .append(quantity)
                .append(seqId)
                .toHashCode();
    }

    @JsonIgnore
    public boolean isMoney() {
        return TradeUtility.isMoney(ticker);
    }

    @Override
    @JsonIgnore
    public boolean isNew() {
        return getCreatedDate() == null;
    }

    @JsonIgnore
    public Money getPriceMoney() {
        return Money.of(price, CurrencyMapper.currencyToCode(priceCurrency));
    }

    @JsonIgnore
    public Money getSumMoney() {
        if (sum == null)
            return null;
        return Money.of(sum, CurrencyMapper.currencyToCode(priceCurrency));
    }

    @JsonIgnore
    @Nullable
    public Money getNkdMoney() {
        if (nkd == null)
            return null;
        return Money.of(nkd, CurrencyMapper.currencyToCode(nkdCurrency));
    }

    @JsonIgnore
    public Money getFeeMoney() {
        return Money.of(fee, CurrencyMapper.currencyToCode(feeCurrency));
    }

    @JsonIgnore
    public LocalDate getLocalDate() {
        return date.toLocalDate();
    }

    @JsonIgnore
    public SecurityKey getSecurityKey() {
        return new SecurityKeyImpl(getTicker(), getExchange());
    }
}


