package com.github.klawru.site.repository.entity;

import com.github.klawru.site.utility.PairKey;

public class SecurityKeyImpl extends PairKey<String, String> implements SecurityKey {
    public SecurityKeyImpl(String one, String two) {
        super(one, two);
    }

    @Override
    public String getTicker() {
        return this.getOne();
    }

    @Override
    public String getExchange() {
        return this.getTwo();
    }

    @Override
    public int hashCode() {
        return super.getHashCode();
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public String toString() {
        return "SecurityKey{" + getTicker() + "," + getExchange() + '}';
    }
}
