package com.github.klawru.site.repository.entity.projection;

import lombok.Value;

@Value
public class LastTransaction {
    String ticker;
    String exchange;
    long sum;
    int sumScale;
    int quantity;
    int totalQuantity;
}
