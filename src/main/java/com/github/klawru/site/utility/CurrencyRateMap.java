package com.github.klawru.site.utility;

import lombok.Value;
import lombok.experimental.Accessors;
import org.eclipse.collections.impl.map.sorted.immutable.ImmutableTreeMap;

import java.math.BigDecimal;
import java.time.LocalDate;

@Value
@Accessors(fluent = true)
public class CurrencyRateMap {
    String from;
    String to;
    ImmutableTreeMap<LocalDate, BigDecimal> rate;
}
