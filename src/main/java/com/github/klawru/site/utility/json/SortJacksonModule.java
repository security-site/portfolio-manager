package com.github.klawru.site.utility.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.module.SimpleDeserializers;
import com.fasterxml.jackson.databind.module.SimpleSerializers;
import com.github.klawru.site.error.EncodeException;
import org.springframework.data.domain.Sort;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SortJacksonModule extends Module {

    @Override
    public String getModuleName() {
        return "SortModule";
    }

    @Override
    public Version version() {
        return new Version(0, 1, 0, "", null, null);
    }

    @Override
    public void setupModule(Module.SetupContext context) {
        SimpleSerializers serializers = new SimpleSerializers();
        serializers.addSerializer(Sort.class, new SortSerializer());
        context.addSerializers(serializers);

        SimpleDeserializers deserializers = new SimpleDeserializers();
        deserializers.addDeserializer(Sort.class, new SortDeserializer());
        context.addDeserializers(deserializers);
    }

    public static class SortSerializer extends JsonSerializer<Sort> {

        @Override
        public void serialize(Sort value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
            gen.writeStartArray();
            value.iterator().forEachRemaining(v -> {
                try {
                    gen.writeObject(v);
                } catch (IOException e) {
                    throw new EncodeException("Couldn't serialize object " + v);
                }
            });
            gen.writeEndArray();
        }

        @Override
        public Class<Sort> handledType() {
            return Sort.class;
        }

    }

    public static class SortDeserializer extends JsonDeserializer<Sort> {

        @Override
        public Sort deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
                throws IOException {
            JsonNode node = jsonParser.getCodec().readTree(jsonParser);
            final var orders = node.get("orders");
            if (orders.isArray()) {
                return getSortDeserialize(orders);
            }
            return Sort.unsorted();
        }

        private Sort getSortDeserialize(JsonNode node) {
            List<Sort.Order> orders = new ArrayList<>(node.size());
            for (JsonNode obj : node) {
                final var directionNode = obj.get("direction");
                final var propertyNode = obj.get("property");
                if (propertyNode != null) {
                    final var direction = directionNode == null ? Sort.DEFAULT_DIRECTION : Sort.Direction.valueOf(directionNode.asText());
                    orders.add(new Sort.Order(direction, propertyNode.asText()));
                }
            }
            return Sort.by(orders);
        }

        @Override
        public Class<Sort> handledType() {
            return Sort.class;
        }

    }

}