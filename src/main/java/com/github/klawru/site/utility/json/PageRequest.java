package com.github.klawru.site.utility.json;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.With;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

@Slf4j
@Data
@NoArgsConstructor
public class PageRequest implements Pageable {
    @With
    int page = 0;
    int size = 25;
    Sort sort = Sort.unsorted();

    public void setSort(Sort sort) {
        this.sort = sort;
    }

    public PageRequest(int page, int size, Sort sort) {
        this.page = page;
        this.size = size;
        this.sort = sort;
    }

    @Override
    @JsonIgnore
    public boolean isPaged() {
        return true;
    }

    @Override
    @JsonIgnore
    public boolean isUnpaged() {
        return !isPaged();
    }

    @Override
    @JsonIgnore
    public int getPageNumber() {
        return page;
    }


    @Override
    @JsonIgnore
    public int getPageSize() {
        return size;
    }

    @Override
    @JsonIgnore
    public long getOffset() {
        return (long) page * (long) size;
    }

    @Override
    @JsonIgnore
    public Pageable next() {
        return new PageRequest(page + 1, size, sort);
    }

    @Override

    @JsonIgnore
    public Pageable previousOrFirst() {
        return hasPrevious() ? previous() : first();
    }

    @JsonIgnore
    private Pageable previous() {
        int newPage = page > 0 ? page - 1 : 0;
        return new PageRequest(newPage, size, sort);
    }

    @Override
    @JsonIgnore
    public Pageable first() {
        return new PageRequest(0, size, sort);

    }

    @Override
    @JsonIgnore
    public boolean hasPrevious() {
        return page > 0;
    }
}
