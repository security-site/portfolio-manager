package com.github.klawru.site.utility;

import com.github.klawru.site.repository.entity.Operation;
import com.github.klawru.site.repository.entity.SecurityKey;
import com.github.klawru.site.repository.entity.SecurityKeyEntity;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.eclipse.collections.api.map.MutableMap;
import org.eclipse.collections.api.multimap.list.MutableListMultimap;
import org.eclipse.collections.impl.factory.Multimaps;

import javax.annotation.Nonnull;
import java.math.BigDecimal;
import java.util.stream.Collector;

import static org.apache.commons.lang3.StringUtils.isNoneEmpty;
import static org.eclipse.collections.impl.collector.Collectors2.groupBy;
import static org.eclipse.collections.impl.collector.Collectors2.toMap;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TradeUtility {

    public static BigDecimal calcSum(BigDecimal price, int quantity, BigDecimal fee, Operation operation) {
        BigDecimal quantityBD = BigDecimal.valueOf(quantity);
        return positiveExact(price.multiply(quantityBD))
                .add(negativeIfSale(operation, fee));
    }

    public static BigDecimal negativeIfSale(Operation operation, BigDecimal number) {
        if (number == null)
            return null;
        if (Operation.SALE.equals(operation))
            return negativeExact(number);
        else
            return positiveExact(number);
    }

    public static long negativeIfSale(Operation operation, long number) {
        if (Operation.SALE.equals(operation))
            return negativeExact(number);
        else
            return positiveExact(number);
    }

    public static int negativeIfSale(Operation operation, int number) {
        return (int) negativeIfSale(operation, (long) number);
    }

    public static BigDecimal negativeExact(BigDecimal number) {
        return number.compareTo(BigDecimal.ZERO) > 0 ? number.negate() : number;
    }

    public static BigDecimal positiveExact(BigDecimal number) {
        return number.compareTo(BigDecimal.ZERO) > 0 ? number : number.negate();
    }

    public static long negativeExact(long number) {
        return number > 0 ? -number : number;
    }

    public static int positiveExact(int number) {
        return (int) positiveExact((long) number);
    }

    public static long positiveExact(long number) {
        return number > 0 ? number : -number;
    }

    public static String createMoneyTicker(String currencyCode) {
        if (currencyCode == null)
            return null;
        return "$" + currencyCode;
    }

    @SneakyThrows
    public static String getMoneyCurrency(String ticker) {
        if (isMoney(ticker))
            return ticker.substring(1);
        throw new IllegalAccessException("Not money ticker='" + ticker + "'");
    }

    public static boolean isMoney(String ticker) {
        return isNoneEmpty(ticker) && ticker.charAt(0) == '$';
    }

    public static Operation invertOperation(Operation operation) {
        return switch (operation) {
            case BUY -> Operation.SALE;
            case SALE -> Operation.BUY;
        };
    }

    @Nonnull
    public static <T extends SecurityKeyEntity> Collector<T, ?, MutableListMultimap<SecurityKey, T>> groupBySecurityKey() {
        return groupBy(T::getSecurityKey, Multimaps.mutable.list::empty);
    }

    @Nonnull
    public static <T extends SecurityKeyEntity> Collector<T, ?, MutableMap<SecurityKey, T>> toMapBySecurityKey() {
        return toMap(T::getSecurityKey, it -> it);
    }
}
