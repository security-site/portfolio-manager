package com.github.klawru.site.utility;

import com.github.klawru.lib.contract.security.v1.SecurityInfo;
import com.github.klawru.lib.contract.security.v1.types.request.CurrencyRateRequestDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.currency.CurrencyPairDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.currency.CurrencyRateDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.currency.RateHistoryDTO;
import com.github.klawru.site.security.OAuthAuthenticationOperator;
import com.github.klawru.site.service.client.DbCacheClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.collections.impl.map.mutable.ConcurrentHashMap;
import org.eclipse.collections.impl.map.sorted.immutable.ImmutableTreeMap;
import org.eclipse.collections.impl.map.sorted.mutable.TreeSortedMap;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class ConversationService {
    private final DbCacheClient service;
    private final OAuthAuthenticationOperator authenticationOperator;
    ConcurrentHashMap<PairKey<String, String>, CurrencyRateMap> currencyMap = ConcurrentHashMap.newMap();

    public Mono<BigDecimal> getRate(LocalDate date, String from, String to) {
        if (from.equals(to))
            return Mono.just(BigDecimal.ONE);
        return Optional.ofNullable(currencyMap.get(PairKey.of(from, to)))
                .flatMap(currencyRateMap -> getRate(currencyRateMap, date, from))
                .map(Mono::just)
                .orElse(requestFromDBService(date, from, to));
    }

    private Mono<BigDecimal> requestFromDBService(LocalDate date, String from, String to) {
        return service.getCurrencyRate(new CurrencyRateRequestDTO(new CurrencyPairDTO(from, to), SecurityInfo.FROM_MIN_DATE))
                .map(currencyRateDTO -> {
                    var rateMap = map(currencyRateDTO);
                    rateMap = save(rateMap);
                    return getRate(rateMap, date, from);
                })
                .flatMap(Mono::justOrEmpty)
                .as(authenticationOperator::withAuthentication);
    }

    private Optional<BigDecimal> getRate(CurrencyRateMap currencyRateMap, LocalDate date, String from) {
        final var rate = currencyRateMap.rate().get(date);
        if (rate == null) {
            return Optional.empty();
        }
        if (currencyRateMap.from().equals(from))
            return Optional.of(rate);
        else
            return Optional.of(BigDecimal.ONE.divide(rate, RoundingMode.HALF_EVEN));
    }

    private CurrencyRateMap save(CurrencyRateMap currencyRateMap) {
        currencyMap.merge(PairKey.of(currencyRateMap.from(), currencyRateMap.to()), currencyRateMap,
                (first, second) -> {
                    final var firstFromDate = first.rate().firstKey();
                    final var secondFromDate = second.rate().firstKey();
                    return firstFromDate.isBefore(secondFromDate) ? first : second;
                });
        return currencyMap.merge(PairKey.of(currencyRateMap.to(), currencyRateMap.from()), currencyRateMap,
                (first, second) -> {
                    final var firstFromDate = first.rate().firstKey();
                    final var secondRomDate = second.rate().firstKey();
                    return firstFromDate.isBefore(secondRomDate) ? first : second;
                });
    }

    private CurrencyRateMap map(CurrencyRateDTO currencyRateDTO) {
        final var sortedMap = new ImmutableTreeMap<>(TreeSortedMap.<LocalDate, BigDecimal>newMap()
                .collectKeysAndValues(
                        currencyRateDTO.getHistory(),
                        RateHistoryDTO::getDate,
                        RateHistoryDTO::getRate
                ));
        return new CurrencyRateMap(
                currencyRateDTO.getCurrency().getFrom(),
                currencyRateDTO.getCurrency().getTo(),
                sortedMap
        );
    }


}
