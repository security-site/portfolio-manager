package com.github.klawru.site.utility.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.data.domain.Sort;

import java.io.IOException;

public class CustomSortDeserializer extends JsonDeserializer<Sort> {
    @Override
    public Sort deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        Sort.Order[] orders = new Sort.Order[node.size()];
        int i = 0;
        for (JsonNode obj : node) {
            final var directionNode = obj.get("direction");
            final var propertyNode = obj.get("property");
            if (propertyNode != null) {
                final var direction = directionNode == null ? Sort.DEFAULT_DIRECTION : Sort.Direction.valueOf(directionNode.asText());
                orders[i] = new Sort.Order(direction, propertyNode.asText());
                i++;
            }
        }
        return Sort.by(orders);
    }
}
