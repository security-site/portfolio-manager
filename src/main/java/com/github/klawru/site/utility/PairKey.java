package com.github.klawru.site.utility;

import lombok.Getter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@Getter
public class PairKey<K1, K2> {
    private final K1 one;
    private final K2 two;
    private final int hashCode;

    public PairKey(K1 one, K2 two) {
        this.one = one;
        this.two = two;
        this.hashCode = new HashCodeBuilder().append(one).append(two).hashCode();
    }

    public static <K1, K2> PairKey<K1, K2> of(K1 first, K2 second) {
        return new PairKey<>(first, second);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        PairKey<?, ?> pairKey = (PairKey<?, ?>) o;

        return new EqualsBuilder().append(one, pairKey.one).append(two, pairKey.two).isEquals();
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

}
