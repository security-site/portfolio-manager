package com.github.klawru.site.utility.parse;

import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.collections.api.set.primitive.IntSet;
import org.eclipse.collections.impl.factory.primitive.IntSets;

@UtilityClass
public class CamelCaseUtility {

    final IntSet DELIMITER_SET = IntSets.immutable.of(' ', '\t', '\n', '\r');

    public static String toCamelCase(String str, IntSet ignoreChars) {
        if (StringUtils.isEmpty(str)) {
            return str;
        }
        final int[] result = new int[str.length()];
        int resultOffset = 0;
        boolean capitalizeNext = true;
        var intIterator = str.toLowerCase().codePoints().iterator();
        while (intIterator.hasNext()) {
            int codePoint = intIterator.next();
            if (!ignoreChars.contains(codePoint)) {
                if (DELIMITER_SET.contains(codePoint)) {
                    capitalizeNext = true;
                } else if (capitalizeNext) {
                    final int titleCaseCodePoint = Character.toTitleCase(codePoint);
                    result[resultOffset++] = titleCaseCodePoint;
                    capitalizeNext = false;
                } else {
                    result[resultOffset++] = codePoint;
                }
            }
        }
        return new String(result, 0, resultOffset);
    }

}
