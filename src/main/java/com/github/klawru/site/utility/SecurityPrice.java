package com.github.klawru.site.utility;

import com.github.klawru.lib.contract.security.v1.types.responce.HistoryDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.MoneyDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.SecurityDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.SecurityType;
import lombok.Value;
import org.eclipse.collections.api.map.ImmutableMap;
import org.eclipse.collections.impl.factory.Lists;
import org.javamoney.moneta.Money;

import javax.money.Monetary;
import javax.money.MonetaryAmount;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Optional;

import static java.math.RoundingMode.HALF_EVEN;

@Value
public class SecurityPrice {
    SecurityType type;
    ImmutableMap<LocalDate, MoneyDTO> historyList;

    public SecurityPrice(SecurityDTO securityDTO) {
        type = securityDTO.getType().getType();
        historyList = Optional.ofNullable(securityDTO.getHistory())
                .map(Lists::adapt)
                .orElse(Lists.mutable.empty())
                .toImmutableMap(HistoryDTO::getDate,
                        history -> switch (type) {
                            case STOCK_BONDS -> getBondPrice(history);
                            default -> MoneyDTO.of(history.getPrice(), history.getCurrency());
                        }
                );
    }

    private MoneyDTO getBondPrice(HistoryDTO history) {
        final var faceValue = history.getFaceValue();
        final var priceAsPercent = history.getPrice().setScale(history.getPrice().scale() + 2, HALF_EVEN);
        final var amount = faceValue.getAmount().multiply(priceAsPercent);
        return MoneyDTO.of(amount, faceValue.getCurrency());
    }

    private Money moneyFrom(MoneyDTO moneyDTO, int quantity) {
        return Money.of(moneyDTO.getAmount().multiply(BigDecimal.valueOf(quantity)), Monetary.getCurrency(moneyDTO.getCurrency()));
    }

    public Optional<MonetaryAmount> calcPriceOnDate(LocalDate date, int quantity) {
        return Optional.ofNullable(historyList.get(date))
                .map(moneyDTO -> moneyFrom(moneyDTO, quantity))
                .map(money -> money.multiply(quantity));
    }
}
