package com.github.klawru.site.utility;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class AverageUtils {

    public static double addAveragesTogether(double averageA, int sizeA, double averageB, int sizeB) {
        return (sizeA * averageA + sizeB * averageB) / (sizeA + sizeB);
    }

    public static double addAveragesTogether2(double averageA, int sizeA, double averageB, int sizeB) {
        return ((sizeA * averageA) / (sizeA + sizeB)) + (averageB * sizeB / (sizeA + sizeB));
    }

    public static BigDecimal addToAverage(BigDecimal oldAverage, BigDecimal newValue, long count) {
        if (count == 0)
            return oldAverage;
        return oldAverage.add(newValue.min(oldAverage).divide(BigDecimal.valueOf(count), RoundingMode.HALF_UP));
    }
}
