package com.github.klawru.site;

import com.github.klawru.site.config.ApplicationProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@Slf4j
@SpringBootApplication
@EnableDiscoveryClient
@EnableConfigurationProperties(value = {ApplicationProperties.class})
public class PortfolioManager {

    public static void main(String[] args) {
        SpringApplication.run(PortfolioManager.class, args);
    }

}
