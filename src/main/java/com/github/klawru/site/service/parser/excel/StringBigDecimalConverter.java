package com.github.klawru.site.service.parser.excel;

import org.springframework.core.convert.converter.Converter;

import javax.annotation.Nonnull;
import java.math.BigDecimal;
import java.util.regex.Pattern;

public class StringBigDecimalConverter implements Converter<String, BigDecimal> {
    static final Pattern pattern = Pattern.compile("[\\D]");

    @Override
    public BigDecimal convert(@Nonnull String input) {
        return valueOf(input);
    }

    @Nonnull
    public static BigDecimal valueOf(String input) {
        int decimalSeparator = lastIndexOfAnyPoint(input);
        if (decimalSeparator > -1) {
            input = pattern.matcher(input).replaceAll("");
            return BigDecimal.valueOf(Long.parseLong(input), input.length() - decimalSeparator);
        } else
            return BigDecimal.valueOf(Long.parseLong(input), 0);
    }

    private static int lastIndexOfAnyPoint(String input) {
        for (int i = input.length() - 1; i > 0; i--) {
            final var charAt = input.charAt(i);
            if (charAt == ',' || charAt == '.')
                return i;
        }
        return -1;
    }


}
