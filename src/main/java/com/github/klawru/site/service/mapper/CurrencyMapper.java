package com.github.klawru.site.service.mapper;

import org.eclipse.collections.impl.map.mutable.primitive.IntObjectHashMap;
import org.mapstruct.Mapper;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import javax.money.CurrencyUnit;
import javax.money.Monetary;

@Mapper
@Named("CurrencyMapper")
public abstract class CurrencyMapper {
    public static final CurrencyMapper INSTANCE = Mappers.getMapper(CurrencyMapper.class);
    //todo заглушка лучше хранить в БД
    private static final IntObjectHashMap<String> currencies = new IntObjectHashMap<>();

    static {
        Monetary.getCurrencies().forEach(currencyUnit ->
                currencies.put(currencyUnit.getNumericCode(), currencyUnit.getCurrencyCode())
        );
    }

    @Named("currencyToInteger")
    public Integer currencyToInteger(String currency) {
        if (currency == null)
            return null;
        return Monetary.getCurrency(currency).getNumericCode();
    }

    @Named("NumCodeToString")
    public String currencyToCode(int currency) {
        return currencies.get(currency);
    }

    public CurrencyUnit getCurrency(int currency) {
        return Monetary.getCurrency(currencyToCode(currency));
    }
}
