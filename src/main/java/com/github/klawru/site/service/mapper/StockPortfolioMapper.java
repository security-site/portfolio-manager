package com.github.klawru.site.service.mapper;

import com.github.klawru.lib.contract.porfolio.v1.types.responce.StockPortfolioDTO;
import com.github.klawru.site.repository.entity.StockPortfolio;
import com.github.klawru.site.service.parser.mapper.CurrencyMapper;
import com.github.klawru.site.utility.TradeUtility;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import javax.money.CurrencyUnit;
import javax.money.Monetary;

@Mapper(uses = CurrencyMapper.class)
public interface StockPortfolioMapper {

    StockPortfolioMapper INSTANCE = Mappers.getMapper(StockPortfolioMapper.class);

    @Mapping(target = "saleQuantity", source = "saleQuantity", qualifiedByName = "positiveExact")
    @Mapping(target = "priceCurrency", source = "priceCurrency", qualifiedByName = {"CurrencyMapper", "NumCodeToString"})
    @Mapping(target = "feeCurrency", source = "feeCurrency", qualifiedByName = {"CurrencyMapper", "NumCodeToString"})
    @Mapping(target = "dividendCurrency", source = "dividendCurrency", qualifiedByName = {"CurrencyMapper", "NumCodeToString"})
    StockPortfolioDTO map(StockPortfolio stockPortfolio);

    @Named("positiveExact")
    default Integer positiveExact(Integer integer) {
        if (integer == null)
            return null;
        return TradeUtility.positiveExact(integer);
    }

    default CurrencyUnit map(Integer currencyId) {
        String currency = CurrencyMapper.currencyToCode(currencyId);
        return Monetary.getCurrency(currency);
    }
}
