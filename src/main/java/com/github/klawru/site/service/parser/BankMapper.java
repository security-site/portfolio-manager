package com.github.klawru.site.service.parser;

import com.github.klawru.site.service.parser.excel.TradesRowMapper;
import lombok.Value;
import lombok.experimental.Accessors;
import org.eclipse.collections.api.list.ImmutableList;
import org.eclipse.collections.api.map.ImmutableMap;
import org.eclipse.collections.api.set.ImmutableSet;
import org.springframework.expression.spel.standard.SpelExpression;

/**
 *
 */
@Value
@Accessors(fluent = true)
public class BankMapper {
    ImmutableSet<String> headers;
    SpelExpression startData;
    SpelExpression endData;
    TradesRowMapper rowMapper;
    ImmutableList<SpelExpression> contextFillers;
    ImmutableMap<String, String> transformMap;

    boolean isCompiled() {
        return startData.compileExpression() && endData.compileExpression()
                && rowMapper.isCompiled() && contextFillers.allSatisfy(SpelExpression::compileExpression);
    }
}
