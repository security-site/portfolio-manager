package com.github.klawru.site.service.parser.excel;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.DateUtil;

import java.time.format.DateTimeFormatter;

/**
 * Класс для получения даты со стандартным форматированием ISO
 */
public class IsoDataFormatter extends DataFormatter {
    final DateTimeFormatter ISO_LOCAL_DATE_TIME = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
    final DateTimeFormatter ISO_LOCAL_DATE = DateTimeFormatter.ISO_LOCAL_DATE;
    final DateTimeFormatter ISO_LOCAL_TIME = DateTimeFormatter.ISO_LOCAL_TIME;

    @Override
    public String formatRawCellContents(double value, int formatIndex, String formatString, boolean use1904Windowing) {
        String result = super.formatRawCellContents(value, formatIndex, formatString, use1904Windowing);
        if (DateUtil.isADateFormat(formatIndex, formatString)) {
            if (DateUtil.isValidExcelDate(value)) {
                var date = DateUtil.getLocalDateTime(value, use1904Windowing);
                if (isDateOnly(formatString))
                    return ISO_LOCAL_DATE.format(date);
                if (isTimeOnly(formatString))
                    return ISO_LOCAL_TIME.format(date);
                return ISO_LOCAL_DATE_TIME.format(date);
            }
        }
        return result;
    }

    static final char[] datePatternChars = new char[]{'y', 'd'};

    private boolean isTimeOnly(String formatString) {
        return !StringUtils.containsAny(formatString, datePatternChars);
    }

    static final String[] timePatternChars = new String[]{"h", "mm", "s"};

    private boolean isDateOnly(String formatString) {
        return !StringUtils.containsAny(formatString, timePatternChars);
    }
}
