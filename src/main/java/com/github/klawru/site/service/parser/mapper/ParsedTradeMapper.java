package com.github.klawru.site.service.parser.mapper;

import com.github.klawru.site.repository.entity.TradeEntity;
import com.github.klawru.site.service.mapper.MoneyTradeGenerator;
import com.github.klawru.site.service.parser.excel.ParsedTrade;
import com.github.klawru.site.utility.UuidGenerator;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.collections.api.list.ImmutableList;
import org.eclipse.collections.api.list.MutableList;
import org.eclipse.collections.impl.factory.Lists;
import org.eclipse.collections.impl.factory.primitive.IntIntMaps;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import javax.validation.constraints.NotNull;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

@Slf4j
@Mapper(uses = CurrencyMapper.class, imports = UuidGenerator.class)
public abstract class ParsedTradeMapper {
    public static final ParsedTradeMapper INSTANCE = Mappers.getMapper(ParsedTradeMapper.class);
    private static final MoneyTradeGenerator moneyTradeGenerator = MoneyTradeGenerator.INSTANCE;
    final static Comparator<TradeEntity> tradeDateReversedComparator = Comparator.comparing(TradeEntity::getDate).reversed()
            .thenComparing(TradeEntity::getTicker)
            .thenComparing(TradeEntity::getId).reversed();

    @Mapping(target = "id", expression = "java(uuidGenerator.generate())")
    @Mapping(target = "withId", ignore = true)
    @Mapping(target = "parentId", ignore = true)
    @Mapping(target = "userId", ignore = true)
    @Mapping(target = "exchange", source = "dto.exchange")
    @Mapping(target = "date", source = "dto.date")
    @Mapping(target = "settlementDate", source = "dto.settlementDate")
    @Mapping(target = "price", source = "dto.price")
    @Mapping(target = "priceCurrency", qualifiedByName = {"CurrencyMapper", "currencyToInteger"})
    @Mapping(target = "sum", source = "dto.sum")
    @Mapping(target = "totalQuantity", ignore = true)
    @Mapping(target = "nkd", source = "dto.nkd")
    @Mapping(target = "nkdCurrency", qualifiedByName = {"CurrencyMapper", "currencyToInteger"})
    @Mapping(target = "fee", source = "dto.fee", defaultValue = "0")
    @Mapping(target = "feeCurrency", qualifiedByName = {"CurrencyMapper", "currencyToInteger"}, defaultValue = "XXX")
    @Mapping(target = "hash", ignore = true)
    @Mapping(target = "seqId", constant = "0")
    @Mapping(target = "fileId", source = "fileId")
    @Mapping(target = "lastModifiedDate", ignore = true)
    @Mapping(target = "lastModifiedBy", ignore = true)
    @Mapping(target = "createdDate", ignore = true)
    public abstract TradeEntity map(ParsedTrade dto, UuidGenerator uuidGenerator, UUID fileId);

    @AfterMapping
    protected void afterMap(@MappingTarget TradeEntity tradeEntity) {
        tradeEntity.convertNew();
    }

    public ImmutableList<TradeEntity> map(List<ParsedTrade> dto, UuidGenerator uuidGenerator, @NotNull UUID fileId) {
        if (dto == null) {
            return null;
        }
        MutableList<TradeEntity> resultList = Lists.mutable.withInitialCapacity(dto.size() * 2);
        for (ParsedTrade parsedTrade : dto) {
            try {
                TradeEntity securityTrade = map(parsedTrade, uuidGenerator, fileId);
                if (!securityTrade.isMoney()) {
                    //Если сделка с ценными бумагами, то создаем связанную с денежными средствами
                    TradeEntity moneyTrade = moneyTradeGenerator.createSubMoneyTrade(securityTrade, uuidGenerator);
                    resultList.add(securityTrade);
                    resultList.add(moneyTrade);
                } else
                    resultList.add(securityTrade);
            } catch (Exception e) {
                log.debug("Error on mapping: trade:'{}',error:'{}'", parsedTrade, e.getMessage());
            }
        }
        modifyDuplicatesHash(resultList);
        return resultList.toImmutable();
    }

    /**
     * В рамках дня сделки могут иметь одинаковый хеш, а сделки с одинаковым хешем (тикером и датой) не могут быть вставлены в БД.
     * Что бы избежать этого подмешиваем к хешу sequence, если сделки иммеют одинаковый хеш
     */
    private void modifyDuplicatesHash(MutableList<TradeEntity> resultList) {
        resultList.sort(tradeDateReversedComparator);
        var intBag = IntIntMaps.mutable.empty();
        TradeEntity first = resultList.getFirst();
        for (TradeEntity trade : resultList) {
            //Считаем совпадения только в рамках одного дня и тикера
            if (!first.getDate().toLocalDate().equals(trade.getDate().toLocalDate())) {
                intBag.clear();
                first = trade;
            } else if (!first.getTicker().equals(trade.getTicker())) {
                intBag.clear();
                first = trade;
            }
            int seqId = intBag.addToValue(trade.getHash(), 1) - 1;
            if (seqId > 0) {
                trade.setSeqId(seqId);
                //Возможна коллизия и после изменения seqId, но из за малого шанса не обрабатываем такой случай
            }
        }
    }

}
