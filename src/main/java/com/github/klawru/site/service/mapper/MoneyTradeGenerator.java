package com.github.klawru.site.service.mapper;

import com.github.klawru.site.repository.entity.Operation;
import com.github.klawru.site.repository.entity.TradeEntity;
import com.github.klawru.site.utility.TradeUtility;
import com.github.klawru.site.utility.UuidGenerator;
import org.apache.commons.lang3.RandomUtils;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;


@Mapper
@Named("MoneyTradeGenerator")
public abstract class MoneyTradeGenerator {

    public static final MoneyTradeGenerator INSTANCE = Mappers.getMapper(MoneyTradeGenerator.class);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "withId", ignore = true)
    @Mapping(target = "parentId", source = "id")
    @Mapping(target = "ticker", source = "priceCurrency", qualifiedByName = "createMoneyTicker")
    @Mapping(target = "exchange", constant = "CURRENCY")
    @Mapping(target = "isin", ignore = true)
    @Mapping(target = "operation", source = "operation", qualifiedByName = "invertOperation")
    @Mapping(target = "price", source = "sum")
    @Mapping(target = "sum", ignore = true)
    @Mapping(target = "quantity", constant = "1")
    @Mapping(target = "totalQuantity", ignore = true)
    @Mapping(target = "nkd", ignore = true)
    @Mapping(target = "fee", expression = "java(BigDecimal.ZERO)")
    @Mapping(target = "feeCurrency", constant = "999")
    @Mapping(target = "hash", ignore = true)
    @Mapping(target = "seqId", constant = "0")
    @Mapping(target = "userId", ignore = true)
    @Mapping(target = "createdDate", ignore = true)
    @Mapping(target = "lastModifiedBy", ignore = true)
    @Mapping(target = "lastModifiedDate", ignore = true)
    public abstract TradeEntity createMoneyTradeFrom(TradeEntity trade);


    @AfterMapping
    public void afterMap(@MappingTarget TradeEntity moneyTrade) {
        if (moneyTrade.isNew())
            moneyTrade.convertNew();
    }

    @Mapping(target = "withId", ignore = true)
    @InheritConfiguration(name = "createMoneyTradeFrom")
    @Mapping(target = "ticker", source = "feeCurrency", qualifiedByName = "createMoneyTicker")
    @Mapping(target = "operation", constant = "SALE")
    @Mapping(target = "price", source = "fee")
    @Mapping(target = "priceCurrency", source = "feeCurrency")
    public abstract TradeEntity createMoneyFeeTradeFrom(TradeEntity trade);

    @Mapping(target = "withId", ignore = true)
    protected abstract TradeEntity copy(TradeEntity oldMoney);

    public TradeEntity copyForUpdate(TradeEntity trade, TradeEntity money) {
        TradeEntity moneyTradeFrom = createMoneyTradeFrom(trade);
        moneyTradeFrom.setId(money.getId());
        moneyTradeFrom.setNote(money.getNote());
        moneyTradeFrom.setUserId(money.getUserId());
        moneyTradeFrom.setCreatedDate(money.getCreatedDate());
        moneyTradeFrom.setSeqId(RandomUtils.nextInt());

        money.convertNew();
        return moneyTradeFrom;
    }

    /**
     * Метод создает связанную сделку с денежными средствами
     *
     * @param trade         исходная сделка
     * @param uuidGenerator
     * @return связанную сделку с денежными средствами
     */
    public TradeEntity createSubMoneyTrade(TradeEntity trade, UuidGenerator uuidGenerator) {
        if (trade.isMoney())
            return null;
        TradeEntity monetTrade = createMoneyTradeFrom(trade);
        monetTrade.setId(uuidGenerator.generate());
        monetTrade.convertNew();
        return monetTrade;
        //if (!trade.getPriceCurrency().equals(trade.getFeeCurrency())) {
        //    TradeEntity feeTrade = createMoneyFeeTradeFrom(trade);
        //    feeTrade.setId(uuidGenerator.generate());
        //    target.add(feeTrade);
        //}
    }

    @Nullable
    @Named("createMoneyTicker")
    protected String createMoneyTicker(int currency) {
        return TradeUtility.createMoneyTicker(CurrencyMapper.INSTANCE.currencyToCode(currency));
    }

    @Nonnull
    @Named("invertOperation")
    protected Operation invertOperation(Operation operation) {
        return TradeUtility.invertOperation(operation);
    }
}
