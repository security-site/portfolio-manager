package com.github.klawru.site.service.parser.excel;

import com.github.klawru.site.service.parser.BankMapper;
import com.github.klawru.site.utility.parse.CamelCaseUtility;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.util.CellAddress;
import org.apache.poi.xssf.eventusermodel.XSSFSheetXMLHandler;
import org.apache.poi.xssf.usermodel.XSSFComment;
import org.eclipse.collections.impl.factory.Maps;
import org.eclipse.collections.impl.factory.primitive.IntSets;
import org.eclipse.collections.impl.map.mutable.primitive.IntObjectHashMap;

import java.util.ArrayList;
import java.util.List;

@Slf4j
class SheetContentParser implements XSSFSheetXMLHandler.SheetContentsHandler {

    private final IntObjectHashMap<String> column2Header = IntObjectHashMap.newMap();

    private final BankMapper mapper;
    private final RootObject root;

    @Getter
    private final List<ParsedTrade> result = new ArrayList<>(32);

    private boolean isHeaderRow = false;
    private boolean isFirstCell = true;
    private boolean isDateStart;


    public SheetContentParser(BankMapper bankMapper) {
        mapper = bankMapper;
        root = new RootObject(Maps.mutable.empty(), bankMapper.transformMap());
    }

    public void reset() {
        column2Header.clear();
        root.getRow().clear();
        root.getRow().clear();
        root.getContext().clear();
        result.clear();
        isDateStart = false;
    }

    @Override
    public void startRow(int rowNum) {
        isHeaderRow = false;
        root.getRow().clear();
        root.setLine(rowNum);
        root.getRow().put("rowNum", String.valueOf(rowNum));
        isFirstCell = true;
    }

    @Override
    public void endRow(int rowNum) {
        log.trace("rowData:{}", root.getRow());
        fillContext();
        if (isDateStart) {
            parseRow(rowNum);
        }
        if (isDateStart) {
            checkEndData(rowNum);
        } else {
            checkStartData(rowNum);
        }
    }

    private void parseRow(int rowNum) {
        try {
            mapper.rowMapper().build(root).ifPresent(result::add);
        } catch (Exception e) {
            log.trace("ParseError row='{}', {}:'{}'", rowNum, e.getClass().getSimpleName(), e.getMessage());
        }
    }

    private void fillContext() {
        final var spelExpressions = mapper.contextFillers();
        for (int i = 0; i < spelExpressions.size(); i++) {
            spelExpressions.get(i).getValue(root);
        }
    }

    private void checkStartData(int rowNum) {
        final var value = mapper.startData().getValue(root, Boolean.class);
        if (value != null && value) {
            isDateStart = true;
            log.trace("dateStart row:{}", rowNum);
        }
    }

    private void checkEndData(int rowNum) {
        final var value = mapper.endData().getValue(root, Boolean.class);
        if (value != null && value) {
            isDateStart = false;
            log.trace("dateEnd row:{}", rowNum);
        }
    }

    @Override
    public void cell(String cellReference, String value, XSSFComment comment) {
        CellAddress cellAddress = new CellAddress(cellReference);
        if (isHeaderRow || isFirstCell) {
            parseHeader(value, cellAddress);
        }
        if (!isDateStart) {
            //Необходимо для поиска начала данных
            root.getRow().put(cellReference, value.trim());
        }
        if (!isHeaderRow && isDateStart) {
            final var headerName = column2Header.get(cellAddress.getColumn());
            if (headerName != null) {
                root.getRow().put(headerName, value);
            }
        }
    }

    private void parseHeader(String formattedValue, CellAddress cellAddress) {
        final var headers = mapper.headers();
        final var header = CamelCaseUtility.toCamelCase(formattedValue, IntSets.immutable.with('\n', '\r'));
        if (headers.contains(header)) {
            if (isFirstCell)
                column2Header.clear();
            column2Header.put(cellAddress.getColumn(), header);
            isHeaderRow = true;
        } else if (isFirstCell) {
            isHeaderRow = false;
        }
        if (isFirstCell)
            isFirstCell = false;
    }

}
