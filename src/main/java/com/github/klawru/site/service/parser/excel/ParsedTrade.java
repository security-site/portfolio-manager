package com.github.klawru.site.service.parser.excel;


import lombok.Value;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Map;

@Value
public class ParsedTrade {
    LocalDateTime date;
    LocalDateTime settlementDate;
    String ticker;
    String exchange;
    String isin;
    String operation;
    BigDecimal price;
    BigDecimal sum;
    String priceCurrency;
    Integer quantity;
    BigDecimal nkd;
    String nkdCurrency;
    BigDecimal fee;
    String feeCurrency;
    String note;
    Map<String, String> info;

    /**
     * Строка на которой была сделка
     */
    long line;

}