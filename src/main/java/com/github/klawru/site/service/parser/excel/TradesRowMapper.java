package com.github.klawru.site.service.parser.excel;

import com.github.klawru.site.error.ParseException;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.collections.api.factory.Maps;
import org.eclipse.collections.api.list.ImmutableList;
import org.eclipse.collections.impl.factory.Lists;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.Expression;
import org.springframework.expression.spel.standard.SpelExpression;
import org.springframework.expression.spel.standard.SpelExpressionParser;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;

@Slf4j
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class TradesRowMapper {
    private final SpelExpression dateMapper;
    private final SpelExpression settlementDateMapper;
    private final SpelExpression tickerMapper;
    private final SpelExpression exchangeMapper;
    private final SpelExpression isinMapper;
    private final SpelExpression operationMapper;
    private final SpelExpression priceMapper;
    private final SpelExpression sumMapper;
    private final SpelExpression priceCurrencyMapper;
    private final SpelExpression quantityMapper;
    private final SpelExpression nkdMapper;
    private final SpelExpression nkdCurrencyMapper;
    private final SpelExpression feeMapper;
    private final SpelExpression feeCurrencyMapper;
    private final SpelExpression note;
    private final ImmutableList<SpelExpression> additionalData;
    private final ImmutableList<DateTimeFormatter> dateTimeFormatters;

    public TradesRowMapper(SpelExpressionParser parser, EvaluationContext context,
                           String dateMapper, String settlementDateMapper,
                           String tickerMapper, String exchangeMapper,
                           String isinMapper,
                           String operationMapper, String priceMapper,
                           String sumMapper, String priceCurrencyMapper,
                           String quantityMapper, String nkdMapper,
                           String nkdCurrencyMapper, String feeMapper,
                           String feeCurrencyMapper, String noteMapper,
                           List<String> additionalDataMappers,
                           ImmutableList<DateTimeFormatter> dateTimeFormatters) {
        this(
                parseExpression(dateMapper, parser, context),
                parseExpression(settlementDateMapper, parser, context),
                parseExpression(tickerMapper, parser, context),
                parseExpression(exchangeMapper, parser, context),
                parseExpression(isinMapper, parser, context),
                parseExpression(operationMapper, parser, context),
                parseExpression(priceMapper, parser, context),
                parseExpression(sumMapper, parser, context),
                parseExpression(priceCurrencyMapper, parser, context),
                parseExpression(quantityMapper, parser, context),
                parseExpression(nkdMapper, parser, context),
                parseExpression(nkdCurrencyMapper, parser, context),
                parseExpression(feeMapper, parser, context),
                parseExpression(feeCurrencyMapper, parser, context),
                parseExpression(noteMapper, parser, context),
                parseExpressions(additionalDataMappers, parser, context),
                dateTimeFormatters
        );
    }


    public Optional<ParsedTrade> build(RootObject rootObject) {
        ParsedTrade dto = new ParsedTrade(
                tryCatch("date", () -> parseDate(dateMapper.getValue(rootObject, String.class))),
                tryCatch("settlementDate", () -> parseDate(settlementDateMapper.getValue(rootObject, String.class))),
                tryCatch("ticker", () -> tickerMapper.getValue(rootObject, String.class)),
                tryCatch("exchange", () -> exchangeMapper.getValue(rootObject, String.class)),
                tryCatch("isin", () -> isinMapper.getValue(rootObject, String.class)),
                tryCatch("operation", () -> operationMapper.getValue(rootObject, String.class)),
                tryCatch("price", () -> priceMapper.getValue(rootObject, BigDecimal.class)),
                tryCatch("sum", () -> sumMapper.getValue(rootObject, BigDecimal.class)),
                tryCatch("priceCurrency", () -> priceCurrencyMapper.getValue(rootObject, String.class)),
                tryCatch("quantity", () -> quantityMapper.getValue(rootObject, Integer.class)),
                tryCatch("nkd", () -> nkdMapper.getValue(rootObject, BigDecimal.class)),
                tryCatch("nkdCurrency", () -> nkdCurrencyMapper.getValue(rootObject, String.class)),
                tryCatch("fee", () -> feeMapper.getValue(rootObject, BigDecimal.class)),
                tryCatch("feeCurrency", () -> feeCurrencyMapper.getValue(rootObject, String.class)),
                tryCatch("note", () -> note.getValue(rootObject, String.class)),
                getAdditionalInfo(rootObject),
                rootObject.getLine()
        );
        if (dto.getTicker() == null || dto.getDate() == null || dto.getPrice() == null || dto.getOperation() == null)
            return Optional.empty();
        return Optional.of(dto);
    }

    @NotNull
    private Map<String, String> getAdditionalInfo(RootObject rootObject) {
        final var map = Maps.mutable.<String, String>withInitialCapacity(additionalData.size());
        for (Expression expression : additionalData) {
            final var pair = tryCatch("additionalInfo", () -> expression.getValue(rootObject, Pair.class));
            if (pair.getLeft() instanceof String key && pair.getRight() instanceof String value)
                map.put(key, value);
            else if (log.isDebugEnabled()) {
                log.debug("AdditionalInfo get not Pair<String,String>, {}", pair);
            }
        }
        return map;
    }

    @SneakyThrows
    LocalDateTime parseDate(String date) {
        for (DateTimeFormatter datePattern : dateTimeFormatters) {
            try {
                return LocalDateTime.parse(date, datePattern);
            } catch (DateTimeParseException ignore) {
            }
        }
        throw new ParseException("Error on parse date: '" + date + "'");
    }

    @NotNull
    private static SpelExpression parseExpression(String stringExpression, SpelExpressionParser parser, EvaluationContext context) {
        final var spelExpression = parser.parseRaw(stringExpression);
        spelExpression.setEvaluationContext(context);
        return spelExpression;
    }

    private static ImmutableList<SpelExpression> parseExpressions(List<String> stringExpressions, SpelExpressionParser parser, EvaluationContext context) {
        return Lists.adapt(stringExpressions)
                .collect(each -> parseExpression(each, parser, context))
                .toImmutable();
    }


    @SneakyThrows
    private <T> T tryCatch(String field, Supplier<T> supplier) {
        try {
            return supplier.get();
        } catch (Exception e) {
            throw new ParseException("field='%s', %s:%s".formatted(field,
                    e.getClass().getSimpleName(),
                    e.getLocalizedMessage()), e);
        }
    }

    public boolean isCompiled() {
        return dateMapper.compileExpression() &&
                settlementDateMapper.compileExpression() && tickerMapper.compileExpression()
                && isinMapper.compileExpression() && operationMapper.compileExpression()
                && priceMapper.compileExpression() && priceCurrencyMapper.compileExpression()
                && quantityMapper.compileExpression() && nkdMapper.compileExpression()
                && nkdCurrencyMapper.compileExpression() && feeMapper.compileExpression()
                && feeCurrencyMapper.compileExpression() && note.compileExpression()
                && additionalData.allSatisfy(SpelExpression::compileExpression);
    }
}
