package com.github.klawru.site.service.parser.excel;

import com.github.klawru.site.error.ParseException;
import lombok.Data;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.collections.api.factory.Maps;
import org.eclipse.collections.api.map.ImmutableMap;
import org.eclipse.collections.api.map.MutableMap;
import org.mapstruct.Context;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.StringJoiner;

@Data
public class RootObject {
    private final MutableMap<String, String> context = Maps.mutable.empty();
    private final MutableMap<String, String> row;
    long line;
    private final ImmutableMap<String, String> transformMap;

    public static Double parseDouble(String input) {
        return Double.parseDouble(input);
    }

    public static BigDecimal parseBigDecimal(String input) {
        return StringBigDecimalConverter.valueOf(input);
    }

    public String contextAdd(String key, String value) {
        return context.put(key, value);
    }

    public static Pair<String, String> pairOf(String first, String second) {
        return Pair.of(first, second);
    }

    public static String concatNotNull(String first, String second) {
        if (first != null)
            if (second != null)
                return first + " " + second;
            else
                return first;
        return second;
    }

    public static String concatNotNull(String[] strings) {
        final var stringJoiner = new StringJoiner(" ");
        for (String string : strings) {
            stringJoiner.add(string);
        }
        return stringJoiner.toString();
    }

    static LocalDateTime parseDate(String date, @Context DateTimeFormatter[] dateTimeFormatters) {
        for (DateTimeFormatter datePattern : dateTimeFormatters) {
            try {
                return LocalDateTime.parse(date, datePattern);
            } catch (DateTimeParseException ignore) {
            }
        }
        throw new ParseException("Error on parse date: '" + date + "'");
    }

}
