package com.github.klawru.site.service.parser.mapper;

import com.github.klawru.lib.contract.porfolio.v1.types.responce.DayBalanceDTO;
import com.github.klawru.site.repository.entity.DayBalance;
import lombok.extern.slf4j.Slf4j;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Slf4j
@Mapper(uses = CurrencyMapper.class)
public abstract class DayBalanceMapper {
    public static final DayBalanceMapper INSTANCE = Mappers.getMapper(DayBalanceMapper.class);

    public abstract DayBalanceDTO map(DayBalance dto);
}
