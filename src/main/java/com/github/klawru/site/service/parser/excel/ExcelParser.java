package com.github.klawru.site.service.parser.excel;

import com.github.klawru.site.repository.entity.TradeEntity;
import com.github.klawru.site.service.parser.BankMapper;
import com.github.klawru.site.service.parser.mapper.ParsedTradeMapper;
import com.github.klawru.site.utility.UuidGenerator;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.util.XMLHelper;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.eventusermodel.XSSFSheetXMLHandler;
import org.apache.poi.xssf.model.SharedStringsTable;
import org.apache.poi.xssf.model.StylesTable;
import org.eclipse.collections.api.list.ImmutableList;
import org.springframework.stereotype.Service;
import org.xml.sax.InputSource;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class ExcelParser {

    private final UuidGenerator uuidGenerator;
    private final ParsedTradeMapper mapper = ParsedTradeMapper.INSTANCE;

    @SneakyThrows
    public Mono<ImmutableList<TradeEntity>> parse(InputStream inputStream, UUID fileId, BankMapper bankMapper) {
        return Mono.fromCallable(() -> {
                    try (final var opcPackage = OPCPackage.open(inputStream)) {
                        return parse(opcPackage, bankMapper);
                    }
                })
                .subscribeOn(Schedulers.boundedElastic())
                .map(trades -> mapper.map(trades, uuidGenerator, fileId));
    }


    public Mono<ImmutableList<TradeEntity>> parse(File file, UUID fileId, BankMapper bankMapper) {
        return Mono.fromCallable(() -> {
                    try (final var opcPackage = OPCPackage.open(file)) {
                        return parse(opcPackage, bankMapper);
                    }
                })
                .subscribeOn(Schedulers.boundedElastic())
                .map(trades -> mapper.map(trades, uuidGenerator, fileId));
    }

    @SneakyThrows
    private static List<ParsedTrade> parse(OPCPackage opcPackage, BankMapper bankMapper) {
        final var reader = new XSSFReader(opcPackage);
        final var sheetParser = new SheetContentParser(bankMapper);
        var handler = getXSSFSheetHandler(reader, sheetParser);

        var result = new ArrayList<ParsedTrade>();
        var xmlReader = XMLHelper.newXMLReader();
        xmlReader.setContentHandler(handler);
        for (Iterator<InputStream> it = reader.getSheetsData(); it.hasNext(); ) {
            try (InputStream next = it.next()) {
                xmlReader.parse(new InputSource(next));
            }
            result.addAll(sheetParser.getResult());
            sheetParser.reset();
        }
        return result;
    }

    private static XSSFSheetXMLHandler getXSSFSheetHandler(XSSFReader reader, SheetContentParser sheetParser) throws IOException, InvalidFormatException {
        StylesTable styles = reader.getStylesTable();
        SharedStringsTable sharedStringsTable = reader.getSharedStringsTable();
        DataFormatter formatter = new IsoDataFormatter();
        return new XSSFSheetXMLHandler(styles, sharedStringsTable, sheetParser, formatter, true);
    }


}
