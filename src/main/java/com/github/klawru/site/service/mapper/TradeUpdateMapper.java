package com.github.klawru.site.service.mapper;

import com.github.klawru.lib.contract.porfolio.v1.types.request.TradeAddRequest;
import com.github.klawru.lib.contract.porfolio.v1.types.request.TradeUpdateRequest;
import com.github.klawru.site.repository.entity.Operation;
import com.github.klawru.site.repository.entity.TradeEntity;
import com.github.klawru.site.service.parser.excel.ParsedTrade;
import com.github.klawru.site.service.parser.mapper.ParsedTradeMapper;
import com.github.klawru.site.utility.UuidGenerator;
import org.apache.commons.lang3.RandomUtils;
import org.eclipse.collections.api.list.ImmutableList;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

import static com.github.klawru.site.utility.TradeUtility.positiveExact;

@Mapper
public abstract class TradeUpdateMapper {

    public static final TradeUpdateMapper INSTANCE = Mappers.getMapper(TradeUpdateMapper.class);

    @Mapping(target = "settlementDate", source = "date")
    @Mapping(target = "note", ignore = true)
    @Mapping(target = "nkdCurrency", ignore = true)
    @Mapping(target = "nkd", ignore = true)
    @Mapping(target = "line", ignore = true)
    @Mapping(target = "isin", ignore = true)
    @Mapping(target = "info", ignore = true)
    @Mapping(target = "priceCurrency", source = "currency")
    @Mapping(target = "feeCurrency", source = "currency")
    protected abstract ParsedTrade copyTo(TradeAddRequest request);

    public ImmutableList<TradeEntity> map(TradeAddRequest request, UuidGenerator uuidGenerator, UUID fileId) {
        ParsedTrade parsedTrade = copyTo(request);
        return ParsedTradeMapper.INSTANCE.map(List.of(parsedTrade), uuidGenerator, fileId);
    }


    @Mapping(target = "withId", ignore = true)
    public abstract TradeEntity copy(TradeEntity target);


    public TradeEntity copyForUpdate(TradeUpdateRequest request, TradeEntity trade) {
        TradeEntity target = copy(trade);
        if (request.getOperation() != null) {
            target.setOperation(operationToOperation(request.getOperation()));
        }
        if (request.getPrice() != null) {
            target.setPrice(request.getPrice());
        }
        if (!target.isMoney() && request.getFee() != null) {
            target.setFee(positiveExact(request.getFee()));
        }
        if (request.getQuantity() != null) {
            target.setQuantity(request.getQuantity());
            target.setTotalQuantity(null);
        }
        if (request.getNkd() != null) {
            target.setNkd(request.getNkd());
        }
        if (request.getNote() != null) {
            target.setNote(request.getNote());
        }
        if (!target.isMoney()
                && (request.getSum() != null || request.getQuantity() != null
                || request.getFee() != null || request.getPrice() != null)) {
            updateSum(request, target);
        }

        //Если добавляет пользователь, то сделка уникальна.
        target.setSeqId(RandomUtils.nextInt());
        target.convertNew();
        return target;
    }

    abstract Operation operationToOperation(com.github.klawru.lib.contract.porfolio.v1.types.responce.Operation operation);

    void updateSum(TradeUpdateRequest request, TradeEntity target) {
        if (target.isMoney())
            return;
        BigDecimal sum = request.getSum();
        if (sum != null)
            target.setSum(sum);
        else if (request.getQuantity() != null || request.getFee() != null || request.getPrice() != null) {
            target.setSum(null);
        }
    }

}
