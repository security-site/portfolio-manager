package com.github.klawru.site.service;

import com.github.klawru.lib.contract.porfolio.v1.types.request.TradeAddRequest;
import com.github.klawru.lib.contract.porfolio.v1.types.request.TradeDeleteRequest;
import com.github.klawru.lib.contract.porfolio.v1.types.request.TradeUpdateRequest;
import com.github.klawru.lib.contract.porfolio.v1.types.request.TradesRequest;
import com.github.klawru.lib.contract.porfolio.v1.types.responce.TradeDTO;
import com.github.klawru.site.error.BadRequestException;
import com.github.klawru.site.error.InternalServerError;
import com.github.klawru.site.repository.FileInfoRepository;
import com.github.klawru.site.repository.TradeRepository;
import com.github.klawru.site.repository.entity.SecurityKey;
import com.github.klawru.site.repository.entity.TradeEntity;
import com.github.klawru.site.repository.entity.TradesFileInfo;
import com.github.klawru.site.service.mapper.MoneyTradeGenerator;
import com.github.klawru.site.service.mapper.TradeDTOMapper;
import com.github.klawru.site.service.mapper.TradeUpdateMapper;
import com.github.klawru.site.utility.UuidGenerator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.collections.api.list.ImmutableList;
import org.eclipse.collections.api.list.MutableList;
import org.eclipse.collections.api.map.MutableOrderedMap;
import org.eclipse.collections.api.multimap.list.ListMultimap;
import org.eclipse.collections.impl.factory.Lists;
import org.eclipse.collections.impl.factory.Multimaps;
import org.eclipse.collections.impl.factory.OrderedMaps;
import org.springframework.data.mapping.callback.ReactiveEntityCallbacks;
import org.springframework.data.r2dbc.mapping.event.BeforeConvertCallback;
import org.springframework.data.relational.core.sql.SqlIdentifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.reactive.TransactionalOperator;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.Nonnull;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.UUID;

import static com.github.klawru.site.security.SecurityUtils.getCurrentUser;
import static com.github.klawru.site.security.SecurityUtils.getCurrentUserId;
import static com.github.klawru.site.utility.TradeUtility.groupBySecurityKey;
import static java.lang.String.format;
import static org.eclipse.collections.impl.collector.Collectors2.toList;
import static org.springframework.util.CollectionUtils.isEmpty;

/**
 * Большинство методов ожидает отсортированные данные по дате от старых к новым
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class TradesService {
    private final TradeRepository tradeRepository;
    private final PortfolioService portfolioService;
    private final FileInfoRepository fileInfoRepository;
    private final UuidGenerator uuidGenerator;

    private final ReactiveEntityCallbacks auditingEntityCallback;
    private final TransactionalOperator operator;

    private static final TradeDTOMapper tradeMapper = TradeDTOMapper.INSTANCE;
    private static final TradeUpdateMapper tradeUpdateMapper = TradeUpdateMapper.INSTANCE;
    private static final MoneyTradeGenerator tradeMoneyMapper = MoneyTradeGenerator.INSTANCE;


    public Flux<TradeDTO> getTrades(TradesRequest request) {
        return getCurrentUserId()
                .flatMapMany(uuid -> {
                    if (request.getFrom() != null)
                        return tradeRepository.findAllByUserIdAndDateAfterOrderByDateDescIdDesc(uuid, request.getFrom().atStartOfDay());
                    if (!isEmpty(request.getFileIdList()))
                        return tradeRepository.findAllByFileIdInOrderByDateDescIdDesc(request.getFileIdList());
                    //Load all trades
                    return tradeRepository.findAllByUserIdOrderByDateDescIdDesc(uuid);
                })
                .map(tradeMapper::map);
    }


    /**
     * @param tradeEntities список сделок
     * @param fileUpload    информация о файле со сделками
     * @return количство сохраненных сделок
     */
    @Transactional
    public Mono<Long> saveTrades(ImmutableList<TradeEntity> tradeEntities, TradesFileInfo fileUpload) {
        return batchUpsert(tradeEntities)
                .collect(groupBySecurityKey())
                .filter(collection -> !collection.isEmpty())
                .flatMap(savedTrades -> {
                    int countSubMoneyTrades = savedTrades.valuesView().count(each -> each.getParentId() != null);
                    long size = savedTrades.size() - countSubMoneyTrades;
                    return getCurrentUserId().flatMap(userId ->
                            afterTradeUpdate(userId, savedTrades, Multimaps.immutable.list.empty())
                                    .then(saveFileInfo(fileUpload))
                                    .then(Mono.just(size)));
                })
                .defaultIfEmpty(0L);
    }

    public Mono<Long> addTrade(TradeAddRequest request) {
        return getUserFileInfo().flatMap(tradesFileInfo -> {
            ImmutableList<TradeEntity> trade = tradeUpdateMapper.map(request, uuidGenerator, tradesFileInfo.getId());
            return saveTrades(trade, tradesFileInfo);
        });
    }

    @Nonnull
    private Mono<TradesFileInfo> getUserFileInfo() {
        return getCurrentUser().flatMap(user -> fileInfoRepository.findById(user.getId())
                .switchIfEmpty(Mono.defer(() -> {
                    TradesFileInfo handMade = new TradesFileInfo(user.getId(), user.getUsername());
                    return fileInfoRepository.save(handMade);
                })));
    }

    @Transactional
    public Mono<Void> updateTrade(TradeUpdateRequest request) {
        return getCurrentUserId().flatMap(userId -> findTradePairById(request.getId(), userId)
                .flatMap(tradeEntities -> {
                    var newTrade = updateTrade(request, tradeEntities);
                    return tradeRepository.saveAll(newTrade.valuesView())
                            .collect(groupBySecurityKey())
                            .flatMap(savedTrades -> {
                                UUID fileId = savedTrades.valuesView().getAny().getFileId();
                                var updateFileInfo = fileInfoRepository.findById(fileId)
                                        .flatMap(this::saveFileInfo);
                                return afterTradeUpdate(userId, savedTrades, tradeEntities.groupBy(TradeEntity::getSecurityKey))
                                        .then(updateFileInfo);
                            }).then();
                }));
    }

    @Transactional
    public Mono<Void> deleteTrades(TradeDeleteRequest request) {
        return getCurrentUserId()
                .flatMap(userId -> findTradePairById(request.getId(), userId)
                        .map(tradeList -> tradeList.reduceInPlace(groupBySecurityKey()))
                        .flatMapMany(tradeMap -> portfolioService.deleteStockPortfolio(tradeMap, userId))
                        .then(tradeRepository.deleteById(request.getId()))
                );

    }

    @Nonnull
    private ListMultimap<SecurityKey, TradeEntity> updateTrade(TradeUpdateRequest request, MutableList<TradeEntity> tradeEntities) {
        if (tradeEntities.size() == 1) {
            TradeEntity money = tradeEntities.detectOptional(TradeEntity::isMoney)
                    .orElseThrow(() -> new BadRequestException("Найдена толька одна сделка, и она должна быть с валютной"));
            TradeEntity copyMoney = tradeUpdateMapper.copyForUpdate(request, money);
            return Multimaps.immutable.list.of(copyMoney.getSecurityKey(), copyMoney);
        } else if (tradeEntities.size() == 2) {
            TradeEntity trade = tradeEntities.detectOptional(entity -> !entity.isMoney())
                    .orElseThrow(() -> new BadRequestException("Одна из сделок дожна быть с ценными бумагами"));
            TradeEntity money = tradeEntities.detectOptional(TradeEntity::isMoney)
                    .orElseThrow(() -> new BadRequestException("Одна из сделок дожна быть с валютой"));

            TradeEntity copyTrade = tradeUpdateMapper.copyForUpdate(request, trade);
            TradeEntity copyMoney = tradeMoneyMapper.copyForUpdate(trade, money);
            return Multimaps.immutable.list.of(
                    copyTrade.getSecurityKey(), copyTrade,
                    copyMoney.getSecurityKey(), copyMoney);
        } else
            throw new InternalServerError(format("Найдено более двух сделок по request=%s, trades=%s", request, tradeEntities));
    }

    /**
     * @param tradeId id сделки
     * @param userId  id пользователя
     * @return саму сделку и ее дочернюю сделку с валютой
     */
    @Nonnull
    private Mono<MutableList<TradeEntity>> findTradePairById(UUID tradeId, UUID userId) {
        return tradeRepository.findByIdWithSubTrade(userId, tradeId)
                .collect(toList())
                .doOnNext(list -> {
                    //Проверяем, что не изменяют дочернюю сделку
                    if (list.size() == 1 && list.getFirst().getParentId() != null)
                        throw new BadRequestException("Нельзя изменять дочернии сделки");
                })
                .switchIfEmpty(Mono.error(() -> new BadRequestException("Не найдена сделка")));
    }

    private Mono<TradesFileInfo> saveFileInfo(TradesFileInfo fileUpload) {
        return fileInfoRepository.save(fileUpload);
    }

    /**
     * @param entityList список сделок для сохранения
     * @return сохраненные сделки
     */
    protected Flux<TradeEntity> batchUpsert(ImmutableList<TradeEntity> entityList) {
        return Flux.fromIterable(entityList)
                .concatMap(tradeEntity -> auditingEntityCallback.callback(BeforeConvertCallback.class, tradeEntity, SqlIdentifier.EMPTY))
                .concatMap(entity -> tradeRepository.upsert(entity)
                        .filter(Boolean::booleanValue)
                        .flatMap(aBoolean -> Mono.just(entity)));
    }

    protected Mono<Void> afterTradeUpdate(UUID userId,
                                          ListMultimap<SecurityKey, TradeEntity> newTrades,
                                          ListMultimap<SecurityKey, TradeEntity> oldTrades) {
        return Flux.concat(
                        updateTradeTotalQuantity(userId, newTrades),
                        portfolioService.updateStockPortfolio(userId, newTrades, oldTrades)
                )
                .then();
    }

    /**
     * Метод выберает самые ранние сделки из списка newTransaction, и обновляет все сделки после данной даты
     *
     * @param userId         пользователя
     * @param newTransaction сделки должны быть отсортированы по тикеру, дате, времени
     */
    protected Mono<Void> updateTradeTotalQuantity(UUID userId, ListMultimap<SecurityKey, TradeEntity> newTransaction) {
        final var securities = new FirstTransactionDate();
        final var money = new FirstTransactionDate();
        //Разделяем сделки с денежными средствами и с ценными бумагами
        for (SecurityKey ticker : newTransaction.keySet()) {
            //Берем самую первую сделку (у нее самая ранняя дата)
            final var firstTrade = newTransaction.get(ticker).getFirst();
            if (firstTrade.isMoney()) {
                money.put(ticker, firstTrade);
            } else {
                securities.put(ticker, firstTrade);
            }
        }

        Flux<Void> calcFlux = Flux.empty();
        if (money.size() > 0)
            calcFlux = calcFlux.concatWith(tradeRepository.updateMoneySum(userId, money.getTickers(), money.getFromDates()));
        if (securities.size() > 0)
            calcFlux = calcFlux.concatWith(tradeRepository.updateTotalQuantity(userId,
                    securities.getTickers(),
                    securities.getExchanges(),
                    securities.getFromDates()));
        return calcFlux.then();
    }


    private static class FirstTransactionDate {
        MutableOrderedMap<SecurityKey, LocalDateTime> fromDate = OrderedMaps.adapt(new LinkedHashMap<>());

        MutableList<String> getTickers() {
            return fromDate.keysView().collect(SecurityKey::getTicker, Lists.mutable.empty());
        }

        MutableList<String> getExchanges() {
            return fromDate.keysView().collect(SecurityKey::getExchange, Lists.mutable.empty());
        }

        Collection<LocalDateTime> getFromDates() {
            return fromDate.values();
        }

        public int size() {
            return fromDate.size();
        }

        public void put(SecurityKey key, TradeEntity trade) {
            fromDate.putIfAbsent(key, trade.getDate());
        }
    }


}
