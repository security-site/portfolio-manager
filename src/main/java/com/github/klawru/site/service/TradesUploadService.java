package com.github.klawru.site.service;

import com.github.klawru.site.repository.entity.TradeEntity;
import com.github.klawru.site.repository.entity.TradesFileInfo;
import com.github.klawru.site.utility.UuidGenerator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.collections.api.list.ImmutableList;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class TradesUploadService {

    private final FileParserService parserService;
    private final TradesService service;
    private final UuidGenerator uuidGenerator;

    public Mono<Long> uploadFile(Flux<DataBuffer> filePartMono, String bankImportType, String filename, Long contentLength) {
        var fileId = uuidGenerator.generate();
        TradesFileInfo fileUpload = new TradesFileInfo(fileId, filename);
        return parseFile(filePartMono, bankImportType, contentLength, fileId)
                .flatMap(tradeEntities -> service.saveTrades(tradeEntities, fileUpload));
    }

    private Mono<ImmutableList<TradeEntity>> parseFile(Flux<DataBuffer> filePartMono, String bankImportType, Long contentLength, UUID fileId) {
        return parserService.parseFile(bankImportType, fileId, contentLength, filePartMono);
    }


}
