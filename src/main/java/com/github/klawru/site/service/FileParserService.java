package com.github.klawru.site.service;


import com.github.klawru.site.config.ApplicationProperties;
import com.github.klawru.site.repository.entity.TradeEntity;
import com.github.klawru.site.service.parser.BankMapper;
import com.github.klawru.site.service.parser.excel.ExcelExpressionParserService;
import com.github.klawru.site.service.parser.excel.ExcelParser;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.eclipse.collections.api.list.ImmutableList;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.io.IOException;
import java.nio.channels.AsynchronousFileChannel;
import java.nio.channels.Channel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Supplier;

@Slf4j
@Service
public class FileParserService {

    private final ExcelExpressionParserService bankMapperRepository;
    private final ExcelParser parser;
    private final Supplier<ApplicationProperties.ParseConfiguration> properties;

    public FileParserService(ExcelExpressionParserService bankMapperRepository, ExcelParser parser, ObjectFactory<ApplicationProperties> properties) {
        this.bankMapperRepository = bankMapperRepository;
        this.parser = parser;
        this.properties = () -> properties.getObject().getFileParse();
    }

    //todo xls, html, csv
    public Mono<ImmutableList<TradeEntity>> parseFile(String bankImportType, UUID fileId, Long contentLength, Flux<DataBuffer> content) {
        return getBankMapper(bankImportType).flatMap(bankMapper -> {
            if (contentLength == null || contentLength > getMaxMemorySize()) {
                return Mono.usingWhen(
                        saveToFile(content),
                        path -> this.parser.parse(path.toFile(), fileId, bankMapper),
                        this::deleteFile);

            } else {
                return DataBufferUtils.join(content, (int) getMaxMemorySize())
                        .flatMap(dataBuffer -> this.parser.parse(dataBuffer.asInputStream(true), fileId, bankMapper));
            }
        });
    }

    private Mono<BankMapper> getBankMapper(String bankImportType) {
        return bankMapperRepository.bankData(bankImportType);
    }

    private Mono<Boolean> deleteFile(Path path) {
        return Mono.fromCallable(() -> FileUtils.deleteQuietly(path.toFile())).subscribeOn(Schedulers.boundedElastic());
    }

    private long getMaxMemorySize() {
        return properties.get().getInMemorySize().toBytes();
    }

    private long getMaxSize() {
        return properties.get().getMaxSize().toBytes();
    }

    Mono<Path> saveToFile(Flux<DataBuffer> data) {
        return Mono.create(sink -> {
            try {
                Path tempFile = Files.createTempFile("tmp-", ".temp");
                log.debug("tempFile:{}", tempFile);
                AsynchronousFileChannel channel = AsynchronousFileChannel.open(tempFile, StandardOpenOption.WRITE);
                sink.onDispose(() -> closeChannel(channel));
                DataBufferUtils.write(data, channel).subscribe((v) -> {
                        },
                        sink::error,
                        () -> sink.success(tempFile));
            } catch (IOException ex) {
                sink.error(ex);
            }
        });
    }


    public static Flux<DataBuffer> takeUntilByteCount(Publisher<DataBuffer> publisher, long maxByteCount) {
        return Flux.defer(() -> {
            AtomicLong countDown = new AtomicLong(maxByteCount);
            return Flux.from(publisher)
                    .map(buffer -> {
                        long remainder = countDown.addAndGet(-buffer.readableByteCount());
                        if (remainder < 0) {
                            throw new MaxUploadSizeExceededException(maxByteCount);
                        }
                        return buffer;
                    })
                    .takeUntil(buffer -> countDown.get() <= 0);
        });
    }

    static void closeChannel(@Nullable Channel channel) {
        if (channel != null && channel.isOpen()) {
            try {
                channel.close();
            } catch (IOException ignored) {
            }
        }
    }
}
