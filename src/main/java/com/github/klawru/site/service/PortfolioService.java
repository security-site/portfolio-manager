package com.github.klawru.site.service;

import com.github.klawru.lib.contract.porfolio.v1.types.responce.StockPortfolioDTO;
import com.github.klawru.site.repository.StockPortfolioRepository;
import com.github.klawru.site.repository.entity.SecurityKey;
import com.github.klawru.site.repository.entity.StockPortfolio;
import com.github.klawru.site.repository.entity.TradeEntity;
import com.github.klawru.site.repository.entity.projection.QuantityAveragePriceDTO;
import com.github.klawru.site.service.mapper.StockPortfolioMapper;
import com.github.klawru.site.utility.UuidGenerator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.collections.api.list.ListIterable;
import org.eclipse.collections.api.map.MutableMap;
import org.eclipse.collections.api.multimap.list.ListMultimap;
import org.eclipse.collections.api.set.SetIterable;
import org.eclipse.collections.impl.factory.Lists;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.util.UUID;

import static com.github.klawru.site.security.SecurityUtils.getCurrentUserId;
import static com.github.klawru.site.utility.TradeUtility.toMapBySecurityKey;

@Slf4j
@Service
@RequiredArgsConstructor
public class PortfolioService {

    private final StockPortfolioRepository portfolioRepository;
    private final UuidGenerator generator;
    private final StockPortfolioMapper mapper = StockPortfolioMapper.INSTANCE;

    public Flux<StockPortfolioDTO> getPortfolio() {
        return getCurrentUserId()
                .flatMapMany(portfolioRepository::findByUserId)
                .map(mapper::map);
    }

    /**
     * @param userId      id пользователя
     * @param addedTrades новые сделки, которые добавляем  портфолио
     * @param oldTrades   старые сделки, которые необходимо удалить из портфолио. Тикеры должны совпадать с addedTrades
     * @return сохраненные
     */
    public Flux<StockPortfolio> updateStockPortfolio(UUID userId,
                                                     ListMultimap<SecurityKey, TradeEntity> addedTrades,
                                                     ListMultimap<SecurityKey, TradeEntity> oldTrades) {
        Mono<MutableMap<SecurityKey, StockPortfolio>> portfolio = findStockPortfolio(userId, addedTrades.keySet());
        Mono<MutableMap<SecurityKey, QuantityAveragePriceDTO>> average = getAveragePrice(userId, addedTrades.keySet());
        return Mono.zip(portfolio, average)
                .map(pair -> {
                    var portfolios = pair.getT1();
                    var averagePriceMap = pair.getT2();
                    removeTradeToPortfolio(oldTrades, portfolios);
                    addTradeToPortfolio(addedTrades, portfolios);
                    setAverageQuantityToPortfolio(averagePriceMap, portfolios);
                    return portfolios.values();
                })
                .flatMapMany(portfolioRepository::saveAll);
    }

    /**
     * @param oldTrades старые сделки, которые необходимо удалить из портфолио.
     * @param userId    id пользователя
     * @return
     */
    public Flux<StockPortfolio> deleteStockPortfolio(ListMultimap<SecurityKey, TradeEntity> oldTrades, UUID userId) {
        return findStockPortfolio(userId, oldTrades.keySet())
                .zipWith(getAveragePrice(userId, oldTrades.keySet()))
                .map(pair -> {
                    MutableMap<SecurityKey, StockPortfolio> portfolio = pair.getT1();
                    MutableMap<SecurityKey, QuantityAveragePriceDTO> averagePrices = pair.getT2();
                    removeTradeToPortfolio(oldTrades, portfolio);
                    setAverageQuantityToPortfolio(averagePrices, portfolio);
                    return portfolio.values();
                })
                .flatMapMany(portfolioRepository::saveAll);
    }

    private void setAverageQuantityToPortfolio(MutableMap<SecurityKey, QuantityAveragePriceDTO> averagePrices,
                                               MutableMap<SecurityKey, StockPortfolio> portfolio) {
        for (var key : averagePrices.keySet()) {
            var quantityAveragePriceDTO = averagePrices.get(key);
            var average = quantityAveragePriceDTO.getAveragePrice();
            var totalQuantity = quantityAveragePriceDTO.getTotalQuantity();
            var stockPortfolio = portfolio.get(key);
            if (average == null)
                stockPortfolio.setAveragePrice(BigDecimal.ZERO);
            int calcTotalQuantity = stockPortfolio.getBuyQuantity() + stockPortfolio.getSaleQuantity();
            if (!stockPortfolio.isMoney() && totalQuantity != null && calcTotalQuantity != totalQuantity)
                log.warn("Значения totalQuantity расходяться calc='{}', totalQuantity='{}', portfolio='{}'",
                        calcTotalQuantity,
                        totalQuantity,
                        stockPortfolio.getId());
        }
    }


    private void removeTradeToPortfolio(ListMultimap<SecurityKey, TradeEntity> oldTrades, MutableMap<SecurityKey, StockPortfolio> tickerPortfolio) {
        for (var key : oldTrades.keySet()) {
            final var tradeList = oldTrades.get(key);
            var stockPortfolio = tickerPortfolio.get(key);
            removeFromPortfolio(stockPortfolio, tradeList);
        }
    }

    private void addTradeToPortfolio(ListMultimap<SecurityKey, TradeEntity> addedTrades,
                                     MutableMap<SecurityKey, StockPortfolio> portfolios) {
        for (SecurityKey key : addedTrades.keySet()) {
            final var tradeList = addedTrades.get(key);
            var stockPortfolio = portfolios.computeIfAbsent(key, securityKey -> createNewPortfolio(tradeList));
            calcPortfolio(stockPortfolio, tradeList);
        }
    }

    private Mono<MutableMap<SecurityKey, QuantityAveragePriceDTO>> getAveragePrice(UUID userId, SetIterable<SecurityKey> securityKeys) {
        return portfolioRepository.calcAveragePrice(userId,
                        securityKeys.collect(SecurityKey::getTicker, Lists.mutable.empty()),
                        securityKeys.collect(SecurityKey::getExchange, Lists.mutable.empty())
                )
                .collect(toMapBySecurityKey());
    }

    public Mono<MutableMap<SecurityKey, StockPortfolio>> findStockPortfolio(UUID userId, SetIterable<SecurityKey> securityKeys) {
        return portfolioRepository.findByUserIdAndTickerIn(userId,
                        securityKeys.collect(SecurityKey::getTicker, Lists.mutable.empty()),
                        securityKeys.collect(SecurityKey::getExchange, Lists.mutable.empty())
                )
                .collect(toMapBySecurityKey());
    }

    private void calcPortfolio(StockPortfolio stockPortfolio, ListIterable<TradeEntity> tradeList) {
        for (TradeEntity trade : tradeList) {
            switch (trade.getOperation()) {
                case BUY -> {
                    stockPortfolio.addBuyQuantity(trade.getQuantity());
                    stockPortfolio.addBuySumPrice(sumPrice(trade));
                    stockPortfolio.addBuyFee(trade.getFee());
                }
                case SALE -> {
                    stockPortfolio.addSellQuantity(trade.getQuantity());
                    stockPortfolio.addSaleSumPrice(sumPrice(trade));
                    stockPortfolio.addSaleFee(trade.getFee());
                }
            }
        }
        tradeList.getLastOptional().ifPresent(trade -> stockPortfolio.setLastTransaction(trade.getDate()));
    }

    private void removeFromPortfolio(StockPortfolio stockPortfolio, ListIterable<TradeEntity> tradeList) {
        for (TradeEntity trade : tradeList) {
            switch (trade.getOperation()) {
                case BUY -> {
                    stockPortfolio.addBuyQuantity(-trade.getQuantity());
                    stockPortfolio.addBuySumPrice(sumPrice(trade).negate());
                    stockPortfolio.addBuyFee(trade.getFee().negate());
                }
                case SALE -> {
                    stockPortfolio.addSellQuantity(-trade.getQuantity());
                    stockPortfolio.addSaleSumPrice(sumPrice(trade).negate());
                    stockPortfolio.addSaleFee(trade.getFee().negate());
                }
            }
        }
        //todo setLastTransaction
    }

    private StockPortfolio createNewPortfolio(ListIterable<TradeEntity> tradeList) {
        //Берем самую первую сделку (у нее самая ранняя дата)
        final var entity = tradeList.getFirst();
        return StockPortfolio.createNew(generator.generate(), entity.getTicker(), entity.getExchange(),
                entity.getPriceCurrency(), entity.getFeeCurrency(),
                entity.getDate());
    }

    private BigDecimal sumPrice(TradeEntity entity) {
        if (entity.isMoney())
            return entity.getPrice();
        else
            return entity.getSum();
    }
}
