package com.github.klawru.site.service.parser.excel;

import com.github.klawru.site.error.ParseException;
import com.github.klawru.site.repository.BankMapperRepository;
import com.github.klawru.site.repository.entity.BankMapperEntity;
import com.github.klawru.site.service.parser.BankMapper;
import org.eclipse.collections.api.factory.Sets;
import org.eclipse.collections.api.list.ImmutableList;
import org.eclipse.collections.api.list.MutableList;
import org.eclipse.collections.impl.factory.Lists;
import org.eclipse.collections.impl.factory.Maps;
import org.springframework.core.convert.support.DefaultConversionService;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.spel.SpelCompilerMode;
import org.springframework.expression.spel.SpelParserConfiguration;
import org.springframework.expression.spel.standard.SpelExpression;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.DataBindingMethodResolver;
import org.springframework.expression.spel.support.SimpleEvaluationContext;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import javax.validation.constraints.NotNull;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.eclipse.collections.impl.tuple.Tuples.pair;

@Service
public class ExcelExpressionParserService {

    private final SpelExpressionParser parser;
    private final BankMapperRepository repository;

    final EvaluationContext evaluationContext;


    public ExcelExpressionParserService(BankMapperRepository repository) throws NoSuchMethodException {
        this.repository = repository;
        final var conversionService = new DefaultConversionService();
        conversionService.addConverter(new StringBigDecimalConverter());
        evaluationContext = SimpleEvaluationContext
                .forReadOnlyDataBinding()
                .withMethodResolvers(DataBindingMethodResolver.forInstanceMethodInvocation())
                .withConversionService(conversionService)
                .build();

        evaluationContext.setVariable("asD", RootObject.class.getDeclaredMethod("parseDouble", String.class));
        evaluationContext.setVariable("asBD", RootObject.class.getDeclaredMethod("parseBigDecimal", String.class));
        evaluationContext.setVariable("pair", RootObject.class.getDeclaredMethod("pairOf", String.class, String.class));
        evaluationContext.setVariable("cnn", RootObject.class.getDeclaredMethod("concatNotNull", String.class, String.class));
        parser = new SpelExpressionParser(new SpelParserConfiguration(SpelCompilerMode.IMMEDIATE, null));
    }


    public Mono<BankMapper> bankData(String bankImportId) {
        return repository.findByBankId(bankImportId)
                .map(this::mapToMapper)
                .switchIfEmpty(Mono.error(new ParseException("Unknown bank type for import")));
    }

    @NotNull
    private BankMapper mapToMapper(BankMapperEntity entity) {
        final var contextFillers = Lists.adapt(entity.getContextFillers())
                .collect(this::parseExpression);
        final var tradesRowMapper = new TradesRowMapper(
                parser, evaluationContext,
                entity.getDate(),
                entity.getSettlementDate(),
                entity.getTicker(),
                entity.getExchange(),
                entity.getIsin(),
                entity.getOperation(),
                entity.getPrice(),
                entity.getSum(),
                entity.getPriceCurrency(),
                entity.getQuantity(),
                entity.getNkd(),
                entity.getNkdCurrency(),
                entity.getFee(),
                entity.getFeeCurrency(),
                entity.getNote(),
                entity.getAdditionalData(),
                parseDatePatterns(entity.getDatePatterns())
        );
        final Map<?, ?> map = Optional.ofNullable(parseExpression(entity.getTransformMap()).getValue(Map.class))
                .orElse(Collections.emptyMap());
        final var transformMap = Maps.adapt(map)
                .collect((key, value) -> pair(key.toString(), value.toString()));
        return new BankMapper(
                Sets.immutable.withAll(entity.getHeaders()),
                parseExpression(entity.getStartData()),
                parseExpression(entity.getEndData()),
                tradesRowMapper,
                contextFillers.toImmutable(),
                transformMap.toImmutable()
        );
    }

    private ImmutableList<DateTimeFormatter> parseDatePatterns(List<String> datePatterns) {
        final MutableList<DateTimeFormatter> formatters = Lists.mutable.withInitialCapacity(datePatterns.size());
        for (String datePattern : datePatterns) {
            if ("iso".equals(datePattern)) {
                formatters.add(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
            } else {
                formatters.add(DateTimeFormatter.ofPattern(datePattern));
            }
        }

        if (datePatterns.isEmpty()) {
            throw new IllegalArgumentException("datePatterns cannot be empty");
        }
        return formatters.toImmutable();
    }

    @NotNull
    private SpelExpression parseExpression(String expression) {
        final var spelExpression = parser.parseRaw(expression);
        spelExpression.setEvaluationContext(evaluationContext);
        return spelExpression;
    }

}
