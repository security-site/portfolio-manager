package com.github.klawru.site.service;

import com.github.kagkarlsson.scheduler.task.ExecutionContext;
import com.github.kagkarlsson.scheduler.task.TaskInstance;
import com.github.klawru.site.repository.DayBalanceRepository;
import com.github.klawru.site.repository.StockPortfolioRepository;
import com.github.klawru.site.repository.TradeRepository;
import com.github.klawru.site.repository.entity.DayBalance;
import com.github.klawru.site.repository.entity.SecurityKey;
import com.github.klawru.site.repository.entity.StockPortfolio;
import com.github.klawru.site.repository.entity.TradeEntity;
import com.github.klawru.site.service.mapper.CurrencyMapper;
import com.github.klawru.site.utility.ConversationService;
import com.github.klawru.site.utility.PairKey;
import com.github.klawru.site.utility.SecurityPrice;
import com.github.klawru.site.utility.TradeUtility;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.collections.api.RichIterable;
import org.eclipse.collections.api.list.MutableList;
import org.eclipse.collections.api.map.ImmutableMap;
import org.eclipse.collections.api.map.MutableMap;
import org.eclipse.collections.api.map.primitive.MutableObjectIntMap;
import org.eclipse.collections.api.map.primitive.ObjectIntMap;
import org.eclipse.collections.api.multimap.list.MutableListMultimap;
import org.eclipse.collections.api.tuple.Pair;
import org.eclipse.collections.impl.collector.Collectors2;
import org.eclipse.collections.impl.factory.Maps;
import org.eclipse.collections.impl.factory.primitive.ObjectIntMaps;
import org.eclipse.collections.impl.map.mutable.primitive.ObjectIntHashMap;
import org.eclipse.collections.impl.tuple.Tuples;
import org.javamoney.moneta.Money;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.Nonnull;
import javax.money.CurrencyUnit;
import javax.money.Monetary;
import javax.money.MonetaryAmount;
import java.math.BigDecimal;
import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.util.*;

import static org.eclipse.collections.impl.collector.Collectors2.toList;
import static org.eclipse.collections.impl.collector.Collectors2.toSortedList;

@Slf4j
@Service
@RequiredArgsConstructor
public class DayBalanceCalcService {
    private final ConversationService currencyConversationService;
    private final StockPortfolioRepository portfolioRepository;
    private final TradeRepository tradeRepository;
    private final TradePriceService tradePriceService;
    private final DayBalanceRepository dayBalanceRepository;
    private final Clock clock;


    private final static int DEFAULT_DAYS_CALCULATION = 11;
    private final static CurrencyMapper currencyMapper = CurrencyMapper.INSTANCE;

    public void calcPortfolio(TaskInstance<UUID> instance, ExecutionContext context) {
        final var userId = instance.getData();
        log.info("start task calcPortfolio userId='{}'", userId);
        calcDayBalanceForUser(userId)
                .doFinally(s -> log.info("doFinally for userId='{}', signal={}", userId, s.toString()))
                .doOnSuccess(c -> log.info("doOnSuccess for userId='{}'", userId))
                .block(Duration.ofMinutes(5));
        log.info("Success calc for userId='{}'", userId);
    }

    protected Mono<Void> calcDayBalanceForUser(UUID userId) {
        Mono<LocalDate> localDateMono = calcFromDate(userId, DEFAULT_DAYS_CALCULATION);
        return localDateMono
                .zipWhen(fromDate -> getTradesFrom(userId, fromDate))
                .flatMap(pairDateSecurity -> {
                            final LocalDate calcFromDate = pairDateSecurity.getT1();
                            var dateTradeMap = pairDateSecurity.getT2();
                            return getStockPortfolio(userId)
                                    .zipWhen(portfolioPair -> tradePriceService.getAllSecurityPrice(calcFromDate, portfolioPair.getTwo().keySet()))
                                    .flatMapMany(pair -> {
                                        var securityPriceMap = pair.getT2();
                                        var currencySumMap = pair.getT1().getOne();
                                        var securityQuantityMap = pair.getT1().getTwo();
                                        var resultCalc = Flux.<DayBalance>empty();

                                        final LocalDate toDate = LocalDate.now(clock).minusDays(1);
                                        //убираем все сделки, дальше сегодняшнего числа(Которые еще не свершились)
                                        dateTradeMap.keysView().select(each -> each.isAfter(toDate))
                                                .forEach(each -> removeDayFrom(dateTradeMap.get(each), currencySumMap, securityQuantityMap));
                                        //Идем от настоящего к прошлому, убирая сделки из последнего состояния
                                        for (long i = toDate.toEpochDay(); i > calcFromDate.toEpochDay(); i--) {
                                            var date = LocalDate.ofEpochDay(i);
                                            MutableMap<CurrencyUnit, MonetaryAmount> dayResult = Maps.mutable.empty();
                                            calcSecuritySumOnDay(date, securityQuantityMap, securityPriceMap, dayResult);
                                            calcMoneySum(currencySumMap, dayResult);
                                            resultCalc = resultCalc.concatWith(reduceDayResult(userId, date, dayResult));
                                            removeDayFrom(dateTradeMap.get(date), currencySumMap, securityQuantityMap);
                                        }
                                        return resultCalc;
                                    })
                                    .collect(toSortedList(Comparator.comparing(DayBalance::getDate)))
                                    .flatMapMany(dayBalanceRepository::saveAll)
                                    .then();
                        }
                );
    }

    /**
     * @param userId        идентификатор пользователя
     * @param maxDayForCalc максимальное количество дней для подсчета
     * @return дату последнего расчет дневного баланса, но не больше чем maxDayForCalc дней назад
     */
    @Nonnull
    private Mono<LocalDate> calcFromDate(UUID userId, int maxDayForCalc) {
        return dayBalanceRepository.findFirstByUserIdOrderByDateDesc(userId)
                .map(DayBalance::getDate)
                //Если пусто берем дату первой сделки
                .switchIfEmpty(tradeRepository.findFirstByUserIdOrderByDateAscIdAsc(userId)
                        .map(entity -> entity.getDate().toLocalDate())
                )
                .map(existDate -> {
                    final var maxDateForCalc = LocalDate.now(clock).minusDays(maxDayForCalc);
                    if (existDate.isBefore(maxDateForCalc))
                        return maxDateForCalc;
                    else
                        return existDate;
                });
    }

    /**
     * Получить с указанной даты все сделки
     */
    private Mono<MutableListMultimap<LocalDate, TradeEntity>> getTradesFrom(UUID userId, LocalDate fromDate) {
        return tradeRepository.findAllFromDate(userId, fromDate)
                .collect(Collectors2.toListMultimap(TradeEntity::getLocalDate));
    }

    /**
     * Метод вычетает сделки из текущего состояния востанавливая предыдущий день
     *
     * @param tradeEntities сделки, которые будут убраны
     * @param moneyMap      количество денежных средств
     * @param tradeMap      количество ценных бумаг средств
     */
    private void removeDayFrom(RichIterable<TradeEntity> tradeEntities, MutableMap<CurrencyUnit, BigDecimal> moneyMap,
                               MutableObjectIntMap<SecurityKey> tradeMap) {
        tradeEntities.forEach(trade -> {
            if (trade.isMoney())
                moneyMap.merge(currencyMapper.getCurrency(trade.getPriceCurrency()), trade.getPrice(), BigDecimal::add);
            else
                tradeMap.forEach(entity -> tradeMap.addToValue(trade.getSecurityKey(), trade.getQuantity()));
        });
    }

    protected Mono<DayBalance> reduceDayResult(UUID userID, LocalDate day, MutableMap<CurrencyUnit, MonetaryAmount> dayResult) {
        final var monoRubSum = Flux.fromIterable(dayResult.entrySet())
                .concatMap(entry -> convert(day, entry.getKey().getCurrencyCode(), "RUB", entry.getValue()))
                .reduce(BigDecimal::add)
                .doOnNext(bd -> log.info("rubSum='{}'", bd));
        final var monoEurSum = Flux.fromIterable(dayResult.entrySet())
                .concatMap(entry -> convert(day, entry.getKey().getCurrencyCode(), "EUR", entry.getValue()))
                .reduce(BigDecimal::add)
                .defaultIfEmpty(BigDecimal.ZERO)
                .doOnNext(bd -> log.info("eurSum='{}'", bd));
        final var monoUsdSum = Flux.fromIterable(dayResult.entrySet())
                .concatMap(entry -> convert(day, entry.getKey().getCurrencyCode(), "USD", entry.getValue()))
                .reduce(BigDecimal::add)
                .defaultIfEmpty(BigDecimal.ZERO)
                .doOnNext(bd -> log.info("usdSum='{}'", bd));
        return Mono.zip(monoRubSum, monoEurSum, monoUsdSum)
                .map(objects -> {
                    log.info("zip reduceDayResult {}", day);
                    return new DayBalance(null,
                            day,
                            objects.getT1(),
                            objects.getT2(),
                            objects.getT3(),
                            userID,
                            Instant.now(clock),
                            userID,
                            Instant.now(clock));
                })
                .single();
    }

    private Mono<BigDecimal> convert(LocalDate day, String from, String to, MonetaryAmount money) {
        return currencyConversationService.getRate(day, from, to)
                .doOnNext(bd -> log.info("CurrencyRate day='{}' cur='{}:{}' rate='{}'", day, from, to, bd))
                .map(rate -> money.multiply(rate).getFactory().setCurrency(to).create())
                .map(monetaryAmount -> monetaryAmount.getNumber().numberValue(BigDecimal.class));
    }

    private void calcMoneySum(MutableMap<CurrencyUnit, BigDecimal> moneyMap, MutableMap<CurrencyUnit, MonetaryAmount> dayResult) {
        for (Map.Entry<CurrencyUnit, BigDecimal> next : moneyMap.entrySet()) {
            dayResult.merge(next.getKey(), Money.of(next.getValue(), next.getKey()), MonetaryAmount::add);
        }
    }


    private void calcSecuritySumOnDay(LocalDate day,
                                      ObjectIntMap<SecurityKey> securityCountMap,
                                      ImmutableMap<SecurityKey, SecurityPrice> securityPriceMap,
                                      MutableMap<CurrencyUnit, MonetaryAmount> result) {
        for (var entry : securityCountMap.keyValuesView()) {
            var key = entry.getOne();
            var value = entry.getTwo();
            Optional.ofNullable(securityPriceMap.get(key))
                    .flatMap(securityPrice -> securityPrice.calcPriceOnDate(day, value))
                    .ifPresent(money -> result.merge(money.getCurrency(), money, MonetaryAmount::add));
        }
    }


    private Mono<Pair<MutableMap<CurrencyUnit, BigDecimal>, MutableObjectIntMap<SecurityKey>>> getStockPortfolio(UUID userId) {
        return portfolioRepository.findByUserId(userId)
                .collect(toList())
                .map(list -> {
                    var partition = list.partition(StockPortfolio::isMoney);
                    //Сделки с валютой
                    final var moneyPortfolio = partition.getSelected();
                    var moneySumMap = Maps.mutable.<CurrencyUnit, BigDecimal>withInitialCapacity(moneyPortfolio.size());
                    moneyPortfolio.forEach(stockPortfolio ->
                            moneySumMap.merge(
                                    Monetary.getCurrency(TradeUtility.getMoneyCurrency(stockPortfolio.getTicker())),
                                    stockPortfolio.getBuySumPrice().subtract(stockPortfolio.getSaleSumPrice()),
                                    BigDecimal::add
                            )
                    );
                    //Сделки с бумагами
                    final var tradePortfolio = partition.getRejected();
                    var securityCountMap = ObjectIntMaps.mutable.<SecurityKey>withInitialCapacity(tradePortfolio.size());
                    tradePortfolio.forEach(stock ->
                            securityCountMap.addToValue(stock.getSecurityKey(), stock.getQuantity())
                    );
                    return Tuples.pair(moneySumMap, securityCountMap);
                });
    }

    protected MutableMap<LocalDate, MutableMap<String, Money>> reduceMoneyByDate(MutableList<TradeEntity> moneyTrade) {
        final MutableMap<LocalDate, MutableMap<String, Money>> moneyTradeMap = Maps.mutable.empty();
        moneyTrade.forEach(trade -> moneyTradeMap
                .getIfAbsentPut(trade.getLocalDate(), Maps.mutable::empty)
                .merge(trade.getTicker(), trade.getPriceMoney(), Money::add)
        );
        return moneyTradeMap;
    }


    protected MutableMap<LocalDate, ObjectIntHashMap<PairKey<String, String>>> reduceTradeByDateAndTicker(List<TradeEntity> trades) {
        final MutableMap<LocalDate, ObjectIntHashMap<PairKey<String, String>>> dateTradeMap = Maps.mutable.withInitialCapacity(trades.size());
        for (TradeEntity trade : trades) {
            final var localDate = trade.getLocalDate();
            final var ticker = trade.getTicker();
            final var exchange = trade.getExchange();
            final var map = dateTradeMap.getIfAbsentPut(localDate, ObjectIntHashMap::new);
            map.addToValue(PairKey.of(ticker, exchange), trade.getQuantity());
        }
        return dateTradeMap;
    }
}
