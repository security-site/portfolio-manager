package com.github.klawru.site.service.mapper;

import com.github.klawru.lib.contract.porfolio.v1.types.responce.TradeDTO;
import com.github.klawru.site.repository.entity.TradeEntity;
import com.github.klawru.site.service.parser.mapper.CurrencyMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(uses = CurrencyMapper.class)
public interface TradeDTOMapper {
    TradeDTOMapper INSTANCE = Mappers.getMapper(TradeDTOMapper.class);

    @Mapping(target = "price", source = "priceMoney")
    @Mapping(target = "sum", source = "sumMoney")
    @Mapping(target = "nkd", source = "nkdMoney")
    @Mapping(target = "fee", source = "feeMoney")
    TradeDTO map(TradeEntity entity);

}
