package com.github.klawru.site.service;

import com.github.klawru.lib.contract.security.v1.types.request.SecurityRequestDTO;
import com.github.klawru.lib.contract.security.v1.types.request.SecurityRequestType;
import com.github.klawru.lib.contract.security.v1.types.responce.SecurityDTO;
import com.github.klawru.site.repository.entity.SecurityKey;
import com.github.klawru.site.service.client.DbCacheClient;
import com.github.klawru.site.utility.SecurityPrice;
import lombok.RequiredArgsConstructor;
import org.eclipse.collections.api.map.ImmutableMap;
import org.eclipse.collections.impl.collector.Collectors2;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class TradePriceService {


    private final DbCacheClient dbCacheService;

    //Заменить на кеш?
    public Mono<ImmutableMap<SecurityKey, SecurityPrice>> getAllSecurityPrice(LocalDate fromDate, Iterable<SecurityKey> securityKeys) {
        Flux<SecurityDTO> securityDTOFlux = Flux.empty();
        for (SecurityKey key : securityKeys) {
            var requestDTO = new SecurityRequestDTO(key.getTicker(), key.getExchange(), fromDate, SecurityRequestType.PRICE_ONLY);
            securityDTOFlux = securityDTOFlux.concatWith(dbCacheService.getSecurity(requestDTO));
        }
        return securityDTOFlux
                .collect(Collectors2.toImmutableMap(dto -> SecurityKey.of(dto.getTicker(), dto.getExchange()), SecurityPrice::new));
    }

}
