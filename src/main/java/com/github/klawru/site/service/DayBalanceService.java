package com.github.klawru.site.service;

import com.github.klawru.lib.contract.porfolio.v1.types.request.DayBalanceRequest;
import com.github.klawru.lib.contract.porfolio.v1.types.responce.DayBalanceDTO;
import com.github.klawru.site.repository.DayBalanceRepository;
import com.github.klawru.site.security.SecurityUtils;
import com.github.klawru.site.service.parser.mapper.DayBalanceMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

@Slf4j
@Service
@RequiredArgsConstructor
public class DayBalanceService {
    private final DayBalanceRepository repository;
    private final DayBalanceMapper dayBalanceMapper = DayBalanceMapper.INSTANCE;

    public Flux<DayBalanceDTO> getDayBalance(DayBalanceRequest request) {
        return SecurityUtils.getCurrentUserId().flatMapMany(uuid -> {
                    if (request.getFrom() == null)
                        return repository.findAllByUserId(uuid);
                    return repository.findAllByUserIdAndDateAfter(uuid, request.getFrom());
                })
                .map(dayBalanceMapper::map);
    }
}
