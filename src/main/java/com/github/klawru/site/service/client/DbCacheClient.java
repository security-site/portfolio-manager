package com.github.klawru.site.service.client;

import com.github.klawru.lib.contract.security.v1.types.request.CurrencyRateRequestDTO;
import com.github.klawru.lib.contract.security.v1.types.request.SecurityRequestDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.SecurityDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.SecurityPriceDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.currency.CurrencyRateDTO;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.messaging.rsocket.RSocketRequester;
import org.springframework.security.rsocket.metadata.BearerTokenMetadata;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.Nonnull;

import static com.github.klawru.lib.contract.config.ClientDiscovery.DBCACHE;
import static com.github.klawru.lib.contract.security.v1.SecurityInfo.*;
import static com.github.klawru.site.config.RSocketConfiguration.AUTHENTICATION_MIME_TYPE;
import static com.github.klawru.site.security.SecurityUtils.getCurrentUserJWT;

@Service
public class DbCacheClient {

    private final RSocketRequester rSocketRequester;

    public DbCacheClient(@Qualifier(DBCACHE) RSocketRequester rSocketRequester) {
        this.rSocketRequester = rSocketRequester;
    }

    public Flux<SecurityDTO> getSecurity(SecurityRequestDTO request) {
        return getCurrentUserJWT().flatMapMany(jwt ->
                rSocketRequester.route(route(GET_SECURITY))
                        .metadata(new BearerTokenMetadata(jwt), AUTHENTICATION_MIME_TYPE)
                        .data(request)
                        .retrieveFlux(SecurityDTO.class));
    }

    public Mono<SecurityPriceDTO> getSecurityPrice(SecurityRequestDTO request) {
        return getCurrentUserJWT().flatMap(jwt ->
                rSocketRequester.route(route(GET_SECURITY_PRICE))
                        .metadata(new BearerTokenMetadata(jwt), AUTHENTICATION_MIME_TYPE)
                        .data(request)
                        .retrieveMono(SecurityPriceDTO.class));
    }


    public Mono<CurrencyRateDTO> getCurrencyRate(CurrencyRateRequestDTO request) {
        return getCurrentUserJWT().flatMap(jwt ->
                rSocketRequester.route(route(GET_CURRENCY_RATE))
                        .metadata(new BearerTokenMetadata(jwt), AUTHENTICATION_MIME_TYPE)
                        .data(request)
                        .retrieveMono(CurrencyRateDTO.class));
    }

    @Nonnull
    private String route(String getSecurity) {
        return DBCACHE + "/" + getSecurity;
    }

}
