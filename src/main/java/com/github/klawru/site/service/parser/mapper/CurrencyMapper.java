package com.github.klawru.site.service.parser.mapper;

import org.eclipse.collections.impl.map.mutable.primitive.IntObjectHashMap;
import org.mapstruct.Mapper;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import java.util.Currency;
import java.util.Set;

@Mapper
@Named("CurrencyMapper")
public abstract class CurrencyMapper {
    public static final CurrencyMapper INSTANCE = Mappers.getMapper(CurrencyMapper.class);
    //todo заглушка лучше хранить в БД
    private static final IntObjectHashMap<String> currencies = new IntObjectHashMap<>();

    static {
        Set<Currency> set = Currency.getAvailableCurrencies();
        for (Currency currency : set) {
            currencies.put(currency.getNumericCode(), currency.getCurrencyCode());
        }
    }

    @Named("currencyToInteger")
    public Integer currencyToInteger(String currency) {
        if (currency == null)
            return null;
        return Monetary.getCurrency(currency).getNumericCode();
    }

    @Named("NumCodeToString")
    public static String currencyToCode(int currency) {
        return currencies.get(currency);
    }

    public static CurrencyUnit currencyToUnit(Integer currencyId) {
        String currency = currencyToCode(currencyId);
        return Monetary.getCurrency(currency);
    }
}
